<?php

use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Headers: Origin, Content-Type'); 
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::any('/getcountry/{id}','api\RegisterController@getallcountry');
Route::any('/getallcountry','api\RegisterController@getcountry');
Route::any('/getall-uae-area','api\RegisterController@getarea');
Route::any('/getstate/{coid}','api\RegisterController@getstateByCountry_id');
Route::any('/getcity/{stateid}','api\RegisterController@gecitybyid');
Route::any('/getcity/{stateid}','api\RegisterController@gecitybyid');
Route::any('/update_device_id','api\RegisterController@update_device_id');
Route::any('/getcity/search/{data}','api\RegisterController@getcityautocomplete');
Route::any('/getPropertyByCity/{cityid}','api\RegisterController@gePropertybyid');
Route::any('/get-Property/{search}','api\RegisterController@getProperty');
Route::any('/get-Property','api\RegisterController@Property');
Route::any('/get-Location/{cityid}','api\RegisterController@getLocation');
Route::any('/get-Location','api\RegisterController@Location');
Route::any('/get-Language','api\RegisterController@getLanguage');

Route::any('/register','api\RegisterController@index');
Route::any('/change-password','api\RegisterController@change_password');
Route::any('/login/{email}/{pass}','api\RegisterController@login');
Route::any('/edit-profile','api\RegisterController@edit_profile');
Route::any('/get-profile','api\RegisterController@get_profile');


  // Add Property
Route::any('/step-one','api\ApartmentController@step_one');
Route::any('/step-two','api\ApartmentController@step_two');
Route::any('/step-three','api\ApartmentController@step_three');
Route::any('/step-four','api\ApartmentController@step_four');

  // Edit Property
Route::any('/edit_step_one','api\ApartmentController@edit_step_one');
Route::any('/edit-step-two','api\ApartmentController@edit_step_two');

//  Frontend Listing

Route::any('/get-property-list','api\FrontController@get_property');
Route::any('/get-property-detail/{id}','api\FrontController@get_property_detail');
Route::any('/frontproperty-list','api\FrontController@frontproperty');
Route::any('/my-listings/{id}','api\FrontController@my_listings');
Route::any('/book-listings/{id}','api\FrontController@book_listings');
Route::any('/my-bookings/{user_id}','api\FrontController@my_bookings');
Route::any('/my-bookings-details/{booking_id}','api\FrontController@my_bookings_details');
Route::any('/verifymobile','api\FrontController@verifymobile');
Route::any('/verifyemail','api\FrontController@verifyemail');
Route::any('/code_verify','api\FrontController@code_verify');
Route::any('/mylisting_approval','api\FrontController@mylisting_approval');
Route::any('/edit-listing/{bed_id}','api\FrontController@edit_listing_data');



// Filter And Delete Property
Route::any('/property-list/{id}','api\FrontController@frontpropertyBYLocation');
Route::any('/listing_filter','api\FrontController@frontpropertyBYFilter');
Route::any('/building_filter','api\FrontController@get_buildings_for_filter');
Route::any('/location_filter','api\FrontController@get_locations_for_filter');
Route::any('/location_filter/{autocom}','api\FrontController@get_locations_autocom');
Route::any('/delete-property/{id}','api\FrontController@delete_property');

// Entire Property
//Route::any('/property-list/{id}','api\FrontController@frontpropertyBYLocation');
Route::any('/entire_filter','api\EntirePropertyController@entirepropertyBYFilter');
Route::any('/get-entire-detail/{id}','api\EntirePropertyController@get_property_detail');


 // History
Route::any('/get_payment_history/{id}','api\FrontController@payment_history');
Route::any('/get_deposit_history/{id}','api\FrontController@deposit_history');

         // Notice Module
Route::any('/add_notice','api\FrontController@add_notice');
Route::any('/view_notice/{id}','api\FrontController@view_notice');

        // Complaint Module

Route::any('/add_complaint','api\ComplaintController@add_complaint');
Route::any('/get-complaint-detail/{sub_com_id}','api\ComplaintController@get_complaint_details');
Route::any('/complaint_status-client/{booking}/{status}','api\ComplaintController@get_client_complaint');
Route::any('/close_complaint','api\ComplaintController@close_complaint');
Route::any('/edit-complaint','api\ComplaintController@edit_complaint');




  // Parking Module
  Route::any('/parking-step-one','api\ParkingController@parking_step_one');
  Route::any('/get-parking','api\ParkingController@get_parking_list');
  Route::any('/get-parking-detail/{prk_id}','api\ParkingController@get_parking_by_id');
  Route::any('/get-parking-id/{tenant_id}','api\ParkingController@get_parking_by_tenant_id');
  Route::any('/delete-parking/{id}','api\ParkingController@delete_parking_by_id');
  Route::any('/parking-floors','api\ParkingController@parking_floors');
  Route::any('/edit_parking/{id}','api\ParkingController@edit_parking');
  Route::any('/change-parking-status/{id}/{status}','api\ParkingController@change_parking_status');

  // Dashboard Details

    Route::any('/total-budget/{id}','api\DashboardController@total_budget');
    Route::any('/get-notification/{id}','api\DashboardController@get_notification');
    Route::any('/delete-booking/{id}','api\DashboardController@delete_booking');

    // Avatar module
 Route::any('/get-avatar','api\DashboardController@get_avatar');
 Route::any('/update-avatar/{user_id}/{avatar_id}','api\DashboardController@update_avatar');

    // Chat Module

  Route::get('/fetchMessages/{bed}/{rec}/{sen}','api\ChatController@fetchMessages');
  Route::post('/sendMessage','api\ChatController@sendMessage');
  Route::get('/getAllChatUser/{id}','api\ChatController@getAllChatUser');
  Route::any('/first-message','api\ChatController@FirstMsg');
  Route::any('/change-message-status','api\ChatController@conversations');

    // Lead Module
  Route::post('/save-lead','api\LeadController@save_Lead');
  Route::get('/get-lead/{bedspace}','api\LeadController@get_Lead');


   // Route::any('/my_/{booking_id}','api\BookingController@parking_step_one');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
 Route::get('/ttttttttttt','api\ChatController@test_event');
 Route::get('/user-detail/{id}','api\UserController@user_details');
//  Route::post('/entire-property/step_one','api\UserController@step_one');
//  Route::post('/entire-property/step_two','api\UserController@step_one');

// Route::post('/entire-property/edit_step_one','api\UserController@edit_step_one');
// Route::post('/entire-property/edit_step_two','api\UserController@edit_step_two');
 

// Route::any('/test',function(){
// 	die('sss');
// });
//entire property
Route::any('/entire_step_one','api\EntirePropertyController@entire_step_one');

Route::any('/entire_step_two','api\EntirePropertyController@entire_step_two');
Route::any('/entire_step_three','api\EntirePropertyController@entire_step_three');
Route::any('/entire_step_four','api\EntirePropertyController@entire_step_four');

//Route::any('/entire_edit_step_two','api\EntirePropertyController@entire_edit_step_two');
