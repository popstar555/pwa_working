<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use DB;
use App\User;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\Building;
use App\Room;
use App\Bedspace;
use App\RoomPic;
use App\PropertyDocument;

class ApartmentController extends Controller
{
            public function step_one(Request $data){
             

             $Property = new Building;
             $Property->property_type = $data->property_type;
             if($data->property_type == 'apartment' && $data->property_id != '')
             {
              $Property->property_id = $data->property_id;
              $prop = DB::table('tbl_property')->where('id', $data->property_id)->first();  
              $Property->fld_building = $prop->name;  
             }

             $area = DB::table('tbl_location')->where('id', $data->fld_area)->first();
             $Property->fld_area = $area->name;
             $Property->fld_address = $data->fld_address;
             $Property->fld_latitude = $data->fld_latitude;
             $Property->fld_longitude = $data->fld_longitude;
             $Property->fld_tanent = $data->fld_tanent;
             $Property->added_from = 1;
             $Property->fld_isactive = 1;
             $Property->fld_num_of_beds = 1;
            $Property->fld_is_deleted  = 0;
            $Property->city_id  = $data->city_id;

             if($Property->save())
             {  

                  return response()->json([
            'status'  => true,
            'id'  => $Property->fld_id,
            'message' => 'Building Added SuccessFully'
               ]);
             } else {
                  return response()->json([
            'status'  => false,
            'message' => '!OOPS, Some Error Occoured'
              ]);
             }
 

            }

            public function edit_step_one(Request $data){
              
             if(!empty($data->fld_building_id)) {
             $Property =  Building::where('fld_id',$data->fld_building_id)->first();
           }
           
             $Property->property_type = $data->property_type;
           if($data->property_type == 'apartment' && $data->property_id != '')
             {
              $Property->property_id = $data->property_id;
              $prop = DB::table('tbl_property')->where('id', $data->property_id)->first();  
              $Property->fld_building = $prop->name;  
             }



            
            // if(!empty($data->fld_area))
            // {
            //  $area = DB::table('tbl_location')->where('id', $data->fld_area)->first();
            //  $Property->fld_area = $area->name;
            // }
           $Property->fld_area = $data->fld_area;
           $Property->fld_address = $data->fld_address;
           $Property->fld_latitude = $data->fld_latitude;
           $Property->fld_longitude = $data->fld_longitude;
           $Property->city_id  = $data->city_id;

             if($Property->save())
             {  

                return response()->json([
                'status'  => true,
                'message' => 'Building Edited SuccessFully'
                ]);
             } else {

                return response()->json([
                'status'  => false,
                'message' => '!OOPS, Some Error Occoured'
                ]);
             }


            }

                

            public function step_two(Request $data){
               
             // return response()->json($data); 


             
               $Property = new Room;
             $Property->fld_building_id = $data->fld_building_id;
             $Property->private_washroom_amenities = $data->bath == 'private_washroom' ? 1 : 0;
             $Property->shared_washroom_amenities = $data->bath  == 'shared_washroom' ? 1 : 0;
             $Property->room_mates_male = $data->room_mates_male;
             $Property->room_mates_female = $data->room_mates_female;
             $Property->occupancy = $data->occupancy;
             $Property->fld_custom_room_name = $data->photos_listing_title;
             $Property->photos_listing_title = $data->photos_listing_title;
             $Property->gender_room = $data->roommate_gender;
             $Property->is_listing_approved = 1;
              
               // amenities room 
                if(!empty($_REQUEST['Room_amenities']))
                {
                   $Room_amenities = $_REQUEST['Room_amenities'];
                   $Room_amenities1 = explode(',', $Room_amenities);
                } else {
                  $Room_amenities1 = [];
                }




               if(in_array('TV', $Room_amenities1))
               {
                $Property->tv_amenities = 1;
               } else {  $Property->tv_amenities = 0; }
                 if(in_array('Private_Balcony', $Room_amenities1))
               {
                $Property->private_balcony_amenities = 1;
               } else {  $Property->private_balcony_amenities = 0; }

                 if(in_array('Window', $Room_amenities1))
               {
                $Property->window = 1;
               } else {  $Property->window = 0; }

                 if(in_array('Natrual_Light', $Room_amenities1))
               {
                $Property->natural_light_amenities = 1;
               } else {  $Property->natural_light_amenities = 0; }

                 if(in_array('Fridge', $Room_amenities1))
               {
                $Property->fridge_amenities = 1;
               }  else {  $Property->fridge_amenities = 0; }


               if(!empty($_REQUEST['outDoorAmenties'] ))
               {
                 $outDoorAmenties = $_REQUEST['outDoorAmenties'];
                 $outDoorAmenty = explode(',', $outDoorAmenties);
               } else {
                 $outDoorAmenty = [];
               }
     


      

               
                if(in_array('Garden', $outDoorAmenty))
               {
                $Property->garden_amenities = 1;
               } else {  $Property->garden_amenities = 0; }
                 

                 if(!empty($_REQUEST['house_amenities']))
                 {
                   $house_ameneties = $_REQUEST['house_amenities'];
                   $house = explode(',', $house_ameneties);
                 } else {
                    $house = [];
                 }

                 if(in_array('Wifi', $house))
               {
                $Property->wifi_amenities = 1;
               }
                 if(in_array('Wash Machine', $house))
               {
                $Property->washing_amenities = 1;
               }
                 if(in_array('Common Balcony', $house))
               {
                $Property->balcony_amenities = 1;
               }
                 if(in_array('Dish Washer', $house))
               {
                $Property->dishwasher_amenities = 1;
               }
                 if(in_array('Wheelchair Frindly', $house))
               {
                $Property->wheelcheer_amenities = 1;
               }
                 if(in_array('Garden', $house))
               {
                $Property->garden_amenities = 1;
               }
                 if(in_array('Terrace', $house))
               {
                $Property->terrace_amenities = 1;
               }
                 if(in_array('Living Room', $house))
               {
                $Property->living_room_amenities = 1;
               }
             
               $Property->other_house_rules = $data->other_rules; 

               if($Property->save()) 
              {      

                       $building =  Building::where('fld_id',$data->fld_building_id)->first();
                             // building Ameneties   $Property->bed_type = $data->bed_type;
                    
                     if(in_array('Parking', $house))
                         {
                          $building->parking_amenities = 1;
                         }
                   // if(in_array('Dish Washer', $house))
                   //       {
                   //        $building->parking_amenities = 1;
                   //       }
                   if(in_array('Elevator', $house))
                         {
                          $building->elevator_amenities = 1;
                         }
                   if(in_array('Pool', $house))
                         {
                          $building->pool_amenities = 1;
                         }
                   if(in_array('gym', $house))
                         {
                          $building->gym = 1;
                         }

                             
                if(in_array('Lawn tennis', $outDoorAmenty))
               {
                $building->lawn_tennis = 1;
               }
                if(in_array('Sqaush', $outDoorAmenty))
               {
                $building->squash = 1;
               }
               if(in_array('Table tenis', $outDoorAmenty))
               {
                $building->table_tennis = 1;
               }
               if(in_array('Pool', $outDoorAmenty))
               {
                $building->pool_amenities = 1;
               }

               if(in_array('Gym', $outDoorAmenty))
               {
                $building->gym = 1;
               }


               
              if(!empty($_REQUEST['house_rules'])){
                $house_rules = $_REQUEST['house_rules'];
                $house_rl = explode(',', $house_rules);
              } else {
                 $house_rl = [];
              }
 
                      


                 // house rules
                           if(in_array('Smoke Friendly', $house_rl))
                         {
                          $building->house_rules_smoker = 1;
                         }
                              if(in_array('Pet Friendly', $house_rl))
                         {
                          $building->house_rules_pet = 1;
                         }
                              if(in_array('Couples Allowed', $house_rl))
                         {
                          $building->house_rules_couple = 1;
                         }
                              if(in_array('Children Allowed', $house_rl))
                         {
                          $building->house_rules_children = 1;
                         }
                
                   if($building->save()) 
                   {  
                           $Bedspace = new Bedspace;
                           
                           $Bedspace->fld_room        = $Property->fld_id;
                           $Bedspace->fld_building_id = $data->fld_building_id;
                           $Bedspace->fld_type        = $data->fld_type;
                           $Bedspace->fld_is_bs_model = '1';
                           $Bedspace->fld_block_unblock = '1';
                           $Bedspace->fld_owner = $building->fld_tanent;

                           if($Bedspace->save())
                           {
                                $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$Property->fld_id,'building_id' => $building->fld_id,'Bedspace_id' => $Bedspace->fld_id); 
                           } else {
                                 $json_data = array('status'=>false,'message'=>"Error in saving Bedspace data"); 
                           }

                    


                   } else {
                         $json_data = array('status'=>false,'message'=>"Error in saving Building Ameneties"); 
                   }

            } else {

              $json_data = array('status'=>false,'message'=>"Error in saving Room Ameneties"); 
            }
  

            return response()->json($json_data);
               
            }

            public function edit_step_two(Request $data){
             
           //  return response()->json($data);

            $Property = Room::where('fld_id',$data->fld_room_id)->first();
             $Property->bath = $data->bath;
             $Property->room_mates_male = $data->room_mates_male;
             $Property->room_mates_female = $data->room_mates_female;
             // $Property->number_of_roommates = $data->number_of_roommates;
              $Property->occupancy = $data->occupancy;


               if($data->has('tv_amenities'))
               { 
                  if($data->tv_amenities == 1 || $data->tv_amenities == 'true')
                  {
                     $Property->tv_amenities = 1;
                   } else {
                     $Property->tv_amenities = 0;
                   }
               
               }

               if($data->has('Private_Balcony'))
               { 
                  if($data->Private_Balcony == 1 || $data->Private_Balcony == 'true')
                  {
                     $Property->private_balcony_amenities = 1;
                   } else {
                     $Property->private_balcony_amenities = 0;
                   }
               
               }

               if($data->has('Window'))
               { 
                  if($data->Window == 1 || $data->Window == 'true')
                  {
                     $Property->window = 1;
                   } else {
                     $Property->window = 0;
                   }
               
               }

               if($data->has('Natrual_Light'))
               { 
                  if($data->Natrual_Light == 1 || $data->Natrual_Light == 'true')
                  {
                     $Property->natural_light_amenities = 1;
                   } else {
                     $Property->natural_light_amenities = 0;
                   }
               
               }

               if($data->has('Fridge'))
               { 
                  if($data->Fridge == 1 || $data->Fridge == 'true')
                  {
                     $Property->fridge_amenities = 1;
                   } else {
                     $Property->fridge_amenities = 0;
                   }
               
               }


               if($data->has('Private_Washroom'))
               { 
                  if($data->Private_Washroom == 1 || $data->Private_Washroom == 'true')
                  {
                     $Property->private_washroom_amenities = 1;
                   } else {
                     $Property->private_washroom_amenities = 0;
                   }
               
               }

               if($data->has('Shared_Washroom'))
               { 
                  if($data->Shared_Washroom == 1 || $data->Shared_Washroom == 'true')
                  {
                     $Property->shared_washroom_amenities = 1;
                   } else {
                     $Property->shared_washroom_amenities = 0;
                   }
               
               }

               if($data->has('Wifi'))
               { 
                  if($data->Wifi == 1 || $data->Wifi == 'true')
                  {
                     $Property->wifi_amenities = 1;
                   } else {
                     $Property->wifi_amenities = 0;
                   }
               
               }

               // if(($data->has('Wifi'))
               // { 
               //    if($data->Wifi == 1 || $data->Wifi == 'true')
               //    {
               //       $Property->wifi_amenities = 1;
               //     } else {
               //       $Property->wifi_amenities = 0;
               //     }
               
               // }
                 if($data->has('washing_amenities'))
               { 
                  if($data->washing_amenities == 1 || $data->washing_amenities == 'true')
                  {
                     $Property->washing_amenities = 1;
                   } else {
                     $Property->washing_amenities = 0;
                   }
               
               }
                 if($data->has('balcony_amenities'))
               { 
                  if($data->balcony_amenities == 1 || $data->balcony_amenities == 'true')
                  {
                     $Property->balcony_amenities = 1;
                   } else {
                     $Property->balcony_amenities = 0;
                   }
               
               }
                 if($data->has('dishwasher_amenities'))
               { 
                  if($data->dishwasher_amenities == 1 || $data->dishwasher_amenities == 'true')
                  {
                     $Property->dishwasher_amenities = 1;
                   } else {
                     $Property->dishwasher_amenities = 0;
                   }
               
               }

                 if($data->has('wheelcheer_amenities'))
               { 
                  if($data->wheelcheer_amenities == 1 || $data->wheelcheer_amenities == 'true')
                  {
                     $Property->wheelcheer_amenities = 1;
                   } else {
                     $Property->wheelcheer_amenities = 0;
                   }
               
               }


                    if($data->has('garden_amenities'))
               { 
                  if($data->garden_amenities == 1 || $data->garden_amenities == 'true')
                  {
                     $Property->garden_amenities = 1;
                   } else {
                     $Property->garden_amenities = 0;
                   }
               
               }
                 if($data->has('terrace_amenities'))
               { 
                  if($data->terrace_amenities == 1 || $data->terrace_amenities == 'true')
                  {
                     $Property->terrace_amenities = 1;
                   } else {
                     $Property->terrace_amenities = 0;
                   }
               
               }
                 if($data->has('living_room_amenities'))
               { 
                  if($data->living_room_amenities == 1 || $data->living_room_amenities == 'true')
                  {
                     $Property->living_room_amenities = 1;
                   } else {
                     $Property->living_room_amenities = 0;
                   }
               
               }
             
               $Property->other_house_rules = $data->other_rules; 

               if($Property->save()) 
              {      

                $building =  Building::where('fld_id',$data->fld_building_id)->first();
                    
                if($data->has('parking_amenities'))
               { 
                  if($data->parking_amenities == 1 || $data->parking_amenities == 'true')
                  {
                     $building->parking_amenities = 1;
                   } else {
                     $building->parking_amenities = 0;
                   }
               
               }

                if($data->has('elevator_amenities'))
               { 
                  if($data->elevator_amenities == 1 || $data->elevator_amenities == 'true')
                  {
                     $building->elevator_amenities = 1;
                   } else {
                     $building->elevator_amenities = 0;
                   }
               
               }
                   if($data->has('table_tennis'))
               { 
                  if($data->table_tennis == 1 || $data->table_tennis == 'true' || $data->table_tennis == true)
                  {
                     $Property->table_tennis = 1;
                   } else {
                     $Property->table_tennis = 0;
                   }
               
               }
                  if($data->has('squash'))
               { 
                  if($data->squash == 1 || $data->squash == 'true' || $data->squash == true)
                  {
                     $Property->squash = 1;
                   } else {
                     $Property->squash = 0;
                   }
               
               }
                          if($data->has('lawn_tennis'))
               { 
                  if($data->lawn_tennis == 1 || $data->lawn_tennis == 'true' || $data->lawn_tennis == true)
                  {
                     $Property->lawn_tennis = 1;
                   } else {
                     $Property->lawn_tennis = 0;
                   }
               
               }
                if($data->has('pool_amenities'))
               { 
                  if($data->pool_amenities == 1 || $data->pool_amenities == 'true')
                  {
                     $building->pool_amenities = 1;
                   } else {
                     $building->pool_amenities = 0;
                   }
               
               }
                 if($data->has('gym'))
               { 
                  if($data->gym == 1 || $data->gym == 'true')
                  {
                     $building->gym = 1;
                   } else {
                     $building->gym = 0;
                   }
               
               }
                 if($data->has('house_rules_pet'))
               { 
                  if($data->house_rules_pet == 1 || $data->house_rules_pet == 'true')
                  {
                     $building->house_rules_pet = 1;
                   } else {
                     $building->house_rules_pet = 0;
                   }
               
               }
                if($data->has('house_rules_smoker'))
               { 
                  if($data->house_rules_smoker == 1 || $data->house_rules_smoker == 'true')
                  {
                     $building->house_rules_smoker = 1;
                   } else {
                     $building->house_rules_smoker = 0;
                   }
               
               }
                if($data->has('house_rules_couple'))
               { 
                  if($data->house_rules_couple == 1 || $data->house_rules_couple == 'true')
                  {
                     $building->house_rules_couple = 1;
                   } else {
                     $building->house_rules_couple = 0;
                   }
               
               }
                if($data->has('house_rules_children'))
               { 
                  if($data->house_rules_children == 1 || $data->house_rules_children == 'true')
                  {
                     $building->house_rules_children = 1;
                   } else {
                     $building->house_rules_children = 0;
                   }
               
               }


                   if($building->save()) 
                   {  
                       $Bedspace = Bedspace::where('fld_id',$data->fld_bedspace_id)->first();
                       
                       $Bedspace->fld_type   = $building->fld_type;
                       if($Bedspace->save())
                       {
                            $json_data = array('status'=>true,'message'=>"Property Edited SuccessFull"); 
                       } else {
                             $json_data = array('status'=>false,'message'=>"Error in saving Bedspace data"); 
                       }

                    


                   } else {
                         $json_data = array('status'=>false,'message'=>"Error in saving Building Ameneties"); 
                   }

            } else {

              $json_data = array('status'=>false,'message'=>"Error in saving Room Ameneties"); 
            }
  

            return response()->json($json_data);

             
            }

              public function step_three(Request $request){
               
                 ini_set('memory_limit','2307278683M');

                  if(!empty($request->Room_id) && !empty($request->Bedspace_id))
                 {
                 $Room = Room::where('fld_id',$request->Room_id)->first();
                 

                       if(!empty($Room))
                       {
                          $Room->minimum_stay       = $request->minimum_stay;
                          $Room->fld_expected_rent  = $request->rent;
                          $Room->room_deposit       = $request->room_deposit;
                          $Room->avalability_date   = $request->from_date;
                          $Room->avalability_end    = $request->to_date;
                          $Room->metro_train        = $request->distance;
                          $Room->bed_type           = $request->bed_type;
                          $Room->fld_owner           = $request->fld_tanent;

                          if($request->bills_included == 'true' || $request->bills_included == 'TRUE' || $request->bills_included == true)
                          {
                            $Room->bills_included = 1;
                          } else {
                            $Room->bills_included = 0;
                          }

                          
                                if($Room->save())
                                {
                                  $Bedspace = Bedspace::where('fld_id',$request->Bedspace_id)->first();

                                      if(!empty($Bedspace)){
                                        $Bedspace->fld_expected_rent = $request->rent;
                                      
                                              if($Bedspace->save())
                                              {
                                                 $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$request->Room_id,'building_id' => $request->building_id,'Bedspace_id' => $request->Bedspace_id);  
                                              }

                                      } else{
                                       $json_data = array('status'=>false,'message'=>"Bedspace Id don't Exists.");  
                                      }

                                } else{
                                   $json_data = array('status'=>false,'message'=>"Error in Saving Room Details."); 
                                }
                         

                       } else {
                          
                          $json_data = array('status'=>false,'message'=>"Room Id don't Exists."); 
                       }
                 
       
                 } else {
                   $json_data = array('status'=>false,'message'=>"Please Enter Room Id."); 
                 }

                 return response()->json($json_data);

           }
 
              public function step_four(Request $request){
                  
                  //  foreach($request->file('room_pics') as $image){
                  //      $data[] =  $image;
                  //  }
                  // return response()->json($request,$data);

                 if(!empty($request->Room_id) && !empty($request->Bedspace_id))
                 {  

                   $Room = Room::where('fld_id',$request->Room_id)->first();
                      if(!empty($Room))
                       { 

                                  //upload file
                        // if ($request->hasFile('commonfile')) {
                        //     $image = $request->file('commonfile');
                        //     $name = time().'.'.$image->getClientOriginalExtension();
                        //     $destinationPath = public_path('PROPERTY_DOC');
                        //     $image->move($destinationPath, $name);
                        //     $commonpics = new PropertyDocument;
                        //     $commonpics->fld_property = $request->building_id;
                        //     $commonpics->fld_name = $name;
                        //     $commonpics->save();
                        // }

                            //save common pics in property documents
                          if($request->hasfile('commonfile')){
                             $del_commonpics = PropertyDocument::where('fld_property',$request->building_id)->delete();
                            foreach($request->file('commonfile') as $image){
                                      $name = time().'.'.$image->getClientOriginalName();
                                      $image->move(public_path('PROPERTY_DOC'), $name);  

                                   

                                      $commonpics = new PropertyDocument;
                                      $commonpics->fld_property = $request->building_id;
                                      $commonpics->fld_name = $name;
                                      $commonpics->save();
                                  }
                          }


                     $Room->display_pic  =$request->display_pic;
                     $Room->photos_listing_title  =$request->photos_listing_title;
                     $Room->fld_room_description  =$request->fld_room_description;
                     $Room->fld_custom_room_name = $request->photos_listing_title;


                      // //save room pics
                      // if($request->hasfile('room_pics')){
                      //             $image = $request->file('room_pics');
                      //             $name = time().'.'.$image->getClientOriginalName();
                      //             $image->move(public_path('room_pic'), $name);  
                                  
                      //             $roompics = new RoomPic;
                      //             $roompics->fld_room_id = $request->Room_id;
                      //             $roompics->fld_name = $name;
                      //             $roompics->save();



                          //save room pics
                          if($request->hasfile('room_pics')){
                                 $del_roompics = RoomPic::where('fld_room_id',$request->Room_id)->delete();
                            foreach($request->file('room_pics') as $image){
                                      $name = time().'.'.$image->getClientOriginalName();
                                      $image->move(public_path('room_pic'), $name);  
                                      

                                      $roompics = new RoomPic;
                                      $roompics->fld_room_id = $request->Room_id;
                                      $roompics->fld_name = $name;
                                      $roompics->save();
                                  }
                          
                              
                      } 

                              if($Room->save())
                                {
                                  $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$request->Room_id,'building_id' => $request->building_id,'Bedspace_id' => $request->Bedspace_id);
                                }

                       } else {
                         $json_data = array('status'=>false,'message'=>"Room Id don't Exists."); 
                       }
                      
                 } else {
                   $json_data = array('status'=>false,'message'=>"Please Enter Room Id."); 
                 }
                  return response()->json($json_data);


        }
}
