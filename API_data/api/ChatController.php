<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Bedspace;
use App\Avatar;
use App\Message;
use App\Conversation;
use App\Notification;
use App\Events\MessageSent;
use App\Events\MyEvent;
use DB;


class ChatController extends Controller
{ 
	public function deleteMessage(Request $request)
		{   

             $user_id = $request['user_id'];
             $msg_id = $request['msg_id'];
             $type = $request['type'];
             
             if($type == 'receiver')
             {
               $message = Message::where('id',$msg_id)->first();	
                $message->is_from_reciever = 1;

                $message->save();

             } else if($type == 'sender'){
               $message= Message::where('id',$msg_id)->first();

                $message->is_from_sender = 1;

                $message->save();
             }
        

         return 'Deleted';


		}

			/**
		 * Fetch all messages
		 *
		 * @return Message
		 */


		public function MyMessages()
		{   
           
		$user_id = Auth::user()->id;
 
		$allreceiver = DB::table('tbl_messages')
				->select('tbl_users.id')
				->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_messages.receiver_id')
				->where('tbl_messages.sender_id', $user_id)
				->groupBy('tbl_messages.receiver_id')->get();   

		$allsender = DB::table('tbl_messages')
				->select('tbl_users.id')
				->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_messages.sender_id')
				->where('tbl_messages.receiver_id', $user_id)
				->groupBy('tbl_messages.receiver_id')->get();
           
            if(!empty($allreceiver[0])){
           foreach ($allreceiver as  $receiver) {
           	$rec[] = $receiver->id;
           } } else {
           	$rec[] = '';
           }
          
           if(!empty($allsender[0])){
		           foreach ($allsender as  $sender) {
		           	$sen[] = $sender->id;
		           } 
           } else {
           	$sen[] = '';
           }
           if(!empty($rec[0]) && !empty($sen[0])){
           $alluser = [$rec,$sen];	
          } else if(!empty($rec[0]) && empty($sen[0])){
            $alluser = [$rec];	
          } else if(empty($rec[0]) && !empty($sen[0])){
            $alluser = [$sen];
          } 
           
         $user = $alluser[0];
           $getAllUsers = DB::table('tbl_users')->whereIn('id',$user)->groupBy('id')->get();
			return $getAllUsers;
		
        }
		public function fetchMessages($bedspace_id,$receiver_id,$sender_id )
		{   
            $target_path = config('app.url').'public/room_pic/';
		

           $messages = Message::join('tbl_users', 'tbl_users.id', '=', 'tbl_messages.sender_id')
	        ->select('tbl_users.fld_name','tbl_users.fld_profile_pic','tbl_messages.message','tbl_messages.sender_id','tbl_messages.receiver_id','tbl_messages.is_from_sender','tbl_messages.is_from_reciever','tbl_messages.id','tbl_messages.created_at','tbl_messages.is_read','tbl_messages.image')
	        ->where('tbl_messages.bedspace_id', $bedspace_id)
            ->where(function ($query) use ($sender_id, $receiver_id){
		    $query->where('sender_id', $sender_id)->where('receiver_id', $receiver_id)->where('is_from_sender',0);
			})->orWhere(function($query) use ($sender_id, $receiver_id){
			    $query->where('sender_id', $receiver_id)->where('receiver_id', $sender_id)->where('is_from_reciever',0);	
			})->orderBy('tbl_messages.created_at', 'DESC')->get();

			$receiver_details = User::where('id',$receiver_id)->first();
		//	$bedspace_details = Bedspace::where('fld_id',$bedspace_id)->first();
			$bedspace_details = DB::table('tbl_bedspace')
				->select('tbl_rooms.fld_custom_room_name AS fld_name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_rooms.fld_id as room_id','tbl_room_pics.fld_name As RoomImage','tbl_countries.currency','tbl_cities.state_name','tbl_building.fld_address','tbl_building.fld_building','tbl_building.fld_area',DB::raw("CONCAT('$target_path',tbl_room_pics.fld_name) AS path"),'tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_bedspace.fld_type')
				->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
				->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
				->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
				->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')
				->leftjoin('tbl_room_pics', 'tbl_room_pics.fld_room_id', '=', 'tbl_rooms.fld_id')
                ->where('tbl_bedspace.fld_id',$bedspace_id)->first();

                  $json = [];

              if($bedspace_details->fld_type == 'R')
                  {
                    $rent = $bedspace_details->room_expected_rent;
                  } else {
                  	$rent = $bedspace_details->bedspace_expected_rent;
                  }

             if(!empty($bedspace_details->fld_address))
				{
					$addr = $bedspace_details->fld_address;
				} else {
					$addr = $bedspace_details->fld_building.'-'.$bedspace_details->state_name.'-'.$bedspace_details->fld_area;
				}


                 $bus = array(
                'fld_name'         => trim($bedspace_details->fld_name),
                'room_id'          => $bedspace_details->room_id,
                'fld_area'         => $bedspace_details->fld_area,
                'state_name'       => $bedspace_details->state_name,
                'fld_building'     => $bedspace_details->fld_building,
                'currency'         => $bedspace_details->currency,
                'addr'             => $addr,
                'rent'             => $rent,
                'fld_type'         => $bedspace_details->fld_type,
                'path'             => $bedspace_details->path );

                  array_push($json, $bus);

            if($receiver_details->is_avatar == 1)
			{
				$check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$receiver_details->id)->first();

				$get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

				$receiver_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
				} 
			 else {
				$receiver_img =config('app.url').'public/assets/profiles'.$receiver_details->fld_profile_pic;
			}


            $sender = User::where('id', $sender_id)->first();


            if($sender->is_avatar == 1)
			{
				$check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$sender->id)->first();

				$get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

				$sender_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
				} 
			 else {
				$sender_img =config('app.url').'public/assets/profiles'.$sender->fld_profile_pic;
			}

                 $mymessages = [];
                 foreach ($messages as  $msg) {
                 
                 if($msg->sender_id == $sender_id)
                 {
                 	$profile_pc = $sender_img;
                 } else {
                    $profile_pc = $receiver_img;
                 }
                 
                 if($msg->image != '')
                 {
                   $img_msg = config('app.url').'public/chat/'.$msg->image;
                 } else {
                   $img_msg = '';
                 }

                 $bus = array(
                '_id'              => $msg->id,
                'fld_name'         => $msg->fld_name,
                'profile_pc'       => $profile_pc,
                'text'             => $msg->message,
                'sender_id'        => $msg->sender_id,
                'image'            => $img_msg,
                'receiver_id'      => $msg->receiver_id,
                'is_from_sender'   => $msg->is_from_sender,
                'is_from_reciever' => $msg->is_from_reciever,
                'created_at'       => $msg->created_at,
                'is_read'          => $msg->is_read
                 );

               array_push($mymessages, $bus);
               }





      

		// $unread_msg  = Message::where('user_id', $user_to_id)->where('user_to_id', $user_id)->where('is_read',0)->get();
  //       foreach($unread_msg as $msg)
  //       {
  //       	$message   = Message::where('id', $msg->id)->first();
	 //        $message->is_read = 1 ;                 
  //           $message->save();
  //       }

		//return $messages;  
		return array('messages' => $mymessages,'sender_id' => $sender_id ,'receiver_id' => $receiver_id,'receiver_details' => $receiver_details, 'bedspace' => $json,'receiver_img'=>$receiver_img,'sender' =>$sender, 'sender_img'=>$sender_img);

		}

		public function getAllChatUser($id)
		{  
	              $target_path = config('app.url').'public/room_pic/';
	                $user = User::where('id',$id)->first();	
	                $user_id = $id;

           	    $allreceiver = DB::table('tbl_messages')
				->select('tbl_messages.*', 'tbl_rooms.fld_custom_room_name',DB::raw("CONCAT('$target_path',tbl_room_pics.fld_name) AS path"))
				->join('tbl_bedspace', 'tbl_bedspace.fld_id', '=', 'tbl_messages.bedspace_id')
				->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
				->leftjoin('tbl_room_pics', 'tbl_room_pics.fld_room_id', '=', 'tbl_rooms.fld_id')    
				->where('tbl_messages.sender_id', $user_id)->Orwhere('tbl_messages.receiver_id',$user_id)
				// ->where(function ($query) use ($user_id){
				//     $query->where('sender_id', $user_id)->where('is_from_sender',0)->where('tbl_messages.receiver_id', '!=', '')->latest();
				// 	})->orWhere(function($query) use ($user_id){
				// 	    $query->where('receiver_id', $user_id)->where('is_from_reciever',0)->where('tbl_messages.sender_id', '!=', '')->latest();	
				// 	})
				->groupBy('tbl_messages.bedspace_id')
				->latest()->get(); 
                    $json = array();
                 foreach ($allreceiver as  $receiver) {
                   if($receiver->sender_id  != $user_id ){
                       
                      $receiver_det = User::where('id',$receiver->sender_id)->first();
                   } else {
                   	  $receiver_det = User::where('id',$receiver->receiver_id)->first();
                   }

                
                if($receiver_det->id != '')
                {
                  
                 $sender_id= $user_id; 
                 $receiver_id= $receiver_det->id; 

              $lastMsg = DB::table('tbl_messages')->latest()
            ->where(function ($query) use ($sender_id, $receiver_id){
		    $query->where('sender_id', $sender_id)->where('receiver_id', $receiver_id)->where('is_from_sender',0)->where('is_from_reciever',0);
			})->orWhere(function($query) use ($sender_id, $receiver_id){
			    $query->where('sender_id', $receiver_id)->where('receiver_id', $sender_id)->where('is_from_reciever',0)->where('is_from_sender',0);	
			})->latest()->first();
                }
             $total_unread = 0;
                if($receiver->bedspace_id != ''){
                  $bed = $receiver->bedspace_id;
                  $total_unread = Message::where('is_read', '=', 0)->where('sender_id', $sender_id)->where('receiver_id', $receiver_id)->where('is_from_sender',0)->where('bedspace_id',$bed)->where('is_from_reciever',0)->count();
                } else {
                  $total_unread = 0;
                }




			if($receiver_det->is_avatar == 1)
			{
				$check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$receiver_det->id)->first();

				$get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

				$receiver_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
				} 
			 else if(!empty($receiver_det->fld_profile_pic)){
				$receiver_img =config('app.url').'public/assets/profiles'.$receiver_det->fld_profile_pic;
			} else {
        $get_avtar = Avatar::inRandomOrder()->first();
        $receiver_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
      }

			if(!empty($receiver->path))
			{
				$addr = $receiver->path;
			} else {
				$addr = config('app.url')."public/assets/images/srech-room.png";
			}
            

                $bus = array(
                'id'               => $receiver->id,
                'bedspace_id'      => $receiver->bedspace_id,
                'sender_id'        => $receiver->sender_id,
                'receiver_id'      => $receiver->receiver_id,
                'total_unread'     => $total_unread,
              //  'addr'             => $addr,
                'message'          => $receiver->message,
                'image'            => $receiver->image,
                'is_from_sender'   => $receiver->is_from_sender,
                'is_from_reciever' => $receiver->is_from_reciever,
                'is_read'          => $receiver->is_read,
                'created_at'       => $receiver->created_at,
                'updated_at'       => $receiver->updated_at,
                'fld_name'         => trim($receiver->fld_custom_room_name),
                // 'room_id'          => $receiver->room_id,
                // 'RoomImage'        => $receiver->RoomImage,
                'receiver'         => $receiver_det,
                'lastMsg'          => $lastMsg,
                'receiver_img'     => $receiver_img,
              //  'rent'             => $rent,
                'path'             => $addr
                 );

               array_push($json, $bus);
               }
         

              $getAllUsers = array('user'=>$user, 'allreceiver'=>$json);
			return $getAllUsers;


		}	
		public function getAllUsers($id)
		{	
			if($id != 'message')
			{
             $id  = base64_decode($id);
             $id = (int)$id; 
			} else {
			 $id = Auth::user()->id;	
			}
            $alluser[] = $id;
            $user_id = Auth::user()->id;
 
		$allreceiver = DB::table('tbl_messages')
				->select('tbl_users.id')
				->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_messages.receiver_id')
				->where('tbl_messages.sender_id', $user_id)
				->groupBy('tbl_messages.receiver_id')->get();   

		$allsender = DB::table('tbl_messages')
				->select('tbl_users.id')
				->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_messages.sender_id')
				->where('tbl_messages.receiver_id', $user_id)
				->groupBy('tbl_messages.receiver_id')->get();
           

           foreach ($allreceiver as  $receiver) {
           	$alluser[] = $receiver->id;
           }
          

           foreach ($allsender as  $sender) {
           	$alluser[] = $sender->id;
           } 
             
          //return $alluser;
           $getAllUsers = DB::table('tbl_users')->whereIn('id',$alluser)->where('id','!=',$user_id)->groupBy('id')->get();
			return $getAllUsers;

		}
   

		/**
		 * Persist message to database
		 *
		 * @param  Request $request
		 * @return Response
		 */
		public function sendMessage(Request $request)
		{     
       include_once('send_notification.php');
      if($request->image !='' && $request->image != 'undefined')
      {   
         $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('chat'), $imageName);
      }else
      {
      	  $imageName = "";
      }


			$sender_id = $request->sender_id;
			$sender = User::where('id',$sender_id)->first();
			$receiverid = $request->receiver_id;


			$message = new Message();

			$message->message = $request->message;
			$message->sender_id = $sender_id;
			$message->receiver_id = $receiverid;
			$message->bedspace_id = $request->bedspace_id;
			$message->is_read = 'Sent';
			$message->image = $imageName;

			$message->save();
           

			 $conversation =new  Conversation();
	         $conversation->message = request('message');
	         $conversation->bedspace_id =$request->bedspace_id;
	         $conversation->status = 'Sent';
	         $conversation->user_id = $sender_id;
	         $conversation->save();
	    
           
          if(!empty($imageName)){
          	$img_msg = config('app.url').'public/chat/'.$imageName;
          } else {
          	$img_msg = '';
          }

            if($sender->is_avatar == 1)
			{
				$check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$sender->id)->first();

				$get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

				$sender_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
				} 
			 else {
				$sender_img =config('app.url').'public/assets/profiles'.$sender->fld_profile_pic;
			}
           
            $data = ['msg_data'=>$request->message,'is_read'=>$message->is_read, 'img_msg' =>$img_msg, 'sender'=>$sender, 'sender_img'=>$sender_img,'message_id' => $message->id];

			// broadcast(new MessageSent($user, "dfdffddfsdfd"))->toOthers();
			broadcast(new MessageSent($sender, $request->message,$data))->toOthers();


         // $data = array('msg'=>$request->message, 'user'=>$sender);
        event(new MyEvent($sender, $message));


         $message = $sender->fld_name .":". $request->message  ;

            $notification_type = 'new_message';
         
             $data= array('fld_sender_id'=>$sender->id, 'fld_receiver_id'=>$receiverid,
           'fld_user_type'=>'C','fld_noti_type'=>$notification_type,'fld_message'=>$message,'fld_subcom_id'=>0 );

         
              $noti =new Notification();

                       $noti->fld_sender_id = $data['fld_sender_id'];
                       $noti->fld_subcom_id = 0;
                       $noti->fld_receiver_id = $data['fld_receiver_id'];
                       $noti->fld_user_type = $data['fld_user_type'];
                       $noti->fld_noti_type = $data['fld_noti_type'];
                       $noti->fld_message =$request->message;

                       $noti->save();
                       
                       $rec =User::find($receiverid); 
                       $send = sendPushNotificationToFCMSever($rec,$message,$notification_type);

			return ['status' => 'ok', 'data' =>$data];
		}

		public function get_User($id){
          
          $sender = User::where('id',$id)->first();

            if($sender->is_avatar == 1)
			{
				$check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$sender->id)->first();

				$get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

				$sender_img = config('app.url').'public/assets/avatar/png/'.$get_avtar->name;
				} 
			 else {
				$sender_img =config('app.url').'public/assets/profiles'.$sender->fld_profile_pic;
			}


        $data = array('sender' => $sender, 'sender_img' =>$sender_img );
         return $data;
		}

    public function test_event(){
         event(new MyEvent('hello tttttttttt world'));

         return "Event sent Success";
    }
       

     public function FirstMsg(Request $request){
       
      //return $request->toArray();
        $message = new Message();
        $sender_id = $request->sender_id;
        
        $receiver = User::where('id',$request->owner_id)->first();

        $sender = User::where('id',$sender_id)->first();

      $message->message = $request->message;
      $message->sender_id = $sender_id;
      $message->receiver_id = $request->owner_id;
      $message->bedspace_id = $request->bedspace_id;
      $message->is_read = 'Sent';

      $data = '';

       if($message->save())
       {
         broadcast(new MessageSent($receiver, $message,$data))->toOthers();
         event(new MyEvent($sender, $message));
        
        $Response = array('status' => true,'message' => 'Successfull');


       } else {
        $Response = array('status' => false,'message' => 'error in saving Message');
       }

       return response()->json($Response);

        }
 

		public function conversations(Request $request){
  
         $con = DB::table('tbl_messages')
        		    ->where('bedspace_id', $request->bedspace_id)
        		    ->where('receiver_id', $request->receiver_id)
                ->where('sender_id', $request->sender_id)
        		    ->update([
		             'is_read' => 1
		            ]);
        $Response = array('status' => true,'message' => 'Successfull');
		     return response()->json($Response);
		}

 
}
