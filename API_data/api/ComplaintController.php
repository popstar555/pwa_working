<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PhoneVerifyRequest;
use DB;
use App\User;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\Property;
use App\States;
use App\Cities;
use App\Building;
use App\Notification;
use App\Bedspace;
use App\Booking_History;
use App\Complaint;
use App\SubComplaint;
use App\Room;
use App\RoomPic;
use App\PropertyDocument;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class ComplaintController extends Controller
{ 
  
        function add_complaint(Request $request){
                 
               include_once('send_notification.php');

                 $complaint = new Complaint();
                 
                 $complaint->fld_tenant_id = $request->fld_tenant_id;
                 $complaint->fld_booking_id = $request->fld_booking_id;
                 $complaint->fld_complaint_description = $request->description;
                         
                          //upload file
                    if ($request->hasFile('image1')) {

                        $image = $request->file('image1');
                        $name = 'comp1'.time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('complaint');
                        $image->move($destinationPath, $name);
                        $complaint->fld_attachment_one = $name;


                    }

                    if ($request->hasFile('image2')) {
                        $image = $request->file('image2');
                        $name = 'comp2'.time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('complaint');
                        $image->move($destinationPath, $name);
                        $complaint->fld_attachment_two = $name;
                    }

                    if ($request->hasFile('image3')) {
                        $image = 'comp3'.$request->file('image3');
                        $name = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('complaint');
                        $image->move($destinationPath, $name);
                        $complaint->fld_attachment_three = $name;
                    }

                 if($request->fld_presence_needed == 'true' || $request->fld_presence_needed == 'True' || $request->fld_presence_needed == 'TRUE')
                 {
                     $complaint->fld_prefer_time = $request->fld_prefer_time;
                     $complaint->fld_prefer_date = $request->fld_prefer_date;
                     $complaint->fld_presence_needed = '1';

                 } else{
                      $complaint->fld_presence_needed = '0';
                 } 


               if($complaint->save())
                 { 

                   $SubComplaint = new SubComplaint();
                                     
                   $SubComplaint->fld_complaint_id = $complaint->fld_id;
                   $SubComplaint->fld_complaint_type = $request->fld_complaint_type;
               
                  if($SubComplaint->save())
                     {   

                      $user = DB::table('tbl_users')->WHERE('id', 787)->first();
                      $client = DB::table('tbl_users')->WHERE('id', $request->fld_tenant_id)->first();

                       $json_data = array('status'=>true,'message'=>"Complaint Added Successfully.", 'complaint'=> $complaint,'subcomplaint'=>$SubComplaint);

                        $message = $client->fld_name .": Raised New Complaint";

                      $notification_type = 'Add_Complaint';
                   
                       $data= array('fld_sender_id'=>$client->id, 'fld_receiver_id'=>$user->id,
                     'fld_user_type'=>'A','fld_noti_type'=>$notification_type,'fld_message'=>$message,'fld_subcom_id'=>$SubComplaint->fld_id );
          
                     $noti =new Notification();

                       $noti->fld_sender_id = $data['fld_sender_id'];
                       $noti->fld_subcom_id = $SubComplaint->fld_id;
                       $noti->fld_receiver_id = $data['fld_receiver_id'];
                       $noti->fld_user_type = $data['fld_user_type'];
                       $noti->fld_noti_type = $data['fld_noti_type'];
                       $noti->fld_message = $message;

                       $noti->save();

                       $send = sendPushNotificationToFCMSever($user,$message,$notification_type);

                      } else {
                          $json_data = array('status'=>false,'message'=>"Error in Adding SubComplaint."); 
                      } 

                 } else {
                       $json_data = array('status'=>false,'message'=>"Error in Adding Complaint."); 
                     }

                  return response()->json($json_data);
                
                
        }


              function get_client_complaint($booking_id,$status){
                 

            $complaint_status = DB::table('tbl_booking_history')
         ->leftjoin('tbl_complaints', 'tbl_booking_history.fld_id', '=', 'tbl_complaints.fld_booking_id')
         ->leftjoin('tbl_sub_complaints', 'tbl_complaints.fld_id', '=', 'tbl_sub_complaints.fld_complaint_id')
         ->leftjoin('tbl_users', 'tbl_sub_complaints.assigned_to', '=', 'tbl_users.id')
         ->leftjoin('tbl_rooms', 'tbl_booking_history.fld_room_id', '=', 'tbl_rooms.fld_id')
         ->leftjoin('tbl_building', 'tbl_rooms.fld_building_id', '=', 'tbl_building.fld_id')
         ->leftjoin('tbl_property', 'tbl_building.property_id', '=', 'tbl_property.id')
            ->where('tbl_booking_history.fld_id',$booking_id)
            ->where('tbl_sub_complaints.is_closed',$status)
            ->select('tbl_complaints.*','tbl_sub_complaints.*', 'tbl_sub_complaints.fld_id AS sub_com_id', 'tbl_users.fld_name AS Assign_name', 'tbl_users.fld_number AS staff_num', 'tbl_rooms.fld_room_name','tbl_property.name AS Building_name')->orderBy('tbl_sub_complaints.fld_id', 'DESC')->distinct()->paginate(15);
                 
                  return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $complaint_status         
                ]);
                
                
        }

        function get_complaint_details($sub_com_id){
          $comp_detail =  DB::table('tbl_booking_history')
          ->leftjoin('tbl_complaints', 'tbl_booking_history.fld_id', '=', 'tbl_complaints.fld_booking_id')
          ->leftjoin('tbl_sub_complaints', 'tbl_complaints.fld_id', '=', 'tbl_sub_complaints.fld_complaint_id')
          ->leftjoin('tbl_users', 'tbl_sub_complaints.assigned_to', '=', 'tbl_users.id')
          ->leftjoin('tbl_rooms', 'tbl_booking_history.fld_room_id', '=', 'tbl_rooms.fld_id')
          ->leftjoin('tbl_building', 'tbl_rooms.fld_building_id', '=', 'tbl_building.fld_id')
          ->leftjoin('tbl_property', 'tbl_building.property_id', '=', 'tbl_property.id')
          ->where('tbl_sub_complaints.fld_id',$sub_com_id)
          ->select('tbl_complaints.*','tbl_sub_complaints.*', 'tbl_sub_complaints.fld_id AS sub_com_id', 'tbl_users.fld_name AS Assign_name', 'tbl_users.fld_number AS staff_num', 'tbl_rooms.fld_room_name','tbl_property.name AS Building_name','tbl_building.fld_address')->first();
                
                if(!empty($comp_detail->fld_attachment_one))
                {
                $comp1 = config('app.url').'public/complaint/'.$comp_detail->fld_attachment_one;
                } else {
                  $comp1 = '';
                }

                if(!empty($comp_detail->fld_attachment_two))
                {
                $comp2 = config('app.url').'public/complaint/'.$comp_detail->fld_attachment_two;
                } else {
                  $comp2 = '';
                }

                if(!empty($comp_detail->fld_attachment_three))
                {
                $comp3 = config('app.url').'public/complaint/'.$comp_detail->fld_attachment_three;
                } else {
                  $comp3 = '';
                }


            $json = array();
            $bus = array(

              "fld_id"=> $comp_detail->fld_id,
              "fld_complaint_id"=> $comp_detail->sub_com_id,
              "fld_complaint_type"=> $comp_detail->fld_complaint_type,
              "assigned_to"=> $comp_detail->assigned_to,
              "fld_complaint_description"=> $comp_detail->fld_complaint_description,
              "fld_prefer_time"=> $comp_detail->fld_prefer_time,
              "fld_prefer_date"=> $comp_detail->fld_prefer_date,
              "RoomName"=> $comp_detail->fld_room_name,
              "AreaName"=> $comp_detail->fld_address,
              "BuildingName"=> $comp_detail->Building_name,
              "Assign_name"=> $comp_detail->Assign_name,
              "staff_num"=> $comp_detail->staff_num,
              "status"=> $comp_detail->status,
              "is_closed"=> $comp_detail->is_closed,
              "remarks"=> $comp_detail->remarks,
              "fld_is_resolved_by"=> $comp_detail->fld_is_resolved_by,
              "Description"=> $comp_detail->fld_complaint_description,
              "fld_update_date"=> $comp_detail->fld_update_date,
              "fld_attachment_one"=> $comp1,
              "fld_attachment_two"=> $comp2,
              "fld_attachment_three"=> $comp3,
              "fld_resolved_image"=> $comp_detail->fld_resolved_image,
              "assigned_on"=> $comp_detail->assigned_on,
              "assigned_by"=> $comp_detail->assigned_by
             );
          array_push($json, $bus);

                 
                  return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $json,     
                ]);
        }

        public function close_complaint(Request $request){

           include_once('send_notification.php');
          $client_id  = $request->client_id;
          $status = $request->status;
          $sub_complaint_id = $request->complaint_id;
          
         $SubComplaint = SubComplaint::where('fld_id', $sub_complaint_id)->first();
          
          $SubComplaint->is_closed = $status;
          $SubComplaint->save();

          $user = User::where('id', $client_id)->first();

          $assigned_to =  $SubComplaint->assigned_to;
         
          $assigned_user = User::where('id', $assigned_to)->first();

         $message = "Complaint closed By ".$user->fld_name;

         $notification_type ="Complaint_closed_Admin";

        $data= array('fld_sender_id'=>$user->id, 'fld_receiver_id'=>$assigned_to,
        'fld_user_type'=>'A','fld_noti_type'=>$notification_type,'fld_message'=>$message, 'fld_subcom_id'=>$sub_complaint_id );

        $noti =new Notification();

        $noti->fld_sender_id = $data['fld_sender_id'];
        $noti->fld_subcom_id = $sub_complaint_id;
        $noti->fld_receiver_id = $data['fld_receiver_id'];
        $noti->fld_user_type = $data['fld_user_type'];
        $noti->fld_noti_type = $data['fld_noti_type'];
        $noti->fld_message = $message;

        $noti->save();

        $send = sendPushNotificationToFCMSever($assigned_user,$message,$notification_type);
          
                return response()->json([
                'status'  => true,
                'message' => 'Complaint Closed Successfully'
           
                ]);
        }

        public function edit_complaint(Request $request){
             include_once('send_notification.php');
          $prefer_time = $request->prefer_time;
          $prefer_date = $request->prefer_date;
          $complaint_id = $request->complaint_id;
          $sub_com_id = $request->sub_com_id;
          
          $Complaint = Complaint::where('fld_id', $complaint_id)->first();


          $Complaint->fld_prefer_time = $prefer_time;
          $Complaint->fld_prefer_date = $prefer_date;
          $Complaint->save();

          $SubComplaint = SubComplaint::where('fld_id', $sub_com_id)->first();
          $SubComplaint->is_accepted = 0;
          $SubComplaint->save();

          $tenent_id = $Complaint->fld_tenant_id;

          $user = User::where('id', $tenent_id)->first();
         //dd($user);
          $notification_type = "Edit_Complaints_Admin";

          $message = "Date And Time of Complaint Raised By :".$user->fld_name." changed";

          $assigned_user = User::where('id', $SubComplaint->assigned_to)->first();
          
          $data= array('fld_sender_id'=>$tenent_id, 'fld_receiver_id'=>$SubComplaint->assigned_to,
                     'fld_user_type'=>'A','fld_noti_type'=>$notification_type,'fld_message'=>$message,'fld_subcom_id'=>$sub_com_id );

          $noti =new Notification();

          $noti->fld_sender_id = $data['fld_sender_id'];
          $noti->fld_subcom_id = $data['fld_subcom_id'];
          $noti->fld_receiver_id = $data['fld_receiver_id'];
          $noti->fld_user_type = $data['fld_user_type'];
          $noti->fld_noti_type = $data['fld_noti_type'];
          $noti->fld_message = $message;

          $noti->save();

          $send = sendPushNotificationToFCMSever($assigned_user,$message,$notification_type);
          
          return response()->json([
          'status'  => true,
          'message' => 'Complaint Edited Successfully'

          ]);
        }

}