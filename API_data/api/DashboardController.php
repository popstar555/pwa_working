<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PhoneVerifyRequest;
use DB;
use App\User;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\Property;
use App\States;
use App\Cities;
use App\Building;
use App\Bedspace;
use App\Notification;
use App\Booking_History;
use App\Complaint;
use App\SubComplaint;
use App\Room;
use App\AvatarAssigned;
use App\RoomPic;
use App\PropertyDocument;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class DashboardController extends Controller
{ 

	function total_budget($id){
     
      $company_id = User::where('id',$id)->first();

     if($id == 610 || $id == 787)
     {

           $id = [];
     	   $owner = DB::table('tbl_building')
                     ->join('tbl_users', 'tbl_building.fld_tanent', '=', 'tbl_users.id')
                     ->where('tbl_users.fld_company_id', $company_id->fld_company_id)
                     ->where('tbl_users.fld_user_type', 'A')
                     ->where('tbl_building.fld_isactive',1)
                     ->select('tbl_users.id','tbl_users.fld_name')
                     ->groupBy('tbl_users.id')
                     ->get();

            foreach($owner AS $data)
            {
             array_push($id,$data->id);
            }

         
     }  
      	   $budget = DB::table('tbl_rooms')
                 ->join('tbl_building', 'tbl_rooms.fld_building_id', '=', 'tbl_building.fld_id')
                 ->join('tbl_users', 'tbl_building.fld_tanent', '=', 'tbl_users.id')
                 ->whereIn('tbl_users.id',$id)
                 ->where('fld_isactive',1)
                 ->where('added_from',2)
                 ->where('tbl_users.fld_company_id',$company_id->fld_company_id)
                 ->sum('tbl_rooms.fld_expected_rent');
    
      $rented = DB::table('tbl_booking_history')
                ->join('tbl_bedspace', 'tbl_bedspace.fld_id', '=', 'tbl_booking_history.fld_bedspace_id')
                ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_bedspace.fld_building_id')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
                ->where('tbl_building.fld_isactive', 1)
                ->where('tbl_building.added_from',2)
                ->where('tbl_users.fld_company_id',$company_id->fld_company_id)
                ->where('tbl_bedspace.fld_is_rented',1)
                ->where('tbl_bedspace.fld_block_unblock',1)
                ->where('tbl_booking_history.fld_is_current_tanent',1)
                ->whereIn('tbl_users.id',$id)
                ->select('tbl_booking_history.fld_rent')
                ->groupBy('tbl_bedspace.fld_id')
                ->get();
           $rent = 0;
          foreach ($rented as  $value) {
          $rent += $value->fld_rent;
          }

          echo $rent;
          die;

       $data = array('data' => $rented);
 
	// return response()->json([
	// 'status'  => true,
	// 'message' => 'success',
	// 'data'=> $data         
	// ]);


	}


   public function get_notification($user_id)
   {
         $target_path = config('app.url').'public/assets/profiles/';
         $complaint_path = config('app.url').'public/complaint/';

         $noti = DB::table('tbl_notification')
                 ->join('tbl_users','tbl_users.id', '=', 'tbl_notification.fld_receiver_id')
                 ->leftjoin('tbl_sub_complaints', 'tbl_sub_complaints.fld_id', '=', 'tbl_notification.fld_subcom_id')
                 ->leftjoin('tbl_complaints', 'tbl_complaints.fld_id', '=', 'tbl_sub_complaints.fld_complaint_id')
                 ->where('fld_receiver_id',$user_id)
                 ->orderBy('fld_id', 'DESC')
                 ->select('tbl_notification.*', DB::raw("CONCAT('$target_path',tbl_users.fld_profile_pic) AS tanent_pic"),DB::raw("CONCAT('$complaint_path',tbl_complaints.fld_attachment_one) AS complaint_one"),DB::raw("CONCAT('$complaint_path',tbl_complaints.fld_attachment_two) AS complaint_two"),  DB::raw("CONCAT('$complaint_path',tbl_complaints.fld_attachment_three) AS complaint_three"))
                 ->paginate(8);
         
       //dd($noti);

        return response()->json([
        'status'  => true,
        'message' => 'success',
        'data'=> $noti         
        ]);
   }
 
   public function get_avatar(){
              $target_path = config('app.url').'public/assets/avatar/png/';
          $avatar = DB::table('tbl_avatars')->select('*', DB::raw("CONCAT('$target_path',tbl_avatars.name) AS image"))->get();
        return response()->json([
        'status'  => true,
        'message' => 'success',
        'data'=> $avatar         
        ]);
    }  

   public function delete_booking($booking_id){

          $booking = Booking_History::where('fld_id',$booking_id)->first();
          $booking->is_deleted = 1;
          $booking->save();

        return response()->json([
        'status'  => true,
        'message' => 'success'       
        ]);
    } 

   public function update_avatar($user_id,$avatar_id){
          
      $user = User::where('id',$user_id)->first();
        $user->is_avatar = 1;
        $user->save();

        
        $delete = AvatarAssigned::where('user_id',$user_id)->delete();
         $Avatar = new AvatarAssigned();

         $Avatar->avatar_id = $avatar_id;
         $Avatar->user_id = $user_id;
         $Avatar->save(); 

        return response()->json([
        'status'  => true,
        'message' => 'Avatar Selected Successfully'      
        ]);
    }

}