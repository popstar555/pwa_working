<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MetaRepository;
use DB;
use Auth;
use App\RoomPic;
use App\Language;
use App\Location;
use App\Property;
use App\Newsletter;
use App\Booking_History;
use App\Searchterm;
use App\View_Lead;
use App\Avatar;
use App\User;
use App\Room;
use Mail;
use App\Building;
use App\Cities;

class EntirePropertyController extends Controller{
      
             public function __construct(MetaRepository $metarepository)
    {
        $this->metarepository = $metarepository;
    }


 public function entirepropertyBYFilter(Request $request){
              // DB::enableQueryLog();
                 

            $sort =  $request->sort_by;

             $json = array();


            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            ->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')
            ->where(function($query) {
                return $query->where('tbl_bedspace.fld_is_notice',1)
                    ->orWhere('tbl_bedspace.fld_is_rented',0);
            })
                
            ->where('tbl_bedspace.fld_block_unblock',1)
            //->where('tbl_rooms.fld_approval_waiting',1)
            ->where('tbl_rooms.is_listing_approved',1)
            ->where('tbl_building.accomodation_type',1)
            ->where('tbl_building.fld_isactive',1);
       

             if(!empty($sort))
             { 
                if($request->sort_by == 'priceHL')
                {

                 $aparts->orderBy('tbl_bedspace.fld_expected_rent', 'DESC');
                } else if($request->sort_by =='priceLH')
                  {
                      $aparts->orderBy('tbl_bedspace.fld_expected_rent', 'ASC');

                  } else if($request->sort_by =='metroLH')
                  {
                           $aparts->orderBy('tbl_rooms.metro_train', 'DESC');

                  } else if($request->sort_by =='metroHL')
                  {
                        $aparts->orderBy('tbl_rooms.metro_train', 'ASC');
                  } 
             } else {
                 $aparts->orderBy('tbl_bedspace.fld_id', 'DESC');
             }
            


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_rooms.fld_custom_room_name as custom_room_name','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_users.is_avatar','tbl_rooms.room_mates_male','tbl_rooms.room_mates_female','tbl_rooms.shared_washroom_amenities','tbl_rooms.private_washroom_amenities','tbl_rooms.fld_custom_room_name','tbl_rooms.avalability_date','tbl_rooms.other_house_rules','tbl_rooms.*','tbl_countries.currency','tbl_users.id AS user_id','tbl_building.property_type','tbl_building.property_size','tbl_building.no_of_rooms','tbl_building.no_of_parking');
            //->paginate(15);
            if(!empty($request->max_search_price) &&  !empty($request->min_search_price))
               {  $aparts->where('tbl_bedspace.fld_expected_rent','<=',$request->max_search_price)
                     ->where('tbl_bedspace.fld_expected_rent','>=',$request->min_search_price);
              }
            else if($request->min_search_price != '' && empty($request->max_search_price)){
               $aparts->where('tbl_bedspace.fld_expected_rent','>=',$request->min_search_price);
            } else if($request->max_search_price != '' && empty($request->min_search_price))
            {
                $aparts->where('tbl_bedspace.fld_expected_rent','<=',$request->max_search_price);
            }
               
               // echo $request->location_id;
               // die('ll');

            
                       //       if($request->has('city_id'))
           //              {
           //         if($request->has('city_id'))
           //      if($request->city_id != '')
           //      {
           //           $building = Location::select('name')->WHERE('id', $request->city_id)->first();
           //            $aparts->where('tbl_building.fld_area','LIKE',$building->name);
           //      }
           // }


                 if(!empty($request->city_id[0]))
                 {
                     $data= json_decode($request->city_id);

                        foreach ($data as  $value) {
                          $city[] = $value->name;
                        }
                      $city= implode(',', $city);

                        //print_r($city);

                    if(!empty($city)){
                       $aparts->where('tbl_building.fld_area','LIKE','%'.$city);
                    }

                  }
              
                        if($request->has('location_id'))
                        {
                    if(!empty($request->location_id))
                if($request->location_id != '')
                    $aparts->where('tbl_building.city_id',$request->location_id);
                       }

                   if($request->has('property_id'))
                if($request->property_id != '')
                    $aparts->where('tbl_building.property_id',$request->property_id);




                if($request->has('gender'))
                {
                   if($request->gender == 'M')
                   {
                     $aparts->where('tbl_rooms.gender','!=','F');
                   }  else if($request->gender == 'F')
                        {
                        $aparts->where('tbl_rooms.gender','!=','M');
                        } else if($request->gender == 'M/F') {
                     $aparts->where('tbl_rooms.occupancy', '>', '1');
                     $aparts->where('tbl_rooms.gender', 'M/F');
                             
                        }
                }

            if($request->has('select_balconies'))
                if($request->select_balconies != '')
                    $aparts->where('tbl_rooms.balconices',$request->select_balconies);

            // if($request->has('select_bath'))
            //     if($request->select_bath != '')

            //         $aparts->where('tbl_rooms.bath',$request->select_bath);

             if($request->has('shared_bath') && $request->has('attached_bath'))
             {

             } else {

            if($request->has('shared_bath'))
            if($request->shared_bath == 'true' || $request->shared_bath === true )
        
                $aparts->where('tbl_rooms.bath', 'Shared Bath');

            if($request->has('attached_bath'))
                if($request->attached_bath == 'true' || $request->attached_bath === true )
            
                    $aparts->where('tbl_rooms.bath', 'Attached Bath');
             }
            

            if($request->has('select_window'))
                if($request->select_window != '')
                    $aparts->where('tbl_rooms.window',$request->select_window);

            if($request->has('select_common_room'))
                if($request->select_common_room != '')
                    $aparts->where('tbl_rooms.common_room',$request->select_common_room);

          //  if($request->has('bedspace'))
                if($request->bedspace === 'true' || $request->bedspace === true )
                {
                    $aparts->where('tbl_bedspace.fld_type','B');
                }

            if($request->room != '')
            {
                if($request->room === 'true' || $request->room === true )
                {
                    $aparts->where('tbl_bedspace.fld_type','R');
                }

            }

            // if($request->has('bed_type'))
            //     if($request->bed_type != '')
            //         $aparts->where('tbl_rooms.bed_type',$request->bed_type);

            if($request->has('hr_smoking'))
                if($request->hr_smoking != '')
                    if($request->hr_smoking == 'true')
                        $aparts->where('tbl_building.house_rules_smoker',1);

            if($request->has('hr_couples'))
                if($request->hr_couples != '')
                    if($request->hr_couples == 'true')
                        $aparts->where('tbl_building.house_rules_couple',1);

            if($request->has('hr_pets'))
                if($request->hr_pets != '')
                    if($request->hr_pets == 'true')
                        $aparts->where('tbl_building.house_rules_pet',1);

            if($request->has('hr_children'))
                if($request->hr_children != '')
                    if($request->hr_children == 'true')
                        $aparts->where('tbl_building.house_rules_children',1);

            if($request->has('hr_balcony'))
                if($request->hr_balcony != '')
                    if($request->hr_balcony == 'true')
                        $aparts->where('tbl_rooms.private_balcony_amenities',1);

            if($request->has('hr_window'))
                if($request->hr_window != '')
                  if($request->hr_window == 'true')
                    $aparts->where('tbl_rooms.window_amenities',1);

            if($request->has('hr_tv'))
                if($request->hr_tv != '')
                    if($request->hr_tv == 'true')
                        $aparts->where('tbl_rooms.tv_amenities',1);

            if($request->has('hr_fridge'))
                if($request->hr_fridge != '')
                    if($request->hr_fridge == 'true')
                        $aparts->where('tbl_rooms.fridge_amenities',1);

            if($request->has('hr_wifi'))
                if($request->hr_wifi != '')
                    if($request->hr_wifi == 'true')
                        $aparts->where('tbl_rooms.wifi_amenities',1);

            if($request->has('hr_parking'))
                if($request->hr_parking != '')
                    if($request->hr_parking == 'true')
                        $aparts->where('tbl_building.parking_amenities',1);

            if($request->has('hr_elevator'))
                if($request->hr_elevator != '')
                    if($request->hr_elevator == 'true')
                        $aparts->where('tbl_building.elevator_amenities',1);

            if($request->has('hr_furnished_room'))
                if($request->hr_furnished_room != '')
                    if($request->hr_furnished_room == 'true')
                        $aparts->where('tbl_rooms.furnished_room_amenities',1);

            if($request->has('hr_pool'))
                if($request->hr_pool != '')
                    if($request->hr_pool == 'true')
                        $aparts->where('tbl_building.pool_amenities',1);

            if($request->has('hr_washing'))
                if($request->hr_washing != '')
                    if($request->hr_washing == 'true')
                        $aparts->where('tbl_rooms.washing_amenities',1);

            if($request->has('hr_common_balcony'))
                if($request->hr_common_balcony != '')
                    if($request->hr_common_balcony == 'true')
                        $aparts->where('tbl_rooms.balcony_amenities',1);

            if($request->has('natural_light'))
                if($request->natural_light != '')
                    if($request->natural_light == 'true')
                        $aparts->where('tbl_rooms.natural_light_amenities',1);

            if($request->has('dishwasher_amenity'))
                if($request->dishwasher_amenity != '')
                    if($request->dishwasher_amenity == 'true')
                        $aparts->where('tbl_rooms.dishwasher_amenities',1);

            if($request->has('wheelchair_amenity'))
                if($request->wheelchair_amenity != '')
                    if($request->wheelchair_amenity == 'true')
                        $aparts->where('tbl_rooms.wheelcheer_amenities',1);

            if($request->has('garden_amenity'))
                if($request->garden_amenity != '')
                    if($request->garden_amenity == 'true')
                        $aparts->where('tbl_rooms.garden_amenities',1);

            if($request->has('terrace_amenity'))
                if($request->terrace_amenity != '')
                    if($request->terrace_amenity == 'true')
                        $aparts->where('tbl_rooms.terrace_amenities',1);

            if($request->has('living_room'))
                if($request->living_room != '')
                    if($request->living_room == 'true')
                        $aparts->where('tbl_rooms.living_room_amenities',1);

            if($request->has('other_house_rules'))
                if($request->other_house_rules != '')
                        $aparts->where('tbl_rooms.other_house_rules',$request->other_house_rules);

             $aparts_count= $aparts->count();
            $aparts = $aparts->paginate(8);
          
            // $dblog = DB::getQueryLog($aparts);

            // print_r($dblog);
            // die('mar');

       foreach ($aparts as  $apt) {
           
           if(!empty($apt->fld_tanent_id))
           {
             $notice_data =  DB::table('tbl_notice')->where('tenent_id', $apt->fld_tanent_id)->first();
           }
          
     
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->first();
    
     $target_path = config('app.url').'public/room_pic';
             
         
             if(empty($roomdata))
             {
               $room_image = $target_path.'/property.jpg';
             } else {
                $room_image = $target_path.'/'.$roomdata->fld_name;
             }



 

          $path = $this->domain =config('app.url');
           
           if($apt->is_avatar == 0)
           {
         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = $path.'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic =  $path.'public/assets/profiles/user.jpeg';
         }
          } else {
               $check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$apt->user_id)->first();

                 $get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

                 if(!empty($get_avtar->name))
               {
                  $fld_profile_pic = $path.'public/assets/avatar/png/'.$get_avtar->name; 

               } else{

                $fld_profile_pic = $path.'public/assets/profiles/user.jpeg';
               }
          }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }
            
            $room_title = preg_replace('/\s+/', ' ', $apt->fld_custom_room_name);
            $room_notes = preg_replace('/\s+/', ' ', $apt->room_notes);

            // if(!empty($notice_data))
            // {
            //       $avalability_date = $notice_data->move_out_date;
            // } elseif(!empty($apt->avalability_date) && $apt->avalability_date != '0000-00-00') {
               
            //      $avalability_date = $apt->avalability_date;
            // } else {
            //      $avalability_date = 'From Today';
            // }

            $notice = Booking_History::where('fld_bedspace_id',$apt->bedspace_id)->orderBy('fld_id', 'desc')->first();
          
            $date = date("Y-m-d");
           if($apt->avalability_date !='' && $apt->avalability_date != '0000-00-00' && strtotime($date) < strtotime($apt->avalability_date))
           {
             $avalability_date  = $apt->avalability_date;
           } else {
                if(!empty($notice))
                       {
                              if($notice->fld_is_notice == 1 && $notice->fld_move_out_date != '0000-00-00')
                              { 
                                 if(strtotime($date) < strtotime($notice->fld_move_out_date))
                                 {
                                     $avalability_date = $notice->fld_move_out_date;    
                                  } else {
                                     $avalability_date = "Today";
                                         }
                              } else {
                                $avalability_date = "Today";
                              } 
                        }  else {
                           $avalability_date = "Today";
                        }
           }


        if($apt->private_washroom_amenities == 1)
        {
          $bath_type = 'Private washroom';
        } else {
          $bath_type = 'Shared washroom';
        }
         
         $pagination_data = array('current_page' => $aparts->currentPage(), 'lastPage' => $aparts->lastPage(), 'previousPageUrl' => $aparts->previousPageUrl(), 'nextPageUrl' => $aparts->nextPageUrl() );

                $bus = array(
                'Area'                 => $apt->Area,
                'property_size'        => $apt->property_size,
                'property_type'        => $apt->property_type,
                'no_of_rooms'          => $apt->no_of_rooms,
                'no_of_parking'        => $apt->no_of_parking,
                'building'             => $apt->building,
                'building_id'          => $apt->building_id,
                'apt_no'               => $apt->apt_no,
                'approved'             => $apt->approved,
                'room_name'            => $apt->room_name,
                'room_id'              => $apt->room_id,
                'room_notes'           => $room_notes,
                'custom_room_name'     => $room_title,
                'bedspace_id'          => $apt->bedspace_id,
                'fld_tanent_id'        => $apt->fld_tanent_id,
                'notice'               => $apt->notice,
                'is_rented'            => $apt->is_rented,
                'bedspace_expected_rent'  => $apt->bedspace_expected_rent,
                'currency'             => $apt->currency,
                'name'                    => $apt->name,
                'private_washroom_amenities'   => $apt->private_washroom_amenities,
                'shared_washroom_amenities'   => $apt->shared_washroom_amenities,
                'room_expected_rent'      => $apt->room_expected_rent,
                'room_title'              => $room_title,
                'bath_type'                  => $bath_type,
                'room_images'             => $room_image,
                'fld_profile_pic'         => $fld_profile_pic,
                'rent'                    => $apt->bedspace_expected_rent,
                'room_mates_female'          => $apt->room_mates_female,
                'room_mates_male'            => $apt->room_mates_male,
                'other_house_rules'            => $apt->other_house_rules,
                'avalability_date'            => $avalability_date,
                // 'building_room_image'             => $room_building_image,
                'fld_type'                => $apt->fld_type);

               array_push($json, $bus);

     }    
         if($aparts_count > 0)
         {
                      return response()->json([
                        'status'  => true,
                        'total'   => $aparts_count,
                        'pagination'   => $pagination_data,
                        'message' => 'Successfully',
                        'data'    => $json,
                         ]); 
                    } else {
             return response()->json([
                        'status'  => true,
                        'total'   => $aparts_count,
                        'pagination'   => null,
                        'message' => 'Successfully',
                        'data'    => $json,
                         ]); 
                    }

         
        }
       public function get_property_detail($id){
            
            $json = array();
            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            //  ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            // ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            ->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')

             ->where('tbl_bedspace.fld_id',$id);


             $aparts->orderBy('tbl_building.added_from', 'desc');


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_building.*','tbl_rooms.*','tbl_users.fld_number','tbl_users.country_code','tbl_users.fld_secondary_number','tbl_countries.currency','tbl_users.id AS Owner_id');
            //->paginate(15);
            $aparts = $aparts->get();

       foreach ($aparts as  $apt) {

           if(!empty($apt->fld_tanent_id))
        {
        $notice_data =  DB::table('tbl_notice')->where('tenent_id', $apt->fld_tanent_id)->first();
        }

       $bedcount = DB::table('tbl_bedspace')->where('fld_block_unblock','1')->where('fld_room', $apt->room_id)->count('*');   

      // dd($bedcount); 
      // echo $bedcount;
      // die;
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->get();
    
     $target_path = config('app.url').'public/room_pic/';
     $room_image = [];
       foreach ($roomdata as  $image_data) {
        
             
             if(!empty($image_data))
            {
                $room_image[] = $target_path.$image_data->fld_name;
             }
  
        } 
        
        if(empty($room_image[0]))
        {
             $room_image = [];
        }


      $propertydata = DB::table('tbl_property_documents')->where('fld_property', $apt->building_id)->get();
    
      $target_path = config('app.url').'public/PROPERTY_DOC/';
      


       if(!empty($propertydata[0]))
       {


         foreach ($propertydata as  $image_data) {
          
       
             if(!empty($image_data))
           {
                $buliding_image[] = $target_path.$image_data->fld_name;
             }
  
         } 
     } else {
                  $buliding_image = [];
     }

         $room_building_image = array_merge($room_image,$buliding_image);

      if($apt->fld_type == 'R')
       {
         $rent = $apt->room_expected_rent; 
       } else {
          $rent = $apt->bedspace_expected_rent; 
       }

           $path = $this->domain =config('app.url');

         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = $path.'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }


         if($apt->bills_included == 1)
         {
            $bills_included = 'All Bills Inclusive';
         } else {
            $bills_included = 'Bills are not Included';
         } 

          $fld_room_description = $apt->fld_room_description;

                $notice = Booking_History::where('fld_bedspace_id',$apt->bedspace_id)->orderBy('fld_id', 'desc')->first();
          
            $date = date("Y-m-d");
           if($apt->avalability_date !='' && $apt->avalability_date != '0000-00-00' && strtotime($date) < strtotime($apt->avalability_date))
           {
             $avalability_date  = $apt->avalability_date;
           } else {
                if(!empty($notice))
                       {
                              if($notice->fld_is_notice == 1 && $notice->fld_move_out_date != '0000-00-00')
                              { 
                                 if(strtotime($date) < strtotime($notice->fld_move_out_date))
                                 {
                                     $avalability_date = $notice->fld_move_out_date;    
                                  } else {
                                     $avalability_date = "Today";
                                         }
                              } else {
                                $avalability_date = "Today";
                              } 
                        }  else {
                           $avalability_date = "Today";
                        }
           }

           

          if($apt->added_from == 1 ) 
          {
            $room_males = $apt->room_mates_male; 
          } else {

             if($apt->fld_type == 'R')
             {

                $room_males = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_building_id', $apt->building_id)
                ->where('tbl_users.fld_sex','M')
                ->count('tbl_bedspace.fld_id');
            } else {
                 $room_males = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_room', $apt->room_id)
                ->where('tbl_users.fld_sex','M')
                ->count('tbl_bedspace.fld_id'); 
            }

          }


      // echo $total_females;



          
          if($apt->added_from == 1 ) 
          {
            $room_females = $apt->room_mates_female; 
          } else {

                if($apt->fld_type == 'R')
             {

                $room_females = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_building_id', $apt->building_id)
                ->where('tbl_users.fld_sex','F')
                ->count('tbl_bedspace.fld_id');

                } else {
                 $room_females = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_room', $apt->room_id)
                ->where('tbl_users.fld_sex','F')
                ->count('tbl_bedspace.fld_id');
                }


          }
       
         // if($apt->added_from == 1 ) 
         //  {
         //    $occupancy = $apt->occupancy; 
         //  } else {
         //    $occupancy =  $room_males + $room_females;
         //  }

             

       if(!empty($apt->fld_number))
       {

       // $contact = '+'.$apt->country_code.$apt->fld_number;

        if (substr($apt->fld_number, 0, strlen($apt->country_code)) == $apt->country_code) {
        $contact = substr($apt->fld_number, strlen($apt->country_code));
        $contact = '+'.$apt->country_code.$contact;
        }  else {
          $contact = '+'.$apt->country_code.$apt->fld_number;
        }


       }else {
        $contact = '+'.$apt->country_code.$apt->fld_secondary_number;
       }
        
        if($apt->private_washroom_amenities == 1)
        {
          $bath_type = 'Private washroom';
        } else {
          $bath_type = 'Shared washroom';
        }


         
       if(!empty($apt->fld_room_description))
      {
          $description = trim($apt->fld_room_description);   
      } else if(!empty($apt->fld_custom_room_name)){
           $description = trim($apt->fld_custom_room_name); 
      }  else {
          $description = "Now Enjoy Lexurious".$bedtype." in just". $rent . "/month";
      } 

        if(is_numeric($apt->room_deposit))
        {
            $deposit = $apt->room_deposit / $bedcount;
          } else {
            $deposit = 0;
          }
     


                $bus = array(
                'Area'                        => $apt->Area,
                'no_of_washrooms'             => $apt->no_of_washrooms,
                'no_of_rooms'                 => $apt->no_of_rooms,
                'no_of_parking'               => $apt->no_of_parking,
                'property_size'               => $apt->property_size,
                'occupancy'                   => $apt->occupancy,
                'room_males'                  => $room_males,
                'room_females'                => $room_females,
                'building'                    => $apt->building,
                'building_id'                 => $apt->building_id,
                'property_type'               => $apt->property_type,
                'fld_address'                 => $apt->fld_address,
                'fld_latitude'                => $apt->fld_latitude,
                'fld_longitude'               => $apt->fld_longitude,
                'parking_amenities'           => $apt->parking_amenities,
                'elevator_amenities'          => $apt->elevator_amenities,
                'pool_amenities'              => $apt->pool_amenities,
                'house_rules_smoker'          => $apt->house_rules_smoker,
                'house_rules_pet'             => $apt->house_rules_pet,
                'house_rules_couple'          => $apt->house_rules_couple,
                'house_rules_children'        => $apt->house_rules_children,
                'selected_listing'            => $apt->selected_listing,
                'table_tennis'                => $apt->table_tennis,
                'gym'                         => $apt->gym,
                'Owner_id'                    => $apt->Owner_id,
                'lawn_tennis'                 => $apt->lawn_tennis,
                'squash'                      => $apt->squash,
                'approved'                    => $apt->approved,
                'room_name'                   => $apt->room_name,
                'room_id'                     => $apt->room_id,
                'bedspace_id'                 => $apt->bedspace_id,
                'fld_tanent_id'               => $apt->fld_tanent_id,
                'room_title'                  => trim($apt->fld_custom_room_name),
                'balconices'                  => $apt->balconices,
                'metro_train'                 => $apt->metro_train,
                'currency'                    => $apt->currency,
                'gender'                      => $apt->gender,
                'common_room'                 => $apt->common_room,
                'avalability_date'            => $avalability_date,
                'room_deposit'                => $deposit,
                'bills_included'              => $bills_included,
                'minimum_stay'                => $apt->minimum_stay,
                'display_pic'                 => $apt->display_pic,
                'other_house_rules'           => $apt->other_house_rules,
                'shared_washroom_amenities'   => $apt->shared_washroom_amenities,
                'private_washroom_amenities'  => $apt->private_washroom_amenities,
                'living_room_amenities'       => $apt->living_room_amenities,
                'fridge_amenities'            => $apt->fridge_amenities,
                'window_amenities'            => $apt->window_amenities,
                'private_balcony_amenities'   => $apt->private_balcony_amenities,
                'fld_room_description'        => trim($description),
                'photos_listing_title'        => $apt->photos_listing_title,
                'terrace_amenities'           => $apt->terrace_amenities,
                'garden_amenities'            => $apt->garden_amenities,
                'wheelcheer_amenities'        => $apt->wheelcheer_amenities,
                'dishwasher_amenities'        => $apt->dishwasher_amenities,
                'natural_light_amenities'     => $apt->natural_light_amenities,
                'balcony_amenities'           => $apt->balcony_amenities,
                'furnished_room_amenities'    => $apt->furnished_room_amenities,
                'washing_amenities'           => $apt->washing_amenities,
                'wifi_amenities'              => $apt->wifi_amenities,
                'tv_amenities'                => $apt->tv_amenities,
                'room_mates_female'           => $apt->room_mates_female,
                'room_mates_male'             => $apt->room_mates_male,
                'name'                        => $apt->name,
                'bath'                        => $bath_type,
                'room_expected_rent'          => $apt->room_expected_rent,
                'room_images'                 => $room_image,
                'fld_profile_pic'             => $fld_profile_pic,
                'contact'                     => $contact,
                'rent'                        => $rent,
                'bedtype'                     => $bedtype,
                'building_room_image'        => $room_building_image,
                'property_share_url'         => config('app.url').'flatshare/'.$id,
                'fld_type'                    => $apt->fld_type);

               array_push($json, $bus);

     }
            return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
  }


  public function entire_step_one(Request $data){

   //return $request->toArray();
        $Property = new Building;
        $Property->property_type = $data->property_type;
        if($data->property_type == 'apartment' && $data->property_id != '')
        {

        $Property->property_id =$data->property_id;
        $prop = DB::table('tbl_property')->where('id',$data->property_id)->first();
        //$Property->fld_building = $prop->name;
        }


     $area = DB::table('tbl_location')->where('id',$data->fld_area)->first();
     //$Property->fld_area = $area->name;
     $Property->fld_address = $data->fld_address;
     $Property->fld_latitude = $data->fld_latitude;
     $Property->fld_longitude = $data->fld_longitude;
     $Property->fld_tanent = $data->fld_tanent;
     $Property->added_from = 1;
     $Property->fld_isactive = 1;
     $Property->accomodation_type = 1;
     $Property->fld_num_of_beds = 1;
     $Property->fld_is_deleted  = 0;
     $Property->city_id  = $data->city_id;
       
     if($Property->save())
     {
        return response()->json([
            'status' =>true,
            'id'    =>$Property->fld_id,
            'message' => 'Building Added SuccessFully'


        ]);
     }
     else
     {
          return response()->json([
            'status' =>false,
             'message' => '!OOPS, Some Error Occoured'
        ]);
     }

  }


  public function entire_edit_step_one(Request $data)
  {
        $Property = new Building;
        $Property->property_type = $data->property_type;
        if($data->property_type == 'apartment' && $data->property_id != '')
        {

        $Property->property_id =$data->property_id;
        $prop = DB::table('tbl_property')->where('id',$data->property_id)->first();
        //$Property->fld_building = $prop->name;
        }


     $area = DB::table('tbl_location')->where('id',$data->fld_area)->first();
     //$Property->fld_area = $area->name;
     $Property->fld_address = $data->fld_address;
     $Property->fld_latitude = $data->fld_latitude;
     $Property->fld_longitude = $data->fld_longitude;
     $Property->fld_tanent = $data->fld_tanent;
     $Property->added_from = 1;
     $Property->fld_isactive = 1;
     $Property->accomodation_type = 1;
     $Property->fld_num_of_beds = 1;
     $Property->fld_is_deleted  = 0;
     $Property->city_id  = $data->city_id;
       
     if($Property->save())
     {
        return response()->json([
            'status' =>true,
            'id'    =>$Property->fld_id,
            'message' => 'Building Edited SuccessFully'


        ]);
     }
     else
     {
          return response()->json([
            'status' =>false,
             'message' => '!OOPS, Some Error Occoured'
        ]);
     }
  }



  public function entire_step_two(Request $data)
  {


    $building = Building::find($data->property_id);

    $building->property_id = $data->property_id;
    $building->property_size = $data->property_size;
    $building->no_of_parking = $data->no_of_parking;
    $building->no_of_rooms =   $data->no_of_rooms;
    $building->no_of_washrooms =   $data->no_of_washrooms;

 
    $Property = new Room;
    $Property->occupancy = 1;

             if(!empty($_REQUEST['house_amenities']))
                 {
                   $house_ameneties = $_REQUEST['house_amenities'];
                   $house = explode(',', $house_ameneties);
                 } else {
                    $house = [];
                 }

                 if(in_array('Wifi', $house))
               {
                $Property->wifi_amenities = 1;
               }

               if(in_array('Parking', $house))
               {
                 $Property->parking_type = 1;
               }

                 if(in_array('Wash Machine', $house))
               {
                $Property->washing_amenities = 1;
               }
                 if(in_array('Common Balcony', $house))
               {
                $Property->balcony_amenities = 1;
               }
                 if(in_array('Dish Washer', $house))
               {
                $Property->dishwasher_amenities = 1;
               }
                 if(in_array('Wheelchair Frindly', $house))
               {
                $Property->wheelcheer_amenities = 1;
               }
                 if(in_array('Garden', $house))
               {
                $Property->garden_amenities = 1;
               }
                 if(in_array('Terrace', $house))
               {
                $Property->terrace_amenities = 1;
               }
               $Property->other_house_rules = $data->other_rules; 



                //outDoorAmenty
                if(!empty($_REQUEST['outDoorAmenties'] ))
               {
                 $outDoorAmenties = $_REQUEST['outDoorAmenties'];
                 $outDoorAmenty = explode(',', $outDoorAmenties);
               } else {
                 $outDoorAmenty = [];
               }
     



                if(in_array('Garden', $outDoorAmenty))
               {
                $Property->garden_amenities = 1;
               } else {  $Property->garden_amenities = 0; }

                  if(in_array('Gym', $outDoorAmenty))
               {
                $Property->gym = 1;
               } else {  $Property->gym = 0; }

                  if(in_array('Pool', $outDoorAmenty))
               {
                $Property->lawn_tennis = 1;
               } else {  $Property->lawn_tennis = 0; }

                  if(in_array('Table Tennis', $outDoorAmenty))
               {
                $Property->table_tennis = 1;
               } else {  $Property->table_tennis = 0; }

                  if(in_array('Squash', $outDoorAmenty))
               {
                $Property->squash = 1;
               } else {  $Property->squash = 0; }

                  if(in_array('Lawn Tennis', $outDoorAmenty))
               {
                $Property->lawn_tennis = 1;
               } else {  $Property->lawn_tennis = 0; }

               //house Rules


              if(!empty($_REQUEST['house_rules'])){
                $house_rules = $_REQUEST['house_rules'];
                $house_rl = explode(',', $house_rules);
              } else {
                 $house_rl = [];
              }
 
                      


                 // house rules
                           if(in_array('Smoke Friendly', $house_rl))
                         {
                          $building->house_rules_smoker = 1;
                         }
                              if(in_array('Pet Friendly', $house_rl))
                         {
                          $building->house_rules_pet = 1;
                        }


                        $Property->save();


                  if($building->save()) 
           {  
                   $Bedspace = new Bedspace;
                   
                   $Bedspace->fld_room        = $Property->fld_id;
                   $Bedspace->fld_building_id = $building->fld_id;
                   $Bedspace->fld_type        = 'R';
                   $Bedspace->fld_is_bs_model = 1;
                   $Bedspace->fld_block_unblock = 1;
                   $Bedspace->fld_owner = $building->fld_tanent;

                   if($Bedspace->save())
                   {
                        $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$Property->fld_id,'building_id' => $building->fld_id,'Bedspace_id' => $Bedspace->fld_id); 
                   } else {
                         $json_data = array('status'=>false,'message'=>"Error in saving Bedspace data"); 
                   }
        
            }
                                             
    }



    public function entire_step_three(Request $request)

    {  

        return $request->toArray();
        ini_set('memory_limit','2307278683M');


        $building = Building::find($data->property_id);

         if(!empty($building))
         {
             $building->view_type = $request->typeofview;
         }
         $building->save();
       

    

                  if(!empty($request->Room_id) && !empty($request->Bedspace_id))
                 {
                 $Room = Room::where('fld_id',$request->Room_id)->first();
                 

                       if(!empty($Room))
                       {
                          $Room->minimum_stay       = $request->minimum_stay;
                          $Room->fld_expected_rent  = $request->rent;
                          $Room->room_deposit       = $request->room_deposit;
                          $Room->avalability_date   = $request->from_date;
         $avalability_date = Carbon::parse($request->availability_start)->format('Y-m-d');
        $availability_end = Carbon::parse($request->availability_end)->format('Y-m-d');
        
        $validatedData = $request->validate([
            'search_street' => 'required'
        ]);


                          $Room->avalability_end    = $request->to_date;
                          $Room->metro_train        = $request->distance;
                          $Room->bed_type           = $request->bed_type;
                          $Room->fld_owner           = $request->fld_tanent;


                          if($request->bills_included == 'true' || $request->bills_included == 'TRUE' || $request->bills_included == true)
                          {
                            $Room->bills_included = 1;
                          } else {
                            $Room->bills_included = 0;
                          }

                          
                                if($Room->save())
                                {
                                  $Bedspace = Bedspace::where('fld_id',$request->Bedspace_id)->first();

                                      if(!empty($Bedspace)){
                                        $Bedspace->fld_expected_rent = $request->rent;
                                      
                                              if($Bedspace->save())
                                              {
                                                 $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$request->Room_id,'building_id' => $request->building_id,'Bedspace_id' => $request->Bedspace_id);  
                                              }

                                      } else{
                                       $json_data = array('status'=>false,'message'=>"Bedspace Id don't Exists.");  
                                      }

                                } else{
                                   $json_data = array('status'=>false,'message'=>"Error in Saving Room Details."); 
                                }
                         

                       } else {
                          
                          $json_data = array('status'=>false,'message'=>"Room Id don't Exists."); 
                       }
                 
       
                 } else {
                   $json_data = array('status'=>false,'message'=>"Please Enter Room Id."); 
                 }

                 return response()->json($json_data);

    }

    public function entire_step_four()
    {
        if(!empty($request->Room_id) && !empty($request->Bedspace_id))
                 {  

                   $Room = Room::where('fld_id',$request->Room_id)->first();
                      if(!empty($Room))
                       { 

                      
                            //save common pics in property documents
                          if($request->hasfile('commonfile')){
                             $del_commonpics = PropertyDocument::where('fld_property',$request->building_id)->delete();
                            foreach($request->file('commonfile') as $image){
                                      $name = time().'.'.$image->getClientOriginalName();
                                      $image->move(public_path('PROPERTY_DOC'), $name);  

                                   

                                      $commonpics = new PropertyDocument;
                                      $commonpics->fld_property = $request->building_id;
                                      $commonpics->fld_name = $name;
                                      $commonpics->save();
                                  }
                          }


                     $Room->display_pic  =$request->display_pic;
                     $Room->photos_listing_title  =$request->photos_listing_title;
                     $Room->fld_room_description  =$request->fld_room_description;
                     $Room->fld_custom_room_name = $request->photos_listing_title;



                          //save room pics
                          if($request->hasfile('room_pics')){
                                 $del_roompics = RoomPic::where('fld_room_id',$request->Room_id)->delete();
                            foreach($request->file('room_pics') as $image){
                                      $name = time().'.'.$image->getClientOriginalName();
                                      $image->move(public_path('room_pic'), $name);  
                                      

                                      $roompics = new RoomPic;
                                      $roompics->fld_room_id = $request->Room_id;
                                      $roompics->fld_name = $name;
                                      $roompics->save();
                                  }
                          
                              
                      } 

                              if($Room->save())
                                {
                                  $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$request->Room_id,'building_id' => $request->building_id,'Bedspace_id' => $request->Bedspace_id);
                                }

                       } else {
                         $json_data = array('status'=>false,'message'=>"Room Id don't Exists."); 
                       }
                      
                 } else {
                   $json_data = array('status'=>false,'message'=>"Please Enter Room Id."); 
                 }
                  return response()->json($json_data);

    }



}