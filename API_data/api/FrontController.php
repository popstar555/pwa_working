<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\PhoneVerifyRequest;
use DB;
use App\User;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\Property;
use App\View_Lead;
use App\States;
use App\Cities;
use App\Building;
use App\Bedspace;
use App\Booking_History;
use App\Complaint;
use App\SubComplaint;
use App\Room;
use App\RoomPic;
use App\Notification;
use App\PropertyDocument;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class FrontController extends Controller
{   


      public function get_property(){
            
            $json = array();
            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')

            ->where(function($query) {
                return $query->where('tbl_bedspace.fld_is_notice',1)
                    ->orWhere('tbl_bedspace.fld_is_rented',0);
            })

            ->where('tbl_bedspace.fld_block_unblock',1);
          //  ->where('tbl_building.fld_isactive',1);
           // ->where('tbl_building.added_from',2);


             $aparts->orderBy('tbl_building.added_from', 'desc');


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_rooms.fld_custom_room_name as custom_room_name','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_rooms.room_mates_male','tbl_rooms.room_mates_female','tbl_rooms.bath_type');
            //->paginate(15);
            $aparts = $aparts->get();

       foreach ($aparts as  $apt) {
           
     
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->first();
    
     $target_path = config('app.url').'/public/room_pic/';
     // config('app.url');
    
             
             if(empty($image_data))
             {
               $room_image = config('app.url').'/rooms/ROOM_IMAGES/property1.jpg';
             } else {
                $room_image = $target_path.$roomdata->fld_name;
             }
  
        


      if($apt->fld_type == 'R')
       {
         $rent = $apt->room_expected_rent; 
       } else {
          $rent = $apt->bedspace_expected_rent; 
       }
     
         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = config('app.url').'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }

                $bus = array(
                'Area'                 => $apt->Area,
                'building'             => $apt->building,
                'building_id'          => $apt->building_id,
                'apt_no'               => $apt->apt_no,
                'approved'             => $apt->approved,
                'room_name'            => $apt->room_name,
                'room_id'              => $apt->room_id,
                'room_notes'           => $apt->room_notes,
                'custom_room_name'     => $apt->custom_room_name,
                'bedspace_id'          => $apt->bedspace_id,
                'fld_tanent_id'        => $apt->fld_tanent_id,
                'notice'               => $apt->notice,
                'is_rented'            => $apt->is_rented,
                'bedspace_expected_rent'  => $apt->bedspace_expected_rent,
                'name'                    => $apt->name,
                'bath'                    => $apt->bath_type,
                'room_expected_rent'      => $apt->room_expected_rent,
            //    'gender'                  => $apt->room_mates_female,
                'room_images'             => $room_image,
                'fld_profile_pic'         => $fld_profile_pic,
                'rent'                    => $rent,
                'bedtype'                    => $bedtype,
                'room_mates_female'          => $apt->room_mates_female,
                'room_mates_male'            => $apt->room_mates_male,
                // 'building_room_image'             => $room_building_image,
                'fld_type'                => $apt->fld_type);

               array_push($json, $bus);

     }
            return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
  }


        public function get_property_detail($id){
            
            $json = array();
            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            //  ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            // ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            ->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')

             ->where('tbl_bedspace.fld_id',$id);


             $aparts->orderBy('tbl_building.added_from', 'desc');


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_building.*','tbl_rooms.*','tbl_users.fld_number','tbl_users.country_code','tbl_users.fld_secondary_number','tbl_countries.currency','tbl_users.id AS Owner_id');
            //->paginate(15);
            $aparts = $aparts->get();

       foreach ($aparts as  $apt) {

           if(!empty($apt->fld_tanent_id))
        {
        $notice_data =  DB::table('tbl_notice')->where('tenent_id', $apt->fld_tanent_id)->first();
        }

       $bedcount = DB::table('tbl_bedspace')->where('fld_block_unblock','1')->where('fld_room', $apt->room_id)->count('*');   

      // dd($bedcount); 
      // echo $bedcount;
      // die;
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->get();
    
     $target_path = config('app.url').'public/room_pic/';
     $room_image = [];
       foreach ($roomdata as  $image_data) {
        
             
             if(!empty($image_data))
            {
                $room_image[] = $target_path.$image_data->fld_name;
             }
  
        } 
        
        if(empty($room_image[0]))
        {
             $room_image = [];
        }


      $propertydata = DB::table('tbl_property_documents')->where('fld_property', $apt->building_id)->get();
    
      $target_path = config('app.url').'public/PROPERTY_DOC/';
      


       if(!empty($propertydata[0]))
       {


         foreach ($propertydata as  $image_data) {
          
       
             if(!empty($image_data))
           {
                $buliding_image[] = $target_path.$image_data->fld_name;
             }
  
         } 
     } else {
                  $buliding_image = [];
     }

         $room_building_image = array_merge($room_image,$buliding_image);

      if($apt->fld_type == 'R')
       {
         $rent = $apt->room_expected_rent; 
       } else {
          $rent = $apt->bedspace_expected_rent; 
       }

           $path = $this->domain =config('app.url');

         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = $path.'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }


         if($apt->bills_included == 1)
         {
            $bills_included = 'All Bills Inclusive';
         } else {
            $bills_included = 'Bills are not Included';
         } 

          $fld_room_description = $apt->fld_room_description;

                $notice = Booking_History::where('fld_bedspace_id',$apt->bedspace_id)->orderBy('fld_id', 'desc')->first();
          
            $date = date("Y-m-d");
           if($apt->avalability_date !='' && $apt->avalability_date != '0000-00-00' && strtotime($date) < strtotime($apt->avalability_date))
           {
             $avalability_date  = $apt->avalability_date;
           } else {
                if(!empty($notice))
                       {
                              if($notice->fld_is_notice == 1 && $notice->fld_move_out_date != '0000-00-00')
                              { 
                                 if(strtotime($date) < strtotime($notice->fld_move_out_date))
                                 {
                                     $avalability_date = $notice->fld_move_out_date;    
                                  } else {
                                     $avalability_date = "Today";
                                         }
                              } else {
                                $avalability_date = "Today";
                              } 
                        }  else {
                           $avalability_date = "Today";
                        }
           }

           

          if($apt->added_from == 1 ) 
          {
            $room_males = $apt->room_mates_male; 
          } else {

             if($apt->fld_type == 'R')
             {

                $room_males = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_building_id', $apt->building_id)
                ->where('tbl_users.fld_sex','M')
                ->count('tbl_bedspace.fld_id');
            } else {
                 $room_males = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_room', $apt->room_id)
                ->where('tbl_users.fld_sex','M')
                ->count('tbl_bedspace.fld_id'); 
            }

          }


      // echo $total_females;



          
          if($apt->added_from == 1 ) 
          {
            $room_females = $apt->room_mates_female; 
          } else {

                if($apt->fld_type == 'R')
             {

                $room_females = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_building_id', $apt->building_id)
                ->where('tbl_users.fld_sex','F')
                ->count('tbl_bedspace.fld_id');

                } else {
                 $room_females = DB::table('tbl_bedspace')
                ->join('tbl_users', 'tbl_users.id', '=', 'tbl_bedspace.fld_tanent_id')
                ->WHERE('tbl_bedspace.fld_is_rented',1)
                ->WHERE('tbl_bedspace.fld_room', $apt->room_id)
                ->where('tbl_users.fld_sex','F')
                ->count('tbl_bedspace.fld_id');
                }


          }
       
         // if($apt->added_from == 1 ) 
         //  {
         //    $occupancy = $apt->occupancy; 
         //  } else {
         //    $occupancy =  $room_males + $room_females;
         //  }

             

       if(!empty($apt->fld_number))
       {

       // $contact = '+'.$apt->country_code.$apt->fld_number;

        if (substr($apt->fld_number, 0, strlen($apt->country_code)) == $apt->country_code) {
        $contact = substr($apt->fld_number, strlen($apt->country_code));
        $contact = '+'.$apt->country_code.$contact;
        }  else {
          $contact = '+'.$apt->country_code.$apt->fld_number;
        }


       }else {
        $contact = '+'.$apt->country_code.$apt->fld_secondary_number;
       }
        
        if($apt->private_washroom_amenities == 1)
        {
          $bath_type = 'Private washroom';
        } else {
          $bath_type = 'Shared washroom';
        }


         
       if(!empty($apt->fld_room_description))
      {
          $description = trim($apt->fld_room_description);   
      } else if(!empty($apt->fld_custom_room_name)){
           $description = trim($apt->fld_custom_room_name); 
      }  else {
          $description = "Now Enjoy Lexurious".$bedtype." in just". $rent . "/month";
      } 

        if(is_numeric($apt->room_deposit))
        {
            $deposit = $apt->room_deposit / $bedcount;
          } else {
            $deposit = 0;
          }
     


                $bus = array(
                'Area'                        => $apt->Area,
                'occupancy'                   => $apt->occupancy,
                'room_males'                  => $room_males,
                'room_females'                => $room_females,
                'building'                    => $apt->building,
                'building_id'                 => $apt->building_id,
                'property_type'               => $apt->property_type,
                'fld_address'                 => $apt->fld_address,
                'fld_latitude'                => $apt->fld_latitude,
                'fld_longitude'               => $apt->fld_longitude,
                'parking_amenities'           => $apt->parking_amenities,
                'elevator_amenities'          => $apt->elevator_amenities,
                'pool_amenities'              => $apt->pool_amenities,
                'house_rules_smoker'          => $apt->house_rules_smoker,
                'house_rules_pet'             => $apt->house_rules_pet,
                'house_rules_couple'          => $apt->house_rules_couple,
                'house_rules_children'        => $apt->house_rules_children,
                'selected_listing'            => $apt->selected_listing,
                'table_tennis'                => $apt->table_tennis,
                'gym'                         => $apt->gym,
                'Owner_id'                    => $apt->Owner_id,
                'lawn_tennis'                 => $apt->lawn_tennis,
                'squash'                      => $apt->squash,
                'approved'                    => $apt->approved,
                'room_name'                   => $apt->room_name,
                'room_id'                     => $apt->room_id,
                'bedspace_id'                 => $apt->bedspace_id,
                'fld_tanent_id'               => $apt->fld_tanent_id,
                'room_title'                  => trim($apt->fld_custom_room_name),
                'balconices'                  => $apt->balconices,
                'metro_train'                 => $apt->metro_train,
                'currency'                    => $apt->currency,
                'gender'                      => $apt->gender,
                'common_room'                 => $apt->common_room,
                'avalability_date'            => $avalability_date,
                'room_deposit'                => $deposit,
                'bills_included'              => $bills_included,
                'minimum_stay'                => $apt->minimum_stay,
                'display_pic'                 => $apt->display_pic,
                'other_house_rules'           => $apt->other_house_rules,
                'shared_washroom_amenities'   => $apt->shared_washroom_amenities,
                'private_washroom_amenities'  => $apt->private_washroom_amenities,
                'living_room_amenities'       => $apt->living_room_amenities,
                'fridge_amenities'            => $apt->fridge_amenities,
                'window_amenities'            => $apt->window_amenities,
                'private_balcony_amenities'   => $apt->private_balcony_amenities,
                'fld_room_description'        => trim($description),
                'photos_listing_title'        => $apt->photos_listing_title,
                'terrace_amenities'           => $apt->terrace_amenities,
                'garden_amenities'            => $apt->garden_amenities,
                'wheelcheer_amenities'        => $apt->wheelcheer_amenities,
                'dishwasher_amenities'        => $apt->dishwasher_amenities,
                'natural_light_amenities'     => $apt->natural_light_amenities,
                'balcony_amenities'           => $apt->balcony_amenities,
                'furnished_room_amenities'    => $apt->furnished_room_amenities,
                'washing_amenities'           => $apt->washing_amenities,
                'wifi_amenities'              => $apt->wifi_amenities,
                'tv_amenities'                => $apt->tv_amenities,
                'room_mates_female'           => $apt->room_mates_female,
                'room_mates_male'             => $apt->room_mates_male,
                'name'                        => $apt->name,
                'bath'                        => $bath_type,
                'room_expected_rent'          => $apt->room_expected_rent,
                'room_images'                 => $room_image,
                'fld_profile_pic'             => $fld_profile_pic,
                'contact'                     => $contact,
                'rent'                        => $rent,
                'bedtype'                     => $bedtype,
                'building_room_image'        => $room_building_image,
                'property_share_url'         => config('app.url').'flatshare/'.$id,
                'fld_type'                    => $apt->fld_type);

               array_push($json, $bus);

     }
            return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
  }

          public function frontproperty(){

                    $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')

            ->where(function($query) {
                return $query->where('tbl_bedspace.fld_is_notice',1)
                    ->orWhere('tbl_bedspace.fld_is_rented',0);
            })

            ->where('tbl_bedspace.fld_block_unblock',1)
            ->where('tbl_building.fld_approved',1)
            ->where('tbl_building.fld_isactive',1)
            ->where('tbl_building.added_from',1);


             $aparts->orderBy('tbl_bedspace.fld_id', 'desc');


            $aparts->select('tbl_building.fld_area as area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_rooms.fld_custom_room_name as custom_room_name','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type');
            //->paginate(15);
            $aparts = $aparts->get();

             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $aparts
             ]); 
         
        }

          public function frontpropertyBYLocation($id){

                       $json = array();
            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')

            ->where(function($query) {
                return $query->where('tbl_bedspace.fld_is_notice',1)
                    ->orWhere('tbl_bedspace.fld_is_rented',0);
            })

            ->where('tbl_bedspace.fld_block_unblock',1)
            ->where('tbl_building.fld_isactive',1)
            ->where('tbl_building.state_id',$id);
           // ->where('tbl_building.added_from',2);


             $aparts->orderBy('tbl_building.added_from', 'desc');


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_rooms.fld_custom_room_name as custom_room_name','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_rooms.room_mates_male','tbl_rooms.room_mates_female','tbl_rooms.bath_type','tbl_rooms.fld_custom_room_name');
            //
            $aparts = $aparts->get();



       foreach ($aparts as  $apt) {
           
     
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $room_image = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->first();
    
     $target_path = config('app.url').'/public/room_pic/';
       //  $room_image = [];
       // foreach ($roomdata as  $image_data) {
        
         
             if(empty($room_image))
             {
               $room_image = $target_path.'public/common_pics/property.jpg';
             } else {
                $room_image = $target_path.$room_image->fld_name;
             }
  
        // } 



      if($apt->fld_type == 'R')
       {
         $rent = $apt->room_expected_rent; 
       } else {
          $rent = $apt->bedspace_expected_rent; 
       }

          $path = $this->domain =config('app.url');

         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = $path.'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }
           $room_title = preg_replace('/\s+/', '', $apt->fld_custom_room_name);

         //  $room_title = trim($apt->fld_custom_room_name);
                $bus = array(
                'Area'                 => $apt->Area,
                'building'             => $apt->building,
                'building_id'          => $apt->building_id,
                'apt_no'               => $apt->apt_no,
                'approved'             => $apt->approved,
                'room_name'            => $apt->room_name,
                'room_id'              => $apt->room_id,
                'room_notes'           => trim(preg_replace('/\s+/', '',$apt->room_notes)),
                'custom_room_name'     => trim(preg_replace('/\s+/', '',$apt->custom_room_name)),
                'bedspace_id'          => $apt->bedspace_id,
                'fld_tanent_id'        => $apt->fld_tanent_id,
                'notice'               => $apt->notice,
                'is_rented'            => $apt->is_rented,
                'bedspace_expected_rent'  => $apt->bedspace_expected_rent,
                'name'                    => $apt->name,
                'bath'                    => $apt->bath_type,
                'room_expected_rent'      => $apt->room_expected_rent,
                'room_title'               =>$room_title,
            //    'gender'                  => $apt->room_mates_female,
                'room_images'             => $room_image,
                'fld_profile_pic'         => $fld_profile_pic,
                'rent'                    => $rent,
                'bedtype'                    => $bedtype,
                'room_mates_female'          => $apt->room_mates_female,
                'room_mates_male'            => $apt->room_mates_male,
                // 'building_room_image'             => $room_building_image,
                'fld_type'                => $apt->fld_type);

               array_push($json, $bus);

     }
            return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
         
        }


          public function frontpropertyBYFilter(Request $request){
              DB::enableQueryLog();
                 

                   $sort =  $request->sort_by;

             $json = array();


            $aparts = DB::table('tbl_bedspace')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_building', 'tbl_building.fld_id', '=', 'tbl_rooms.fld_building_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            ->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')
            ->where(function($query) {
                return $query->where('tbl_bedspace.fld_is_notice',1)
                    ->orWhere('tbl_bedspace.fld_is_rented',0);
            })
                
            ->where('tbl_bedspace.fld_block_unblock',1)
            //->where('tbl_rooms.fld_approval_waiting',1)
            ->where('tbl_rooms.is_listing_approved',1)
            ->where('tbl_building.fld_isactive',1);
       

             if(!empty($sort))
             { 
                if($request->sort_by == 'priceHL')
                {

                 $aparts->orderBy('tbl_bedspace.fld_expected_rent', 'DESC');
                } else if($request->sort_by =='priceLH')
                  {
                      $aparts->orderBy('tbl_bedspace.fld_expected_rent', 'ASC');

                  } else if($request->sort_by =='metroLH')
                  {
                           $aparts->orderBy('tbl_rooms.metro_train', 'DESC');

                  } else if($request->sort_by =='metroHL')
                  {
                        $aparts->orderBy('tbl_rooms.metro_train', 'ASC');
                  } 
             } else {
                 $aparts->orderBy('tbl_bedspace.fld_id', 'DESC');
             }
            


            $aparts->select('tbl_building.fld_area as Area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.fld_id as room_id','tbl_rooms.fld_notes as room_notes','tbl_rooms.fld_custom_room_name as custom_room_name','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_bedspace.fld_type', 'tbl_users.fld_profile_pic','tbl_users.is_avatar','tbl_rooms.room_mates_male','tbl_rooms.room_mates_female','tbl_rooms.shared_washroom_amenities','tbl_rooms.private_washroom_amenities','tbl_rooms.fld_custom_room_name','tbl_rooms.avalability_date','tbl_rooms.other_house_rules','tbl_rooms.*','tbl_countries.currency','tbl_users.id AS user_id');
            //->paginate(15);
            if(!empty($request->max_search_price) &&  !empty($request->min_search_price))
               {  $aparts->where('tbl_bedspace.fld_expected_rent','<=',$request->max_search_price)
                     ->where('tbl_bedspace.fld_expected_rent','>=',$request->min_search_price);
              }
            else if($request->min_search_price != '' && empty($request->max_search_price)){
               $aparts->where('tbl_bedspace.fld_expected_rent','>=',$request->min_search_price);
            } else if($request->max_search_price != '' && empty($request->min_search_price))
            {
                $aparts->where('tbl_bedspace.fld_expected_rent','<=',$request->max_search_price);
            }
               
               // echo $request->location_id;
               // die('ll');

            
                       //       if($request->has('city_id'))
           //              {
           //         if($request->has('city_id'))
           //      if($request->city_id != '')
           //      {
           //           $building = Location::select('name')->WHERE('id', $request->city_id)->first();
           //            $aparts->where('tbl_building.fld_area','LIKE',$building->name);
           //      }
           // }


                 if(!empty($request->city_id[0]))
                 {
                     $data= json_decode($request->city_id);

                        foreach ($data as  $value) {
                          $city[] = $value->name;
                        }
                      $city= implode(',', $city);

                        //print_r($city);

                    if(!empty($city)){
                       $aparts->where('tbl_building.fld_area','LIKE','%'.$city);
                    }

                  }
              
                        if($request->has('location_id'))
                        {
                    if(!empty($request->location_id))
                if($request->location_id != '')
                    $aparts->where('tbl_building.city_id',$request->location_id);
                       }

                   if($request->has('property_id'))
                if($request->property_id != '')
                    $aparts->where('tbl_building.property_id',$request->property_id);




                if($request->has('gender'))
                {
                   if($request->gender == 'M')
                   {
                     $aparts->where('tbl_rooms.gender','!=','F');
                   }  else if($request->gender == 'F')
                        {
                        $aparts->where('tbl_rooms.gender','!=','M');
                        } else if($request->gender == 'M/F') {
                     $aparts->where('tbl_rooms.occupancy', '>', '1');
                     $aparts->where('tbl_rooms.gender', 'M/F');
                             
                        }
                }

            if($request->has('select_balconies'))
                if($request->select_balconies != '')
                    $aparts->where('tbl_rooms.balconices',$request->select_balconies);

            // if($request->has('select_bath'))
            //     if($request->select_bath != '')

            //         $aparts->where('tbl_rooms.bath',$request->select_bath);

             if($request->has('shared_bath') && $request->has('attached_bath'))
             {

             } else {

            if($request->has('shared_bath'))
            if($request->shared_bath == 'true' || $request->shared_bath === true )
        
                $aparts->where('tbl_rooms.bath', 'Shared Bath');

            if($request->has('attached_bath'))
                if($request->attached_bath == 'true' || $request->attached_bath === true )
            
                    $aparts->where('tbl_rooms.bath', 'Attached Bath');
             }
            

            if($request->has('select_window'))
                if($request->select_window != '')
                    $aparts->where('tbl_rooms.window',$request->select_window);

            if($request->has('select_common_room'))
                if($request->select_common_room != '')
                    $aparts->where('tbl_rooms.common_room',$request->select_common_room);

          //  if($request->has('bedspace'))
                if($request->bedspace === 'true' || $request->bedspace === true )
                {
                    $aparts->where('tbl_bedspace.fld_type','B');
                }

            if($request->room != '')
            {
                if($request->room === 'true' || $request->room === true )
                {
                    $aparts->where('tbl_bedspace.fld_type','R');
                }

            }

            // if($request->has('bed_type'))
            //     if($request->bed_type != '')
            //         $aparts->where('tbl_rooms.bed_type',$request->bed_type);

            if($request->has('hr_smoking'))
                if($request->hr_smoking != '')
                    if($request->hr_smoking == 'true')
                        $aparts->where('tbl_building.house_rules_smoker',1);

            if($request->has('hr_couples'))
                if($request->hr_couples != '')
                    if($request->hr_couples == 'true')
                        $aparts->where('tbl_building.house_rules_couple',1);

            if($request->has('hr_pets'))
                if($request->hr_pets != '')
                    if($request->hr_pets == 'true')
                        $aparts->where('tbl_building.house_rules_pet',1);

            if($request->has('hr_children'))
                if($request->hr_children != '')
                    if($request->hr_children == 'true')
                        $aparts->where('tbl_building.house_rules_children',1);

            if($request->has('hr_balcony'))
                if($request->hr_balcony != '')
                    if($request->hr_balcony == 'true')
                        $aparts->where('tbl_rooms.private_balcony_amenities',1);

            if($request->has('hr_window'))
                if($request->hr_window != '')
                  if($request->hr_window == 'true')
                    $aparts->where('tbl_rooms.window_amenities',1);

            if($request->has('hr_tv'))
                if($request->hr_tv != '')
                    if($request->hr_tv == 'true')
                        $aparts->where('tbl_rooms.tv_amenities',1);

            if($request->has('hr_fridge'))
                if($request->hr_fridge != '')
                    if($request->hr_fridge == 'true')
                        $aparts->where('tbl_rooms.fridge_amenities',1);

            if($request->has('hr_wifi'))
                if($request->hr_wifi != '')
                    if($request->hr_wifi == 'true')
                        $aparts->where('tbl_rooms.wifi_amenities',1);

            if($request->has('hr_parking'))
                if($request->hr_parking != '')
                    if($request->hr_parking == 'true')
                        $aparts->where('tbl_building.parking_amenities',1);

            if($request->has('hr_elevator'))
                if($request->hr_elevator != '')
                    if($request->hr_elevator == 'true')
                        $aparts->where('tbl_building.elevator_amenities',1);

            if($request->has('hr_furnished_room'))
                if($request->hr_furnished_room != '')
                    if($request->hr_furnished_room == 'true')
                        $aparts->where('tbl_rooms.furnished_room_amenities',1);

            if($request->has('hr_pool'))
                if($request->hr_pool != '')
                    if($request->hr_pool == 'true')
                        $aparts->where('tbl_building.pool_amenities',1);

            if($request->has('hr_washing'))
                if($request->hr_washing != '')
                    if($request->hr_washing == 'true')
                        $aparts->where('tbl_rooms.washing_amenities',1);

            if($request->has('hr_common_balcony'))
                if($request->hr_common_balcony != '')
                    if($request->hr_common_balcony == 'true')
                        $aparts->where('tbl_rooms.balcony_amenities',1);

            if($request->has('natural_light'))
                if($request->natural_light != '')
                    if($request->natural_light == 'true')
                        $aparts->where('tbl_rooms.natural_light_amenities',1);

            if($request->has('dishwasher_amenity'))
                if($request->dishwasher_amenity != '')
                    if($request->dishwasher_amenity == 'true')
                        $aparts->where('tbl_rooms.dishwasher_amenities',1);

            if($request->has('wheelchair_amenity'))
                if($request->wheelchair_amenity != '')
                    if($request->wheelchair_amenity == 'true')
                        $aparts->where('tbl_rooms.wheelcheer_amenities',1);

            if($request->has('garden_amenity'))
                if($request->garden_amenity != '')
                    if($request->garden_amenity == 'true')
                        $aparts->where('tbl_rooms.garden_amenities',1);

            if($request->has('terrace_amenity'))
                if($request->terrace_amenity != '')
                    if($request->terrace_amenity == 'true')
                        $aparts->where('tbl_rooms.terrace_amenities',1);

            if($request->has('living_room'))
                if($request->living_room != '')
                    if($request->living_room == 'true')
                        $aparts->where('tbl_rooms.living_room_amenities',1);

            if($request->has('other_house_rules'))
                if($request->other_house_rules != '')
                        $aparts->where('tbl_rooms.other_house_rules',$request->other_house_rules);

             $aparts_count= $aparts->count();
            $aparts = $aparts->paginate(8);
          
            // $dblog = DB::getQueryLog($aparts);

            // print_r($dblog);
            // die('mar');

       foreach ($aparts as  $apt) {
           
           if(!empty($apt->fld_tanent_id))
           {
             $notice_data =  DB::table('tbl_notice')->where('tenent_id', $apt->fld_tanent_id)->first();
           }
          
     
    //$roomdata="SELECT fld_name FROM `tbl_room_pics` WHERE `fld_room_id`='".$apt->room_id."'";
     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->first();
    
     $target_path = config('app.url').'public/room_pic';
             
         
             if(empty($roomdata))
             {
               $room_image = $target_path.'/property.jpg';
             } else {
                $room_image = $target_path.'/'.$roomdata->fld_name;
             }



      if($apt->fld_type == 'R')
       {
         $rent = $apt->room_expected_rent; 
       } else {
          $rent = $apt->bedspace_expected_rent; 
       }

          $path = $this->domain =config('app.url');
           
           if($apt->is_avatar == 0)
           {
         if(!empty($apt->fld_profile_pic))
         {
             $fld_profile_pic = $path.'public/assets/profiles/'.$apt->fld_profile_pic;
         } else {
            $fld_profile_pic =  $path.'public/assets/profiles/user.jpeg';
         }
          } else {
               $check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$apt->user_id)->first();

                 $get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

                 if(!empty($get_avtar->name))
               {
                  $fld_profile_pic = $path.'public/assets/avatar/png/'.$get_avtar->name; 

               } else{

                $fld_profile_pic = $path.'public/assets/profiles/user.jpeg';
               }
          }

         if($apt->fld_type == 'R')
         {
            $bedtype = 'Room';
         } else {
            $bedtype = 'Bedspace';
         }
            
            $room_title = preg_replace('/\s+/', ' ', $apt->fld_custom_room_name);
            $room_notes = preg_replace('/\s+/', ' ', $apt->room_notes);

            // if(!empty($notice_data))
            // {
            //       $avalability_date = $notice_data->move_out_date;
            // } elseif(!empty($apt->avalability_date) && $apt->avalability_date != '0000-00-00') {
               
            //      $avalability_date = $apt->avalability_date;
            // } else {
            //      $avalability_date = 'From Today';
            // }

            $notice = Booking_History::where('fld_bedspace_id',$apt->bedspace_id)->orderBy('fld_id', 'desc')->first();
          
            $date = date("Y-m-d");
           if($apt->avalability_date !='' && $apt->avalability_date != '0000-00-00' && strtotime($date) < strtotime($apt->avalability_date))
           {
             $avalability_date  = $apt->avalability_date;
           } else {
                if(!empty($notice))
                       {
                              if($notice->fld_is_notice == 1 && $notice->fld_move_out_date != '0000-00-00')
                              { 
                                 if(strtotime($date) < strtotime($notice->fld_move_out_date))
                                 {
                                     $avalability_date = $notice->fld_move_out_date;    
                                  } else {
                                     $avalability_date = "Today";
                                         }
                              } else {
                                $avalability_date = "Today";
                              } 
                        }  else {
                           $avalability_date = "Today";
                        }
           }


        if($apt->private_washroom_amenities == 1)
        {
          $bath_type = 'Private washroom';
        } else {
          $bath_type = 'Shared washroom';
        }
         
         $pagination_data = array('current_page' => $aparts->currentPage(), 'lastPage' => $aparts->lastPage(), 'previousPageUrl' => $aparts->previousPageUrl(), 'nextPageUrl' => $aparts->nextPageUrl() );

                $bus = array(
                'Area'                 => $apt->Area,
                'building'             => $apt->building,
                'building_id'          => $apt->building_id,
                'apt_no'               => $apt->apt_no,
                'approved'             => $apt->approved,
                'room_name'            => $apt->room_name,
                'room_id'              => $apt->room_id,
                'room_notes'           => $room_notes,
                'custom_room_name'     => $room_title,
                'bedspace_id'          => $apt->bedspace_id,
                'fld_tanent_id'        => $apt->fld_tanent_id,
                'notice'               => $apt->notice,
                'is_rented'            => $apt->is_rented,
                'bedspace_expected_rent'  => $apt->bedspace_expected_rent,
                'currency'             => $apt->currency,
                'name'                    => $apt->name,
                'private_washroom_amenities'   => $apt->private_washroom_amenities,
                'shared_washroom_amenities'   => $apt->shared_washroom_amenities,
                'room_expected_rent'      => $apt->room_expected_rent,
                'room_title'              => $room_title,
                'bath_type'                  => $bath_type,
                'room_images'             => $room_image,
                'fld_profile_pic'         => $fld_profile_pic,
                'rent'                    => $rent,
                'bedtype'                    => $bedtype,
                'room_mates_female'          => $apt->room_mates_female,
                'room_mates_male'            => $apt->room_mates_male,
                'other_house_rules'            => $apt->other_house_rules,
                'avalability_date'            => $avalability_date,
                // 'building_room_image'             => $room_building_image,
                'fld_type'                => $apt->fld_type);

               array_push($json, $bus);

     }    
         if($aparts_count > 0)
         {
                      return response()->json([
            'status'  => true,
            'total'   => $aparts_count,
            'pagination'   => $pagination_data,
            'message' => 'Successfully',
            'data'    => $json,
             ]); 
                    } else {
             return response()->json([
            'status'  => true,
            'total'   => $aparts_count,
            'pagination'   => null,
            'message' => 'Successfully',
            'data'    => $json,
             ]); 
                    }

         
        }

        public function my_listings($userid)
        {    
           DB::enableQueryLog();
            $data = DB::table('tbl_building')
            ->join('tbl_bedspace', 'tbl_building.fld_id', '=', 'tbl_bedspace.fld_building_id')
            ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
            ->join('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
            ->Join('tbl_property', 'tbl_building.property_id', '=', 'tbl_property.id')
            ->leftJoin('tbl_location', 'tbl_building.fld_area', '=', 'tbl_location.id')
            ->where('tbl_building.fld_tanent',$userid)
            ->where('tbl_building.added_from',1)
            ->select('tbl_location.name as location_name','tbl_property.name AS property_name','tbl_building.fld_area as area','tbl_building.fld_building as building','tbl_building.fld_id as building_id','tbl_building.fld_apt_no as apt_no','tbl_building.fld_approved as approved','tbl_rooms.fld_room_name as room_name','tbl_rooms.occupancy as occupancy','tbl_rooms.metro_train as metro_train','tbl_rooms.balconices as balconices','tbl_rooms.gender as gender','tbl_rooms.common_room as common_room','tbl_rooms.avalability_date as avalability_date','tbl_rooms.bath as bath','tbl_rooms.window as window','tbl_rooms.fld_room_description','tbl_bedspace.fld_id as bedspace_id','tbl_bedspace.fld_tanent_id','tbl_bedspace.fld_room as bedspace','tbl_bedspace.fld_is_notice as notice','tbl_bedspace.fld_is_rented as is_rented','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_bedspace.fld_type','tbl_bedspace.fld_expected_rent as bedspace_expected_rent','tbl_users.fld_name as name','tbl_rooms.fld_expected_rent as room_expected_rent','tbl_rooms.fld_id as room_id','tbl_users.fld_number as phone_number','tbl_rooms.room_mates_male','tbl_rooms.room_mates_female','tbl_rooms.minimum_stay','tbl_rooms.is_listing_approved', 'tbl_rooms.fld_approval_waiting','tbl_rooms.fld_update_date AS Update_time','tbl_building.fld_address')
            ->get();
            //   print_r($data);
            // dd(DB::getQueryLog());
           
    
           $json = [];
                foreach ($data as  $apt) {

     $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $apt->room_id)->first();
      
     $target_path = config('app.url').'public/room_pic/';
             
         
             if(empty($roomdata))
             {
               $room_image = env('ROOMDADDY_PATH').'/rooms/ROOM_IMAGES/property1.jpg';
             } else {
                $room_image = $target_path.$roomdata->fld_name;
             }

             if(!empty($apt->bedspace_id))
             {
              $email_lead = View_Lead::where('fld_bedspace_id',$apt->bedspace_id)->where('fld_lead_type','e')->sum('fld_view_count');
         
         $chat_lead = View_Lead::where('fld_bedspace_id',$apt->bedspace_id)->where('fld_lead_type','c')->sum('fld_view_count');
         
         $phone_lead = View_Lead::where('fld_bedspace_id',$apt->bedspace_id)->where('fld_lead_type','p')->sum('fld_view_count'); 

         $total_views = View_Lead::where('fld_bedspace_id',$apt->bedspace_id)->where('fld_lead_type','v')->sum('fld_view_count');
       
        $total_lead = array('email_lead'=>$email_lead,'chat_lead'=>$chat_lead,'phone_lead'=>$phone_lead,'total_views'=>$total_views);

       
             }


        $bus = array(
            "location_name"=> $apt->location_name,
            "property_name"=> $apt->property_name,
            "fld_approval_waiting" => $apt->fld_approval_waiting,
            "area"=> $apt->area,
            "total_lead"=> $total_lead,
            "building"=> $apt->building,
            "building_id"=> $apt->building_id,
            "apt_no"=> $apt->apt_no,
            "approved"=> $apt->approved,
            "room_name"=> $apt->room_name,
            "occupancy"=> $apt->occupancy,
            "metro_train"=> $apt->metro_train,
            "balconices"=> $apt->balconices,
            "gender"=> $apt->gender,
            "common_room"=> $apt->common_room,
            "avalability_date"=> $apt->avalability_date,
            "bath"=> $apt->bath,
            "fld_address"=> $apt->fld_address,
            "window"=> $apt->window,
            "fld_room_description"=> $apt->fld_room_description,
            "bedspace_id"=> $apt->bedspace_id,
            "fld_tanent_id"=> $apt->fld_tanent_id,
            "bedspace"=> $apt->bedspace,
            "notice"=> $apt->notice,
            "is_rented"=> $apt->is_rented,
            "bedspace_expected_rent"=> $apt->bedspace_expected_rent,
            "fld_type"=> $apt->fld_type,
            "name"=> $apt->name,
            "Update_time"=> $apt->Update_time,
            "room_expected_rent"=> $apt->room_expected_rent,
            "room_id"=> $apt->room_id,
            "phone_number"=> $apt->phone_number,
            "room_mates_male"=> $apt->room_mates_male,
            "room_mates_female"=> $apt->room_mates_female,
            "minimum_stay"=> $apt->minimum_stay,
            "is_listing_approved"=> $apt->is_listing_approved,
            "room_image"    => $room_image);

               array_push($json, $bus);

     }

             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
        }

          public function my_bookings($userid)
        {
                $json = array();
            $Booking_data = DB::table('tbl_users')
            ->leftjoin('tbl_booking_history', 'tbl_users.id', '=', 'tbl_booking_history.fld_tenent_id')
            ->leftjoin('tbl_rooms', 'tbl_booking_history.fld_room_id', '=', 'tbl_rooms.fld_id')
            ->leftjoin('tbl_building', 'tbl_rooms.fld_building_id', '=', 'tbl_building.fld_id')
            ->leftjoin('tbl_rent_status', 'tbl_rent_status.fld_tanent_id', '=', 'tbl_users.id')
            // ->join('tbl_location', 'tbl_building.fld_area', '=', 'tbl_location.id')
            ->where('tbl_users.id',$userid)
            //->where('tbl_booking_history.is_deleted',0)
            ->select('tbl_users.*','tbl_rooms.fld_room_name','tbl_rooms.fld_id AS Room_id','tbl_rooms.fld_custom_room_name',
          'tbl_building.fld_building', 'tbl_building.fld_whtsappgroup', 'tbl_booking_history.fld_bedspace_id',
          'tbl_booking_history.fld_move_in_date', 'tbl_booking_history.fld_review', 'tbl_booking_history.fld_deposit AS Deposit_paid','tbl_booking_history.fld_payment_due_date', 'tbl_booking_history.fld_rent', 'tbl_booking_history.fld_comission', 'tbl_booking_history.fld_minimum_stay', 'tbl_booking_history.fld_comments', 'tbl_booking_history.fld_contract_image','tbl_rent_status.fld_rent_paid', 'tbl_booking_history.fld_booked_by', 'tbl_booking_history.fld_payment_frequency', 'tbl_booking_history.fld_id AS booking_id','tbl_booking_history.fld_is_current_tanent' )->groupBy('fld_bedspace_id')->get();
           
            foreach ($Booking_data as  $data) {

           
            $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $data->Room_id)->first();

            $target_path = config('app.url').'public/room_pic/';
            // config('app.url');
    
             // print_r($roomdata);
             // die;
             if(!empty($roomdata))
             {
                $room_image = $target_path.$roomdata->fld_name;
             } else {
               
                $room_image = env('ROOMDADDY_PATH').'/rooms/ROOM_IMAGES/property1.jpg';
             }


            $payment_history = DB::table('tbl_rent_status')
                               ->WHERE('fld_booking_id', $data->booking_id)->orderBy('fld_id','DESC')->first();
            //print_r($payment_history);

            if(!empty($payment_history->fld_paid_date))
            {
                $last_pay = $payment_history->fld_paid_date;
            } else {
                $last_pay = '';
            }

            if(!empty($payment_history->fld_rent_paid))
            {
                $last_rent = $payment_history->fld_rent_paid;
            } else {
                $last_rent = '';
            }
          
            $payment_history = DB::table('tbl_deposit_in')
                               ->WHERE('fld_booking_id', $data->booking_id)
                                ->sum('fld_deposit');
                   

       if(!empty($payment_history))
       {
        $deposit_amount = $payment_history;
       } else {
        $deposit_amount = 0;
       }

          if(!empty($data->fld_profile_pic))
         {
             $fld_profile_pic = config('app.url').'public/assets/profiles/'.$data->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

          $month_date = date('m Y');


           $last_rent_amount = DB::table('tbl_rent_status')
                               ->WHERE('fld_booking_id', $data->booking_id)
                                ->orderBy('fld_id','DESC')->first(); 
          if(!empty($last_rent_amount))
          {
            $paid_rent = $last_rent_amount->fld_rent_paid;
          }  else {
            $paid_rent = 0;
          }

          $total_rent_paid = DB::table('tbl_rent_status')
                                     ->WHERE('fld_booking_id', $data->booking_id)
                                      ->sum('fld_rent_paid');
               
            $end_date = date('Y-m-d');

            $start_date = $data->fld_move_in_date;
            $to   = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date);
            $diff_in_months = $to->diffInMonths($from);

            $total_exp_rent = ($diff_in_months * $data->fld_rent);

            $bal = $total_exp_rent - $total_rent_paid;


              $bus = array(
             "id"=>  $data->id,
            "fld_name"=> $data->fld_name,
            "Room_id"=> $data->Room_id,
            "fld_number"=>  $data->fld_number,
            "fld_secondary_number"=>  $data->fld_secondary_number,
            "fld_profile_pic"=>  $fld_profile_pic,
            "email"=>  $data->email,
            "password"=>  $data->password,
            "fld_country_id"=>  $data->fld_country_id,
            "fld_sex"=>  $data->fld_sex,
            "dob_year"=>  $data->dob_year,
            "dob_month"=>  $data->dob_month,
            "dob_day"=>  $data->dob_day,
            "fld_user_type"=> $data->fld_user_type,
            "fld_is_active"=>  $data->fld_is_active,
            "fld_whatsapp_no"=>  $data->fld_whatsapp_no,
            "fld_is_approved"=> $data->fld_is_approved,
            "approved_by"=>  $data->approved_by,
            "fld_is_setup_done"=>$data->fld_is_setup_done,
            "fld_created_by"=> $data->fld_created_by,
            "fld_created_at"=> $data->fld_created_at,
            "fld_created_at"=> $data->fld_created_at,
            "fld_is_current_tanent"=>  $data->fld_is_current_tanent,
            "device_type"=>  $data->device_type,
            "fld_company_id"=>  $data->fld_company_id,
            "device_id"=>  $data->device_id,
            "fld_location"=>  $data->fld_location,
            "languages"=>  $data->languages,
            "is_email_verified"=>  $data->is_email_verified,
            "is_phone_verified"=>  $data->is_phone_verified,
            "login_type"=>  $data->login_type,
            "remember_token"=>  $data->remember_token,
            "verification_code"=> $data->verification_code,
            "fld_room_name"=>  $data->fld_room_name,
            "fld_custom_room_name"=> trim($data->fld_custom_room_name),
            "fld_building"=>  $data->fld_building,
            "fld_whtsappgroup"=> $data->fld_whtsappgroup,
            "fld_bedspace_id"=>  $data->fld_bedspace_id,
            "fld_move_in_date"=>  $data->fld_move_in_date,
            "Deposit_committed"=>  $data->Deposit_paid,
            "Deposit_paid"=>  $deposit_amount,
            "fld_payment_due_date"=>  $data->fld_payment_due_date,
            "booking_id"=>  $data->booking_id,
            "fld_rent"=>  $data->fld_rent,
            "fld_comission"=>  $data->fld_comission,
            "fld_minimum_stay"=>  $data->fld_minimum_stay,
            "fld_comments"=>  $data->fld_comments,
            "fld_contract_image"=> $data->fld_contract_image,
            "fld_rent_paid"=>  $data->fld_rent_paid,
            "fld_booked_by"=> $data->fld_booked_by,
            "last_payment_date"=> $last_pay,
            "last_payment_amount"=> $paid_rent,
            "next_payment_amount"=> 'immediate',
            "room_image"=> $room_image,
            "bal"=> $bal,
            "fld_payment_frequency"=>  $data->fld_payment_frequency );

           array_push($json, $bus);

        }

             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
        }
                     public function my_bookings_details($booking_id)
        {

            $data = DB::table('tbl_booking_history')
            ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_booking_history.fld_tenent_id')
            ->leftjoin('tbl_rooms', 'tbl_booking_history.fld_room_id', '=', 'tbl_rooms.fld_id')
            ->leftjoin('tbl_building', 'tbl_rooms.fld_building_id', '=', 'tbl_building.fld_id')
            ->leftjoin('tbl_rent_status', 'tbl_rent_status.fld_tanent_id', '=', 'tbl_users.id')
            // ->join('tbl_location', 'tbl_building.fld_area', '=', 'tbl_location.id')
            ->where('tbl_booking_history.fld_id',$booking_id)
            ->select('tbl_users.*','tbl_rooms.fld_room_name','tbl_rooms.fld_id AS Room_id','tbl_rooms.fld_custom_room_name', 'tbl_building.fld_building', 'tbl_building.fld_whtsappgroup', 'tbl_booking_history.fld_bedspace_id', 'tbl_booking_history.fld_move_in_date', 'tbl_booking_history.fld_deposit AS Deposit_paid','tbl_booking_history.fld_payment_due_date', 'tbl_booking_history.fld_rent', 'tbl_booking_history.fld_comission', 'tbl_booking_history.fld_minimum_stay', 'tbl_booking_history.fld_comments', 'tbl_booking_history.fld_contract_image','tbl_rent_status.fld_rent_paid', 'tbl_booking_history.fld_booked_by', 'tbl_booking_history.fld_payment_frequency', 'tbl_booking_history.fld_id AS booking_id','tbl_booking_history.fld_is_current_tanent' )->groupBy('fld_bedspace_id')->first();
            $json = [];


            $roomdata = DB::table('tbl_room_pics')->where('fld_room_id', $data->Room_id)->first();

            $target_path = config('app.url').'public/room_pic/';
            // config('app.url');
    
             // print_r($roomdata);
             // die;
             if(!empty($roomdata))
             {
                $room_image = $target_path.$roomdata->fld_name;
             } else {
               
                $room_image = env('ROOMDADDY_PATH').'/rooms/ROOM_IMAGES/property1.jpg';
             }


            $payment_history = DB::table('tbl_rent_status')
                               ->WHERE('fld_booking_id', $booking_id)->orderBy('fld_id','DESC')->first();
            //print_r($payment_history);

            if(!empty($payment_history->fld_paid_date))
            {
                $last_pay = $payment_history->fld_paid_date;
            } else {
                $last_pay = '';
            }

            if(!empty($payment_history->fld_rent_paid))
            {
                $last_rent = $payment_history->fld_rent_paid;
            } else {
                $last_rent = '';
            }
          
            $payment_history = DB::table('tbl_deposit_in')
                               ->WHERE('fld_booking_id', $booking_id)
                                ->sum('fld_deposit');
                   

       if(!empty($payment_history))
       {
        $deposit_amount = $payment_history;
       } else {
        $deposit_amount = 0;
       }

          if(!empty($data->fld_profile_pic))
         {
             $fld_profile_pic = config('app.url').'public/assets/profiles/'.$data->fld_profile_pic;
         } else {
            $fld_profile_pic = '';
         }

          $month_date = date('m Y');


           $last_rent_amount = DB::table('tbl_rent_status')
                               ->WHERE('fld_booking_id', $booking_id)
                                ->orderBy('fld_id','DESC')->first(); 
          if(!empty($last_rent_amount))
          {
            $paid_rent = $last_rent_amount->fld_rent_paid;
          }  else {
            $paid_rent = 0;
          }

          $total_rent_paid = DB::table('tbl_rent_status')
                                     ->WHERE('fld_booking_id', $booking_id)
                                      ->sum('fld_rent_paid');
               
            $end_date = date('Y-m-d');

            $start_date = $data->fld_move_in_date;
            $to   = \Carbon\Carbon::createFromFormat('Y-m-d', $start_date);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d', $end_date);
            $diff_in_months = $to->diffInMonths($from);

            $total_exp_rent = ($diff_in_months * $data->fld_rent);

            $bal = $total_exp_rent - $total_rent_paid;


              $bus = array(
             "id"=>  $data->id,
            "fld_name"=> $data->fld_name,
            "Room_id"=> $data->Room_id,
            "fld_number"=>  $data->fld_number,
            "fld_secondary_number"=>  $data->fld_secondary_number,
            "fld_profile_pic"=>  $fld_profile_pic,
            "email"=>  $data->email,
            "password"=>  $data->password,
            "fld_country_id"=>  $data->fld_country_id,
            "fld_sex"=>  $data->fld_sex,
            "dob_year"=>  $data->dob_year,
            "dob_month"=>  $data->dob_month,
            "dob_day"=>  $data->dob_day,
            "fld_user_type"=> $data->fld_user_type,
            "fld_is_active"=>  $data->fld_is_active,
            "fld_whatsapp_no"=>  $data->fld_whatsapp_no,
            "fld_is_approved"=> $data->fld_is_approved,
            "approved_by"=>  $data->approved_by,
            "fld_is_setup_done"=>$data->fld_is_setup_done,
            "fld_created_by"=> $data->fld_created_by,
            "fld_created_at"=> $data->fld_created_at,
            "fld_updated_at"=>  $data->fld_updated_at,
            "device_type"=>  $data->device_type,
            "fld_is_current_tanent"=>  $data->fld_is_current_tanent,
            "fld_company_id"=>  $data->fld_company_id,
            "device_id"=>  $data->device_id,
            "fld_location"=>  $data->fld_location,
            "languages"=>  $data->languages,
            "is_email_verified"=>  $data->is_email_verified,
            "is_phone_verified"=>  $data->is_phone_verified,
            "login_type"=>  $data->login_type,
            "remember_token"=>  $data->remember_token,
            "verification_code"=> $data->verification_code,
            "fld_room_name"=>  $data->fld_room_name,
            "fld_custom_room_name"=> trim($data->fld_custom_room_name),
            "fld_building"=>  $data->fld_building,
            "fld_whtsappgroup"=> $data->fld_whtsappgroup,
            "fld_bedspace_id"=>  $data->fld_bedspace_id,
            "fld_move_in_date"=>  $data->fld_move_in_date,
            "Deposit_committed"=>  $data->Deposit_paid,
            "Deposit_paid"=>  $deposit_amount,
            "fld_payment_due_date"=>  $data->fld_payment_due_date,
            "booking_id"=>  $data->booking_id,
            "fld_rent"=>  $data->fld_rent,
            "fld_comission"=>  $data->fld_comission,
            "fld_minimum_stay"=>  $data->fld_minimum_stay,
            "fld_comments"=>  $data->fld_comments,
            "fld_contract_image"=> $data->fld_contract_image,
            "fld_rent_paid"=>  $data->fld_rent_paid,
            "fld_booked_by"=> $data->fld_booked_by,
            "last_payment_date"=> $last_pay,
            "last_payment_amount"=> $paid_rent,
            "next_payment_amount"=> 'immediate',
            "room_image"=> $room_image,
            "bal"=> $bal,
            "fld_payment_frequency"=>  $data->fld_payment_frequency );

           array_push($json, $bus);


             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    => $json
             ]); 
        }

        public function verifymobile(PhoneVerifyRequest $request) {
       include 'phone.php';
        
        $otp = rand(1000, 9999);
        $uid = $request->user_id;
        $country_code = '+'.$request->country_code;

        $phone_number = $request->phone_number;
        $phone = $country_code.''.$phone_number;

        if (checknumber($country_code,$phone_number)) { 
 
        //save code in table
        $user = User::where('id',$uid)->first();

        // if($user->fld_number == $phone_number || $user->fld_secondary_number == $phone_number)
        //     {

                $user->verification_code = $otp;
                $user->fld_number = $phone_number;
                $user->country_code = $request->country_code;

                $user->save();
                
                $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
                $authToken  = config('app.twilio')['TWILIO_AUTH_TOKEN'];
                $appSid     = config('app.twilio')['TWILIO_APP_SID'];
                $client = new Client($accountSid, $authToken);
            
               

             try{
                $client->messages->create(
                    $phone,
                    array(
                        'from' => '+447480800091',
                        'body' => 'Verification code for roomdaddy is:'.$otp
                    )
                );


                 return response()->json([
                'status'  => true,
                'message' => 'Successfully',
                'data'    => 'Message Sent Successfully'
                 ]); 

                }catch (Exception $e){
                    dd($e->getMessage());
                    echo "Error: " . $e->getMessage();
                }

            // }

            //  else {
            //       return response()->json([
            //     'status'  => false,
            //     'message' => 'Contact Number Not Registered with Roomdaddy',
            //     'data'    => 'Contact Number Not Registered with Roomdaddy.'
            //      ]); 
            // }
    
        } else {
            return response()->json([
                'status'  => false,
                'message' => 'Wrong Country Code or Phone Number',
                'data'    => 'Wrong Country Code or Phone Number'
                 ]); 
        }
    }    

        function code_verify(Request $request){
               
                $code = $request->code;
                $uid  = $request->user_id;

                $user = User::where('id',$uid)->first();

                if($user->verification_code == $code){
                $user->is_phone_verified = 1;
                $user->save();
                return response()->json([
                'status'  => true,
                'message' => 'success' ,
                'data' => $user         
                ]);
                }else{
                return response()->json([
                'status'  => false,
                'message' => 'Verification Code Error'
                ]);
                }
        }

        function verifyemail(Request $request){

                $code = $request->code;
                $uid  = $request->user_id;

                $user = User::where('id',$uid)->first();

                if($user->verification_code == $code){
                $user->is_email_verified = 1;
                $user->save();
                return response()->json([
                'status'  => true,
                'message' => 'success' ,
                'data' => $user         
                ]);
                }else{
                return response()->json([
                'status'  => false,
                'message' => 'Verification Code Error'
                ]);
                }
        }


        function mylisting_approval(Request $request){

               $room_id = $request->room_id;
               $booking = $request->booking;

               $room = Room::WHERE('fld_id', $room_id)->first();


                
                if($booking == 1)
                {   
                    if($request->avalability_date != '')
                    {
                         $room->is_listing_approved = $booking; 
                         $room->avalability_date =  $request->avalability_date;
                         $room->save();

                        $json_data = array('status'=>true,'message'=>"Room Published Successfully."); 
                    } else {
                        $json_data = array('status'=>false,'message'=>"Availability date not defined"); 
                    } 

                }

                $room->is_listing_approved = $booking; 


               
              if($room->save())
                {
                  $json_data = array('status'=>true,'message'=>"Room Status Changed Successfully."); 
                  return response()->json([
                'status'  => true,
                'message' => 'success',   
                 'data' => $json_data        
                ]);
                } else {
                  return response()->json([
                'status'  => false,
                'message' => 'Error',
                 'data' => $json_data           
                ]);
                }


                
        }


        function delete_property($room_id){
                 

            $room = Room::WHERE('fld_id', $room_id)->first();


            $building_id = $room->fld_building_id;

            $building = Building::WHERE('fld_id', $building_id)->delete();
              
            $bedspace = Bedspace::WHERE('fld_room',$room_id)->delete(); 

             $room = Room::WHERE('fld_id', $room_id)->delete();
           
                  return response()->json([
                'status'  => true,
                'message' => 'success'          
                ]);
                
                
        }

        function edit_listing_data($bedspace_id){
                 

        $details = DB::table('tbl_bedspace')
        ->join('tbl_rooms', 'tbl_bedspace.fld_room', '=', 'tbl_rooms.fld_id')
        ->join('tbl_building', 'tbl_bedspace.fld_building_id', '=', 'tbl_building.fld_id')
        ->join('tbl_users', 'tbl_building.fld_tanent', '=', 'tbl_users.id')
        ->leftJoin('tbl_property', 'tbl_building.property_id', '=', 'tbl_property.id')
        ->where('tbl_bedspace.fld_id',$bedspace_id)
        ->select('tbl_property.name AS property_name', 'tbl_building.*', 'tbl_rooms.*', 'tbl_bedspace.*', 'tbl_users.fld_name', 'tbl_users.id','tbl_building.fld_id AS building_id', 'tbl_rooms.fld_id AS room_id', 'tbl_building.fld_area AS location_name'  )->distinct()->first();
          
           
            $target_path = config('app.url').'/public/room_pic/';


         $room_images = RoomPic::select("tbl_room_pics.fld_id AS size","tbl_room_pics.fld_name",DB::raw("CONCAT('$target_path',tbl_room_pics.fld_name) AS path"))->where("fld_room_id", $details->room_id)->get();

             $building_path = config('app.url').'public/PROPERTY_DOC/';

              $building_images = PropertyDocument::select("tbl_property_documents.fld_id AS size","tbl_property_documents.fld_name",DB::raw("CONCAT('$building_path',tbl_property_documents.fld_name) AS path"))->where("fld_property", $details->building_id)->get();


        $cityname = DB::table('tbl_cities')->select('state_name')->where('id', $details->city_id)->first();

         $other_data = array('city_name' => $cityname->state_name, 'room_images' => $room_images,'building_images' => $building_images);

                  return response()->json([
                'status'  => true,
                'message' => 'success',
                'room_details'=> $details,
                'total' =>$other_data
                ]);
                
                
        }

  
        function add_notice(Request $request){
                 
             include_once('send_notification.php');

                 $notice_data = Booking_History::WHERE('fld_id', $request->booking_id)->first();
                 
                $user = DB::table('tbl_users')->WHERE('id', 787)->first();
                $client = DB::table('tbl_users')->WHERE('id', $notice_data->fld_tenent_id)->first();

                 if(!empty($notice_data))
                 {
                     $notice_data->fld_move_out_date = $request->date;
                     $notice_data->fld_is_notice    = 1;
                     $notice_data->move_out_reason  = $request->reason;

                     if($notice_data->save())
                     {
                       $json_data = array('status'=>true,'message'=>"Notice Added Successfully.");
                     } else {
                        $json_data = array('status'=>false,'message'=>"Error in Adding Notice"); 
                     }
                     
                     $message = $client->fld_name .": Added New Notice For Approval";

                      $notification_type = 'Add_Notice';
                   
                       $data= array('fld_sender_id'=>$client->id, 'fld_receiver_id'=>$user->id,
                     'fld_user_type'=>'A','fld_noti_type'=>$notification_type,'fld_message'=>$message,'fld_subcom_id'=>0 );
          
                     $noti =new Notification();

                       $noti->fld_sender_id = $data['fld_sender_id'];
                       $noti->fld_subcom_id = 0;
                       $noti->fld_receiver_id = $data['fld_receiver_id'];
                       $noti->fld_user_type = $data['fld_user_type'];
                       $noti->fld_noti_type = $data['fld_noti_type'];
                       $noti->fld_message = $message;

                       $noti->save();

                       $send = sendPushNotificationToFCMSever($user,$message,$notification_type);

                 }  else {
                       $json_data = array('status'=>false,'message'=>"Booking Id not Found."); 
                     }

                  return response()->json($json_data);
                
                
        }

        function view_notice($booking_id){
                 

                 $notice_data = Booking_History::WHERE('fld_id', $booking_id)->first();

                 if(!empty($notice_data))
                 {
                      $json_data = array('status'=>true,'message'=>"Success", 'data'=> $notice_data); 
                 }  else {
                       $json_data = array('status'=>false,'message'=>"Booking Id not Found."); 
                     }

                  return response()->json($json_data);
                
                
        }


                function payment_history($booking_id){
                 

            $payment_history = DB::table('tbl_rent_status')
            ->join('tbl_booking_history', 'tbl_booking_history.fld_id', '=', 'tbl_rent_status.fld_booking_id')
            ->where('tbl_booking_history.fld_id',$booking_id)
            ->select('tbl_rent_status.fld_paid_date','tbl_rent_status.fld_income_type','tbl_rent_status.fld_date','tbl_rent_status.fld_rent_paid')->distinct()->orderBy('tbl_rent_status.fld_id', 'DESC')->get();
                 
                $json = [];
           
             foreach ($payment_history as $data) {

                     
            $newDate = date("M-Y", strtotime($data->fld_date));
                          
                 $bus = array(
                    "fld_income_type"       => $data->fld_income_type,
                    "fld_rent_paid" => $data->fld_rent_paid,
                    "remarks"       => 'Rent',
                    "fld_paid_date" => $data->fld_paid_date,
                    "fld_date"      => $newDate
                    );
              array_push($json, $bus);
                   }

                  return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $json         
                ]);
                
                
        }


                function deposit_history($booking_id){
                 

            $deposit_history = DB::table('tbl_deposit_in')
            ->join('tbl_booking_history', 'tbl_booking_history.fld_id', '=', 'tbl_deposit_in.fld_booking_id')
            ->where('tbl_booking_history.fld_id',$booking_id)
            ->select('tbl_deposit_in.*')->distinct()->orderBy('tbl_deposit_in.fld_id', 'DESC')->get();
                 
            //     $json = [];
           
            //  foreach ($payment_history as $data) {

                     
            // $newDate = date("M-Y", strtotime($data->fld_date));
                          
            //      $bus = array(
            //         "fld_income_type"       => $data->fld_income_type,
            //         "fld_rent_paid" => $data->fld_rent_paid,
            //         "remarks"       => 'Rent',
            //         "fld_paid_date" => $data->fld_paid_date,
            //         "fld_date"      => $newDate
            //         );
            //   array_push($json, $bus);
            //        }

                  return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $deposit_history         
                ]);
                
                
        }


            public function get_buildings_for_filter(){  
              $building = Property::select('name','id')->groupBy('name')->get();
             
                return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $building         
                ]);

            }  

            public function get_locations_for_filter(){  
              $building = Location::select('name','id')->groupBy('name')->get();
             
                return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $building         
                ]);

            }       

          public function get_locations_autocom($data){  
              $building = Location::where('name', 'LIKE','%'.$data.'%')->select('name','id')->groupBy('name')->get();
             
                return response()->json([
                'status'  => true,
                'message' => 'success',
                'data'=> $building         
                ]);

            }    



}
