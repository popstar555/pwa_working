<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Input;
use DB;
use Response;
use App\Room;
use App\Building;
use App\Bedspace;
use App\RoomPic;
use App\PropertyDocument;
use Carbon\Carbon;
use App\Http\Requests\ParkingRequest;
use App\Parking;
use App\ParkingBooking;
use App\View_Lead;
use Mail;


class LeadController extends Controller{
   
	   public function save_Lead(Request $request){
	   //	return $request->toArray();
         $user_id = $request->user_id;

        $get_last =  View_Lead::where('fld_bedspace_id',$request->bedspace_id)->where('fld_user_id',$user_id)->where('fld_lead_type',$request->lead_type)->where('fld_system_ip',$request->fld_system_ip)->whereDate('created_at', Carbon::today())->first();


        if(empty($get_last->fld_id)){
       
	   	$lead = new View_Lead();
	   	$lead->fld_building_id =  $request->building_id;
	   	$lead->fld_room_id =  $request->room_id;
	   	$lead->fld_bedspace_id =  $request->bedspace_id;
	   	$lead->fld_user_id =  $user_id;
	   	$lead->fld_lead_type =  $request->lead_type;
	   	$lead->fld_system_ip =  $request->fld_system_ip;
	   	$lead->fld_access_type =  'A';

	   	if($lead->save())
	   	{
           $json_data = array('status'=>true,'message'=>"Lead Added Successfully",'data'=>$lead); 
	   	} else {
           $json_data = array('status'=>false,'message'=>"Error in Adding Lead."); 
               }

        } else {
        	$json_data = array('status'=>true,'message'=>"Lead Already Exists",'data'=>$get_last); 
        }
        
        return response()->json($json_data); 
        
	   }


   
	    public function get_Lead($bedspace_id){
         
         $email_lead = View_Lead::where('fld_bedspace_id',$bedspace_id)->where('fld_lead_type','e')->sum('fld_view_count');
         
         $chat_lead = View_Lead::where('fld_bedspace_id',$bedspace_id)->where('fld_lead_type','c')->sum('fld_view_count');
         
         $phone_lead = View_Lead::where('fld_bedspace_id',$bedspace_id)->where('fld_lead_type','p')->sum('fld_view_count'); 

         $total_views = View_Lead::where('fld_bedspace_id',$bedspace_id)->where('fld_lead_type','v')->sum('fld_view_count');
       
        $total_lead = array('email_lead'=>$email_lead,'chat_lead'=>$chat_lead,'phone_lead'=>$phone_lead,'total_views'=>$total_views);

          $json_data = array('status'=>true,'message'=>"Successfull", 'data'=>$total_lead ); 


         return response()->json($json_data); 

       }

	   // public function save_view_counter(Request $request){
	   // //	return $request->toArray();

    //     $user_id = $request->user_id;   

    //     $lead = new View_Lead();
    //     $lead->fld_building_id =  $request->building_id;
	   // 	$lead->fld_room_id =  $request->room_id;
	   // 	$lead->fld_bedspace_id =  $request->bedspace_id;
	   // 	$lead->fld_user_id =  $user_id;
	   // 	$lead->fld_lead_type =  'v';
	   // 	$lead->fld_system_ip =  $_SERVER['REMOTE_ADDR'];	
	   // 	$lead->fld_access_type =  'W';
	   // 	$lead->fld_view_count =  1;
    //     $lead->save();

	   // 	return "View Counter Updated";
        

	   // }

}