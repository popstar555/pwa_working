<?php

namespace App\Http\Controllers\api;

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ParkingRequest;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Parking;
use App\ParkingBooking;
use App\Property;
use App\Building;
use DB;
use Auth;
use Carbon\Carbon;
use Mail;
use App\Country;
use App\Location;

class ParkingController extends Controller{

    
     public function parking_step_one(Request $request)
     {  
           // dd($request);
	       // $build_id = Property::where('name',$request->build_id)->pluck('id')->first();
            $user_details = User::where('id',$request->user_id)->first();

			$avalability_date = Carbon::parse($request->availability_start)->format('Y-m-d');


            if(!empty($user_details->fld_number))
            {
                $contact = $user_details->fld_number;
            } else {
                $contact = $user_details->fld_secondary_number;
            }


			$parking = new Parking();
			$parking->prk_title = $request->title;
			$parking->prk_building_id =$request->build_id;
            $parking->location =$request->location;
            $parking->city =$request->city;
            $parking->city_id =$request->city_id;
			$parking->prk_street_address = $request->search_park_street;
			$parking->initial_deposit = $request->initial_deposit;
			$parking->prk_start_date = $avalability_date;

             if ($request->availability_end == '') {
                $parking->prk_end_date = '0000-00-00';
             } else {

             $availability_end = Carbon::parse($request->availability_end)->format('Y-m-d');
                $parking->prk_end_date = $availability_end;
             }
			
			// $parking->contact_number = $contact;
			$parking->comments = $request->parking_description;
			$parking->prk_status = 0;
			$parking->added_by =$request->user_id;
			$parking->search_latitude = $request->search_latitude;
			$parking->search_longitude = $request->search_longitude;
			$parking->parking_type = $request->parking_type;
			$parking->monthly_price = $request->monthly_price;
			// $parking->quaterly_price = $request->quaterly_price;
			// $parking->yearly_price = $request->yearly_price;
			//$parking->vehicle_number = $request->vehicle_number;
			// $parking->parking_bay_number = $request->parking_bay_number;
			$parking->parking_bay_value = $request->parking_bay_value;
			$parking->save();

     	  return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $parking->prk_id
             ]); 
         

     }

     public function get_parking_list(){

     	$Parking =  DB::table('tbl_parking')
                        // ->leftjoin('tbl_parking_vehicle_type', 'tbl_parking.parking_vehicle_size', '=', 'tbl_parking_vehicle_type.pvt_id')
                        ->leftjoin('tbl_parking_type', 'tbl_parking.parking_type', '=', 'tbl_parking_type.prk_type_id')
                        ->leftjoin('tbl_parking_floors', 'tbl_parking.parking_bay_value', '=', 'tbl_parking_floors.value')
                        ->leftjoin('tbl_property', 'tbl_parking.prk_building_id', '=', 'tbl_property.id')
                        ->leftjoin('tbl_building', 'tbl_building.fld_id', '=', 'tbl_parking.prk_building_id')
                        ->leftjoin('tbl_cities', 'tbl_building.city_id', '=', 'tbl_cities.id')
                        ->leftjoin('tbl_countries', 'tbl_cities.country_id', '=', 'tbl_countries.id')
                        ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')
                      // 'tbl_parking_vehicle_type.title'
                        ->select('tbl_parking.*','tbl_parking_type.title AS Parking_type','tbl_parking_floors.label AS Floor','tbl_property.name','tbl_users.fld_number AS Owner_contact' ,'tbl_users.country_code','tbl_building.fld_longitude','tbl_building.fld_latitude','tbl_countries.currency')
                        ->where('tbl_parking.prk_status', '!=', 3)
                        ->where(function($query) {
                            return $query->where('tbl_parking.prk_status',1)
                                ->orWhere('tbl_parking.prk_status', 4);
                        })
                        ->paginate(8);
                 $json = [];
                        foreach($Parking As $apt)
                        {

                if($apt->added_from == '2')
                  {

                    $search_latitude  =  $apt->fld_latitude; 
                    $search_longitude =  $apt->fld_longitude;
                  } else {
                    $search_latitude  =  $apt->search_latitude;
                    $search_longitude =  $apt->search_longitude;
                  } 

            $pagination_data = array('current_page' => $Parking->currentPage(), 'lastPage' => $Parking->lastPage(), 'previousPageUrl' => $Parking->previousPageUrl(), 'nextPageUrl' => $Parking->nextPageUrl() );

                 $bus = array(
                'prk_id'                   => $apt->prk_id,
                'prk_title'                => $apt->prk_title,
                'prk_building_id'          => $apt->prk_building_id,
                'prk_street_address'       => $apt->prk_street_address,
                'initial_deposit'          => $apt->initial_deposit,
                'prk_start_date'           => $apt->prk_start_date,
                'prk_end_date'             => $apt->prk_end_date,
                'contact_number'           => $apt->country_code.$apt->contact_number,
                'comments'                 => $apt->comments,
                'currency'                 => $apt->currency,
                'prk_status'               => $apt->prk_status,
                'created_at'               => $apt->created_at,
                'updated_at'               => $apt->updated_at,
                'search_latitude'          => $search_latitude,
                'search_longitude'         => $search_longitude,
                'parking_bay_number'       => $apt->parking_bay_number,
                'parking_bay_value'        => $apt->parking_bay_value,
                'parking_type'             => $apt->parking_type,
                'parking_vehicle_size'     => $apt->parking_vehicle_size,
                'added_from'               => $apt->added_from,
                'monthly_price'            => $apt->monthly_price,
                'vehicle_number'           => $apt->vehicle_number,
                // 'title'                    => $apt->title,
                'Parking_type'             => $apt->Parking_type,
                'Floor'                    => $apt->Floor,
                'Owner_contact'            => $apt->Owner_contact,
                'country_code'             => $apt->country_code,
                'name'                     => $apt->name);


               array_push($json, $bus);
                        }
         	 return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'pagination_data' => $pagination_data,
            'data'    =>  $json,
             ]); 

     }    

      public function parking_floors(){

        $Parking = DB::table('tbl_parking_floors')->get();
        
             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $Parking
             ]); 

     }


     public function get_parking_by_tenant_id($id){

        	$Parking = Parking::where('added_by',$id)->get();
        
         	 return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $Parking
             ]); 

     }
    
     public function get_parking_by_id($id){

            $Parking = DB::table('tbl_parking')
                        ->leftjoin('tbl_parking_vehicle_type', 'tbl_parking.parking_vehicle_size', '=', 'tbl_parking_vehicle_type.pvt_id')
                        ->leftjoin('tbl_parking_type', 'tbl_parking.parking_type', '=', 'tbl_parking_type.prk_type_id')
                        ->leftjoin('tbl_parking_floors', 'tbl_parking.parking_bay_value', '=', 'tbl_parking_floors.value')
                        ->leftjoin('tbl_property', 'tbl_parking.prk_building_id', '=', 'tbl_property.id')
                        ->leftjoin('tbl_building', 'tbl_building.fld_id', '=', 'tbl_parking.prk_building_id')
                        ->leftjoin('tbl_users', 'tbl_users.id', '=', 'tbl_building.fld_tanent')

                        ->where('prk_id',$id)
                        ->select('tbl_parking.*', 'tbl_parking_vehicle_type.title','tbl_parking_type.title AS Parking_type','tbl_parking_floors.label AS Floor','tbl_property.name','tbl_users.fld_number AS Owner_contact' ,'tbl_users.country_code')
                        ->first();
              $contact = '+'.$Parking->country_code.$Parking->Owner_contact;
             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $Parking,
            'contact' => $contact
             ]); 

     }

     public function delete_parking_by_id($id){

        	$Parking = Parking::where('prk_id',$id)->delete();
        
         	 return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $Parking
             ]); 

     }
          public function change_parking_status($id,$status,Request $request){

             $Parking = Parking::where('prk_id',$id)->first();
             $Parking->prk_status = $status;

             if($status == 1)
             {
              $Parking->prk_start_date = $request->avalability_date;  
             }

             $Parking->save();
        
             return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $Parking
             ]); 

     }
     

     public function edit_parking($parking_id ,Request $request){

            $parking = Parking::where('prk_id',$parking_id)->first();

            $avalability_date = Carbon::parse($request->availability_start)->format('Y-m-d');
            $availability_end = Carbon::parse($request->availability_end)->format('Y-m-d');

            $parking->prk_title = $request->title;
            $parking->prk_building_id =$request->build_id;
            $parking->prk_street_address = $request->search_park_street;
            $parking->initial_deposit = $request->initial_deposit;
            $parking->prk_start_date = $avalability_date;
            $parking->prk_end_date = $availability_end;
            $parking->comments = $request->parking_description;
            $parking->search_latitude = $request->search_latitude;
            $parking->search_longitude = $request->search_longitude;
            $parking->parking_type = $request->parking_type;
            $parking->monthly_price = $request->monthly_price;
            $parking->parking_bay_number = $request->parking_bay_number;
            $parking->parking_bay_value = $request->parking_bay_value;
            $parking->save();

          return response()->json([
            'status'  => true,
            'message' => 'Successfully',
            'data'    =>  $parking
             ]); 
         


     }

    }