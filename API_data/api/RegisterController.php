<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use DB;
use App\User;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\AvatarAssigned;

class RegisterController extends Controller
{
    public function index(Request $request){

       $checkfb = $request->checkfb;
      
       if(!empty($request->email) && !empty($request->name) && !empty($request->password) )
       {
       $email_exists = User::where('email',$request->email)->first();

       if($checkfb == 'REGISTER')
       {
               if(empty($email_exists))
               {
                    $otp = rand(1000, 9999);
                    $user = new User;
                    $user->fld_name = $request->name;
                    $user->email = $request->email;
                    $user->password = bcrypt($request->password);
                    $user->fld_number = $request->contact;
                    $user->fld_user_type = 'T';
                    $user->approved_by = 0;
                    $user->verification_code = $otp;
                    $user->login_type = 'REGISTER';
                             //upload file
                        if ($request->hasFile('fld_profile_pic')) {
                            $image = $request->file('fld_profile_pic');
                            $name = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('assets/profiles');
                            $image->move($destinationPath, $name);
                            $user->fld_profile_pic = $name;
                            $user->is_avatar = 0;
                        }
                    $user->save();
                     
                     if(!empty($request->avatar))
                     {
                      $Avatar = new AvatarAssigned();

                     $Avatar->avatar_id = $request->avatar;
                     $Avatar->user_id = $user->id;
                     $Avatar->save(); 

                     $user = User::where('id',$user->id)->first();
                      
                      $user->is_avatar = 1;
                      $user->save();
                      }


                    //send mail to user with verify link

                    $data = array(
                    'uid'   => base64_encode($user->id),
                    'name'  => $user->fld_name,
                    'email' => $user->email,
                    'otp'   => $otp
                    );

                    Mail::send('email.confirmEmail', $data, function($message) use ($data) {
                    $message->to($data['email'], 'RoomDaddy')
                    ->subject('RoomDaddy Confirm Email');
                    $message->from('info@roomdaddy.ae','RoomDaddy');
                    });

                    return response()->json([
                    'status'  => true,
                    'message' => 'User Registered Successfully',
                    'data'    => 'Please Verify Your Email throught Link sent on Registered mail Id.',
                    'id'      =>  $user->id
                    ]);    
                } else {

                                return response()->json([
                                'status'  => false,
                                'message' => '!OOPS, Email Already Exists.',
                                'data'    => $email_exists
                            ]);
                       }
    
       } else
               {
                        if(empty($email_exists))
                           {   

                            $otp = rand(1000, 9999);
                                $user = new User;
                                $user->fld_name = $request->name;
                                $user->email = $request->email;
                                $user->fld_number = $request->contact;
                                $user->password = bcrypt($request->password);
                                $user->fld_user_type = 'T';
                                $user->approved_by = 0;
                                if($checkfb != '')
                                    { $user->login_type = $checkfb;
                                    } else {
                                      $user->login_type = 'GOOGLE';
                                    }
                                $user->verification_code = $otp;
                                $user->is_email_verified = 1;
                                $user->save();

                                //send mail to user with verify link

                                // $data = array(
                                // 'uid'   => base64_encode($user->id),
                                // 'name'  => $user->fld_name,
                                // 'email' => $user->email,
                                // 'otp'   => $otp
                                // );

                                // Mail::send('email.confirmEmail', $data, function($message) use ($data) {
                                // $message->to($data['email'], 'RoomDaddy')
                                // ->subject('RoomDaddy Confirm Email');
                                // $message->from('info@roomdaddy.ae','RoomDaddy');
                                // });

                                return response()->json([
                                'status'  => true,
                                'message' => 'User Registered Successfully! Please Verify Your Email.',
                                'data'    => $user
                                ]);    
                            } else {
                                 return response()->json([
                                'status'  => true,
                                'message' => 'Email Already Exists.',
                                'data'    => $email_exists
                            ]); 
                            }
                }

      } else {
                            return response()->json([
                                'status'  => false,
                                'message' => 'Please Fill All Fields'
                            ]); 
      }

        
    }

        public function getallcountry($country_id){
     
       $countries = Country::where('id',$country_id)->first();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $countries
        ]);

    }

        public function getcountry(){
     
       $country = Country::get();
          $json = [];  
                $target_path = config('app.url')."public/flags/32x32/";
        foreach ($country as  $countries) {
         $data = array(
                    "id"=> $countries->id,
                    "sortname"=> $countries->sortname,
                    "name"=> $countries->name,
                    "phonecode"=> $countries->phonecode,
                    "image"=> $target_path.strtolower($countries->sortname).".png"
                    );

           array_push($json, $data);

        }
        return response()->json([
        'status' => true,
        'message' => 'success',
        'data' => $json
        ]);

    }

            public function login($email,$pass){


     
            $user = DB::table('tbl_users')
                  ->where(function($query) use($email) {
                        return $query->where('email',$email);
                           //  ->orwhere('fld_number',$email)
                           //  ->orWhere('fld_number','+'.$email)
                           //  ->orWhere('fld_whatsapp_no',$email)
                           //  ->orWhere('fld_whatsapp_no','+'.$email);
                    })->first();



                    if (password_verify($pass, $user->password)) 
                    {

                             if($user->is_email_verified == 0){
                                    return response()->json([
                                    'status' => false,
                                    'message' => 'Email Not verified',
                                    'data' => $user
                                    ]);
                             }  else {
                                    return response()->json([
                                    'status' => true,
                                    'message' => 'success',
                                    'data' => $user
                                    ]);
                             }  
                    } else {
                         return response()->json([
                                    'status' => false,
                                    'message' => 'Email Or Password is Wrong'  ]);
                    } 




    }

     public function change_password(Request $request){
            $email_exists = User::where('id',$request->user_id)->first();
             
             $old_pas = $request->old_pas;
             $new_pas = $request->new_pas;
             $conf_pass = $request->conf_pass;
              
        if($new_pas != $old_pas)
        {

              if($new_pas == $conf_pass)
              {
                     if(!empty($email_exists))
                    {     
                           if (password_verify($old_pas, $email_exists->password)) 
                           {
                             $email_exists->password = bcrypt($new_pas);
                             if($email_exists->save())
                             {
                              $json = array('status' => true, 'message' => "Password Updated Successfully"); 
                               }  else {
                              $json = array('status' => false, 'message' => "Error in Updating Password"); 
                               }
                           } else {
                         $json = array('status' => false, 'message' => "Old password do not match Existing Password");     
                           }

                    } else {
                      $json = array('status' => false, 'message' => "Id doesn't Exists." );    
                    }

              } else {
                $json = array('status' => false, 'message' => 'New Password And Confirm Password do not match');
              }
        } else {
               $json = array('status' => false, 'message' => "New Password And Old Password can't be same");
        }  


              return response()->json($json);  



     }
        public function getarea(){
     
       $city = DB::table('tbl_cities')->whereBetween('state_id', array(3796, 3804))->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $city
        ]);

    }
            public function getcity(){
          ini_set('memory_limit', '64M');
          $city = DB::table('tbl_cities')->where('country_id',229)->get();
        //  $city = DB::table('tbl_cities')->get();
         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $city
        ]);

    }

                public function getcityautocomplete($city){

          ini_set('memory_limit', '64M');
          $city = DB::table('tbl_cities')->where('name', 'like', $city.'%')->get();
       
         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $city
        ]);

    }

        public function getstateByCountry_id($country_id){
     
       $states = DB::table('tbl_states')->where('country_id', $country_id)->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $states
        ]);

    }

        public function gecitybyid($state_id){
     
       $cities = DB::table('tbl_cities')->where('state_id', $state_id)->groupBy('name')->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $cities
        ]);

    }

        public function gePropertybyid($city_id){
     
       $property = DB::table('tbl_property')->where('fld_city_id', $city_id)->groupBy('name')->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $property
        ]);

    }

            public function getProperty($search){
              

            // $country_id = DB::table('tbl_cities')->where('id', $city_id)->select('country_id')->first();

            $property = DB::table('tbl_property')->where('name','LIKE', '%'.$search.'%')->groupBy('tbl_property.name')->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $property
        ]);

     
    }

                public function Property(){
              

            $property = DB::table('tbl_property')->groupBy('name')->limit(1500)->get();

                 return response()->json([
                    'status' => true,
                    'message' => 'success',
                    'data' => $property
                ]);

     
    }

            public function getLocation($city_id){
            // DB::enableQueryLog();
            $country_id = DB::table('tbl_cities')->where('id', $city_id)->select('country_id')->first();
              
            // $dblog = DB::getQueryLog();
                $property = DB::table('tbl_location')->where('country_id',$country_id->country_id)->groupBy('name')->get();

            

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $property
        ]);

     
    }

                public function Location(){
                $property = DB::table('tbl_location')->groupBy('name')->get();
            
                     return response()->json([
                        'status' => true,
                        'message' => 'success',
                        'data' => $property
                    ]);

     
    }
                public function update_device_id(Request $req){
                $user = User::find($req->user_id);
                
                $user->device_type = $req->device_type;
                $user->device_id = $req->device_id;
                $user->save();

                     return response()->json([
                        'status' => true,
                        'message' => 'success',
                        'data' => $user
                    ]);

     
    }

                public function getLanguage(){

            $property = DB::table('tbl_languages')->groupBy('name')->get();

         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $property
        ]);

     
    }
         public function get_profile(Request $request){
                
			$uid = $request->user_id;
			$property = User::where('id',$uid)->first();
			$path = $this->domain =config('app.url');

            if($property != '')
            {
			if($property->is_avatar == 0)
				{

				if(!empty($property->fld_profile_pic))
				{
				$image = $path.'public/assets/profiles/'.$property->fld_profile_pic; 

				} else{

				$image = $path.'public/assets/profiles/user.jpeg';
				}

             } else {

                 $check_avtar = DB::table('tbl_avatar_assigned_to_users')->where('user_id',$property->id)->first();

                 $get_avtar = DB::table('tbl_avatars')->where('id',$check_avtar->avatar_id)->first();

                // dd($get_avtar);



                     if(!empty($get_avtar->name))
                   {
                      $image = $path.'public/assets/avatar/png/'.$get_avtar->name; 

                   } else{

                    $image = $path.'public/assets/profiles/user.jpeg';
                   }
             }

         }

            if(!empty($property)) {
            	 $lang_data = $property->languages;
            } else {
            	$lang_data = '';
            }

       

            if(!empty($lang_data[0]))
            { 
                  $lang =  unserialize($property->languages);
                  $languages = implode(",",$lang);
            } else {
                 $languages =  ''; 
            }
        
           if(!empty($property->fld_number))
           {
            $number = $property->fld_number;
           } else {
            $number = '';
           }

               if(!empty($property))
               {

       $data = array(
                    "id"=>  $property->id,
                    "fld_name"=> $property->fld_name,
                    "fld_number"=> $number,
                    "fld_secondary_number"=> $property->fld_secondary_number,
                    "fld_profile_pic"=> $image,
                    "email"=> $property->email,
                    "country_code"=> "$property->country_code",
                    "password"=> $property->password,
                    "fld_country_id"=> $property->fld_country_id,
                    "fld_sex"=> $property->fld_sex,
                    "dob_year"=> $property->dob_year,
                    "dob_month"=> $property->dob_month,
                    "dob_day"=> $property->dob_day,
                    "fld_user_type"=> $property->fld_user_type,
                    "fld_is_active"=> $property->fld_is_active,
                    "fld_whatsapp_no"=> $property->fld_whatsapp_no,
                    "fld_is_approved"=> $property->fld_is_approved,
                    "approved_by"=> $property->approved_by,
                    "fld_is_setup_done"=> $property->fld_is_setup_done,
                    "fld_created_by"=> $property->fld_created_by,
                    "fld_created_at"=> $property->fld_created_at,
                    "fld_updated_at"=> $property->fld_updated_at,
                    "device_type"=> $property->device_type,
                    "fld_company_id"=> $property->fld_company_id,
                    "device_id"=> $property->device_id,
                    "fld_location"=> $property->fld_location,
                    "languages"=> $languages,
                    "is_email_verified"=> $property->is_email_verified,
                    "is_phone_verified"=> $property->is_phone_verified,
                    "remember_token"=> $property->remember_token,
                    "verification_code"=> $property->verification_code
               
               );



         return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => $data
            
        ]);

     }
      return response()->json([
            'status' => true,
            'message' => 'success',
            'data' => ''
            
        ]);

     
    }

                    public function edit_profile(Request $request){
          
             // $data = $request->all();
             //    return response()->json([
             //    'status'  => true,
             //    'message' => 'Data coming from your side.',
             //    'data'    => $data
             //    ]); 

             //  die('llll');

             
             $uid = $request->user_id;
                
            $user = User::where('id',$uid)->first();

               if(!empty($user))
                {
                        $user = User::where('id',$uid)->first();
                        $user->fld_name = $request->username;
                        if($request->sex != '' && $request->sex == 'female')
                       {
                        $user->fld_sex = "F";
                        } else {
                        $user->fld_sex = "M";  
                        }
                        $user->country_code = $request->country_code;

                        if(!empty($request->fld_number) && $user->fld_number != $request->fld_number)
                        {
                        $user->fld_number = $request->fld_number; 
                        $user->is_phone_verified = 0;
                        

                      $json_data = array('status' => true, 'message' => 'Profile Update Successfully. Please vefify Phone Number');                 
                        } else {
                         $json_data = array('status' => true, 'message' => 'Profile Update Successfully.');  
                        }

 
                       $time=strtotime($request->dob);

                         $month=date("m",$time);
                         $year=date("Y",$time);
                         $date=date("d",$time);
                         

                        $user->dob_year = $year;
                        $user->dob_month = $month;
                        $user->dob_day = $date;

                        if($request->languages != ''){

                        $array = explode(',', $request->languages);

                        $user->languages = serialize($array);
                        }
                                //upload file
                        if ($request->hasFile('fld_profile_pic')) {
                            $image = $request->file('fld_profile_pic');
                            $name = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('assets/profiles');
                            $image->move($destinationPath, $name);
                            $user->fld_profile_pic = $name;
                            $user->is_avatar = 0;
                        }


                        if($user->save())
                        {

                            return response()->json([$json_data]); 
                        }

                } else {
                    return response()->json([
                            'status' => false,
                            'message' => 'User id not found.'
                           
                            ]); 
                }

     
    }



}
