<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use DB;
use App\User;
use App\Building;
use Auth;
use Mail;
use App\Country;
use App\Location;
use App\AvatarAssigned;

class UserController extends Controller
{
   public function user_details($id)
  {
      //$user = User::find($id);
      // obj $user->id;
      // array $user[0]->id;
     $user = DB::table('tbl_users')

            ->join('tbl_avatar_assigned_to_users', 'tbl_users.id'  ,'=','tbl_avatar_assigned_to_users.user_id')
            ->join('tbl_avatars', 'tbl_avatar_assigned_to_users.avatar_id', '=', 'tbl_avatars.id')
            ->where('tbl_users.id',$id)
            ->first();
            $path = config('app.url');
            
            if($user != '')
            {
            if($user->is_avatar == 0)
                {

                if(!empty($user->fld_profile_pic))
                {
                $image = $path.'public/assets/profiles/'.$user->fld_profile_pic; 

                } else{

                $image = $path.'public/assets/profiles/user.jpeg';
                }
            } else{


                     if(!empty($user->name))
                   {
                      $image = $path.'public/assets/avatar/png/'.$user->name; 

                   } else{

                    $image = $path.'public/assets/profiles/user.jpeg';
                   }
            }

        }

      return response()->json([$user,$image]);
  }
  // public function form_submit($id)
  // {
  //   return ('hh');
  // }
  // public function step_one(Request $data){
             

  //            $Property = new Building;
  //            $Property->property_type = $data->property_type;
  //            if($data->property_type == 'apartment' && $data->property_id != '')
  //            {
  //             $Property->property_id = $data->property_id;
  //             $prop = DB::table('tbl_property')->where('id', $data->property_id)->first();  
  //             $Property->fld_building = $prop->name;  
  //            }

  //            $area = DB::table('tbl_location')->where('id', $data->fld_area)->first();
        
  //            if($Property->save())
  //            {  

  //                 return response()->json([
  //           'status'  => true,
  //           'id'  => $Property->fld_id,
  //           'message' => 'Building Added SuccessFully'
  //              ]);
  //            } else {
  //                 return response()->json([
  //           'status'  => false,
  //           'message' => '!OOPS, Some Error Occoured'
  //             ]);
  //            }
 

  //           }

  //            public function edit_step_one(Request $data){
              
  //            if(!empty($data->fld_building_id)) {
  //            $Property =  Building::where('fld_id',$data->fld_building_id)->first();
  //          }
           
  //            $Property->property_type = $data->property_type;
  //          if($data->property_type == 'apartment' && $data->property_id != '')
  //            {
  //             $Property->property_id = $data->property_id;
  //             $prop = DB::table('tbl_property')->where('id', $data->property_id)->first();  
  //             $Property->fld_building = $prop->name;  
  //            }

  //           $Property->fld_area = $data->fld_area;
  //          $Property->fld_address = $data->fld_address;
  //          $Property->fld_latitude = $data->fld_latitude;
  //          $Property->fld_longitude = $data->fld_longitude;
  //          $Property->city_id  = $data->city_id;

  //            if($Property->save())
  //            {  

  //               return response()->json([
  //               'status'  => true,
  //               'message' => 'Building Edited SuccessFully'
  //               ]);
  //            } else {

  //               return response()->json([
  //               'status'  => false,
  //               'message' => '!OOPS, Some Error Occoured'
  //               ]);
  //            }


  //           }

  //            public function step_two(Request $data){
               
  //            // return response()->json($data); 


             
  //              $Property = new Room;
  //            $Property->fld_building_id = $data->fld_building_id;
  //            $Property->private_washroom_amenities = $data->bath == 'private_washroom' ? 1 : 0;
  //            $Property->shared_washroom_amenities = $data->bath  == 'shared_washroom' ? 1 : 0;
  //            $Property->room_mates_male = $data->room_mates_male;
  //            $Property->room_mates_female = $data->room_mates_female;
  //            $Property->occupancy = $data->occupancy;
  //            $Property->fld_custom_room_name = $data->photos_listing_title;
  //            $Property->photos_listing_title = $data->photos_listing_title;
  //            $Property->gender_room = $data->roommate_gender;
  //            $Property->is_listing_approved = 1;
              
  //              // amenities room 
  //               if(!empty($_REQUEST['Room_amenities']))
  //               {
  //                  $Room_amenities = $_REQUEST['Room_amenities'];
  //                  $Room_amenities1 = explode(',', $Room_amenities);
  //               } else {
  //                 $Room_amenities1 = [];
  //               }




  //              if(in_array('TV', $Room_amenities1))
  //              {
  //               $Property->tv_amenities = 1;
  //              } else {  $Property->tv_amenities = 0; }
  //                if(in_array('Private_Balcony', $Room_amenities1))
  //              {
  //               $Property->private_balcony_amenities = 1;
  //              } else {  $Property->private_balcony_amenities = 0; }

  //                if(in_array('Window', $Room_amenities1))
  //              {
  //               $Property->window = 1;
  //              } else {  $Property->window = 0; }

  //                if(in_array('Natrual_Light', $Room_amenities1))
  //              {
  //               $Property->natural_light_amenities = 1;
  //              } else {  $Property->natural_light_amenities = 0; }

  //                if(in_array('Fridge', $Room_amenities1))
  //              {
  //               $Property->fridge_amenities = 1;
  //              }  else {  $Property->fridge_amenities = 0; }


  //              if(!empty($_REQUEST['outDoorAmenties'] ))
  //              {
  //                $outDoorAmenties = $_REQUEST['outDoorAmenties'];
  //                $outDoorAmenty = explode(',', $outDoorAmenties);
  //              } else {
  //                $outDoorAmenty = [];
  //              }
     


      

               
  //               if(in_array('Garden', $outDoorAmenty))
  //              {
  //               $Property->garden_amenities = 1;
  //              } else {  $Property->garden_amenities = 0; }
                 

  //                if(!empty($_REQUEST['house_amenities']))
  //                {
  //                  $house_ameneties = $_REQUEST['house_amenities'];
  //                  $house = explode(',', $house_ameneties);
  //                } else {
  //                   $house = [];
  //                }

  //                if(in_array('Wifi', $house))
  //              {
  //               $Property->wifi_amenities = 1;
  //              }
  //                if(in_array('Wash Machine', $house))
  //              {
  //               $Property->washing_amenities = 1;
  //              }
  //                if(in_array('Common Balcony', $house))
  //              {
  //               $Property->balcony_amenities = 1;
  //              }
  //                if(in_array('Dish Washer', $house))
  //              {
  //               $Property->dishwasher_amenities = 1;
  //              }
  //                if(in_array('Wheelchair Frindly', $house))
  //              {
  //               $Property->wheelcheer_amenities = 1;
  //              }
  //                if(in_array('Garden', $house))
  //              {
  //               $Property->garden_amenities = 1;
  //              }
  //                if(in_array('Terrace', $house))
  //              {
  //               $Property->terrace_amenities = 1;
  //              }
  //                if(in_array('Living Room', $house))
  //              {
  //               $Property->living_room_amenities = 1;
  //              }
             
  //              $Property->other_house_rules = $data->other_rules; 

  //              if($Property->save()) 
  //             {      

  //                      $building =  Building::where('fld_id',$data->fld_building_id)->first();
  //                            // building Ameneties   $Property->bed_type = $data->bed_type;
                    
  //                    if(in_array('Parking', $house))
  //                        {
  //                         $building->parking_amenities = 1;
  //                        }
  //                  // if(in_array('Dish Washer', $house))
  //                  //       {
  //                  //        $building->parking_amenities = 1;
  //                  //       }
  //                  if(in_array('Elevator', $house))
  //                        {
  //                         $building->elevator_amenities = 1;
  //                        }
  //                  if(in_array('Pool', $house))
  //                        {
  //                         $building->pool_amenities = 1;
  //                        }
  //                  if(in_array('gym', $house))
  //                        {
  //                         $building->gym = 1;
  //                        }

                             
  //               if(in_array('Lawn tennis', $outDoorAmenty))
  //              {
  //               $building->lawn_tennis = 1;
  //              }
  //               if(in_array('Sqaush', $outDoorAmenty))
  //              {
  //               $building->squash = 1;
  //              }
  //              if(in_array('Table tenis', $outDoorAmenty))
  //              {
  //               $building->table_tennis = 1;
  //              }
  //              if(in_array('Pool', $outDoorAmenty))
  //              {
  //               $building->pool_amenities = 1;
  //              }

  //              if(in_array('Gym', $outDoorAmenty))
  //              {
  //               $building->gym = 1;
  //              }


               
  //             if(!empty($_REQUEST['house_rules'])){
  //               $house_rules = $_REQUEST['house_rules'];
  //               $house_rl = explode(',', $house_rules);
  //             } else {
  //                $house_rl = [];
  //             }
 
                      


  //                // house rules
  //                          if(in_array('Smoke Friendly', $house_rl))
  //                        {
  //                         $building->house_rules_smoker = 1;
  //                        }
  //                             if(in_array('Pet Friendly', $house_rl))
  //                        {
  //                         $building->house_rules_pet = 1;
  //                        }
  //                             if(in_array('Couples Allowed', $house_rl))
  //                        {
  //                         $building->house_rules_couple = 1;
  //                        }
  //                             if(in_array('Children Allowed', $house_rl))
  //                        {
  //                         $building->house_rules_children = 1;
  //                        }
                
  //                  if($building->save()) 
  //                  {  
  //                          $Bedspace = new Bedspace;
                           
  //                          $Bedspace->fld_room        = $Property->fld_id;
  //                          $Bedspace->fld_building_id = $data->fld_building_id;
  //                          $Bedspace->fld_type        = $data->fld_type;
  //                          $Bedspace->fld_is_bs_model = '1';
  //                          $Bedspace->fld_block_unblock = '1';
  //                          $Bedspace->fld_owner = $building->fld_tanent;

  //                          if($Bedspace->save())
  //                          {
  //                               $json_data = array('status'=>true,'message'=>"SuccessFull",'room_id'=>$Property->fld_id,'building_id' => $building->fld_id,'Bedspace_id' => $Bedspace->fld_id); 
  //                          } else {
  //                                $json_data = array('status'=>false,'message'=>"Error in saving Bedspace data"); 
  //                          }

                    


  //                  } else {
  //                        $json_data = array('status'=>false,'message'=>"Error in saving Building Ameneties"); 
  //                  }

  //           } else {

  //             $json_data = array('status'=>false,'message'=>"Error in saving Room Ameneties"); 
  //           }
  

  //           return response()->json($json_data);
               
  //           }



}