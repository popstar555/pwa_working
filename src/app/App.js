import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import '../app/style/css/style.css';

// Routs Page
import RoomReant from './pages/Routes/room-rent'
import Chat from './pages/Routes/chat'
import Booking from './pages/Routes/booking'
import Listings from './pages/Routes/listings'
import Analytics from './pages/Routes/analytics'
import Currency from './pages/Routes/currency'
import Nearby from './pages/Routes/nearby'

// pages
import Home from './pages/Home'
import MyProfile from './pages/MyProfile'
import ProfileSetting from './pages/ProfileSettings'
import Filters from './pages/Filters'
import PlaceAnAdd from './pages/PlaceAnAdd'
import Login from './pages/LoginInfo/Login'
import Signup from './pages/LoginInfo/Signup'

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path='/' exact component={Home}></Route>
          <Route path='/chat' component={Chat}></Route>
          <Route path='/booking' component={Booking}></Route>
          <Route path='/listings' component={Listings}></Route>
          <Route path='/analytics' component={Analytics}></Route>
          <Route path='/currency' component={Currency}></Route>
          <Route path='/nearby' component={Nearby}></Route>
          <Route path='/settings' component={ProfileSetting}></Route>
          <Route path='/my_profile' component={MyProfile}></Route>
          <Route path='/filters' component={Filters}></Route>
          <Route path='/place-an-add' component={PlaceAnAdd}></Route>
          <Route path='/room-rent' component={RoomReant}></Route>
          <Route path='/login' component={Login}></Route>
          <Route path='/signup' component={Signup}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
