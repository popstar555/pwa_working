import React from 'react'

export default function About() {
    return (
        <div className="container mt-4">
            <div className="card">
                <div className="card-body">
                    <h6 className="text-secondary">About us</h6>
                    <h4>Its time to fall in love with Creative design, Flexibility &amp; Uniqueness.</h4>
                    <h6 className="text-secondary">We are at our best</h6>
                    <p className="text-secondary mt-3">User experienced user interfaces with HTML and CSS also providing flexibility of style color customization. We have created specific website template demos and component library which be used across any demo.</p>
                    <a href="#" className="btn btn-sm btn-info">Read more</a>
                </div>
            </div>
        </div>
    )
}
