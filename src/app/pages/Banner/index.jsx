import React from 'react'

export default function Banner(props) {
    return (
        <div className="img-fluid">
            <img src={props.url} className="img-fluid" alt="Responsive image"></img>
        </div>
    )
}
