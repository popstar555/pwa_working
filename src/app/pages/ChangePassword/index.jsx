import React, { useState } from 'react'

export default function ChangePassword() {
    const [currentVal, setCurrentVal] = useState('')
    const [newPasswordVal, setNewPasswordVal] = useState('')
    const [confirmPasswordVal, setConfirmPasswordVal] = useState('')
    return (
        <>
            <h6 className="mt-3">Change Password</h6>
            <div className="card mt-3 mb-4">
                <div className="card-body">
                    <div className="row ">
                        <div className="col-12 col-md-6">
                            <div className={currentVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="password" 
                                    onChange={(e)=>{setCurrentVal(e.target.value)}} 
                                    className="form-control floating-input"
                                />
                                <label className="floating-label">Current Password</label>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className={newPasswordVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="password" 
                                    onChange={(e)=>{setNewPasswordVal(e.target.value)}} 
                                    className="form-control floating-input"
                                />
                                <label className="floating-label">New Password</label>
                            </div>
                        </div>
                        <div className="col-12 col-md-6">
                            <div className={confirmPasswordVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="password" 
                                    onChange={(e)=>{setConfirmPasswordVal(e.target.value)}} 
                                    className="form-control floating-input"
                                />
                                <label className="floating-label">Confirm New Password</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
