import React, { useState } from 'react'

export default function CounterFun(props) {
    const [count, setCount] = useState(0);

    // Create handleIncrement event handler
    const handleIncrement = () => {
        setCount(count + 1);
    };

    //Create handleDecrement event handler
    const handleDecrement = () => {
        if(count == 0){
            return;
        }else{
            setCount(count - 1);
        }
    };

    return (
        <>   
            <div className="counter-head mb-3">
                <div>
                    <p className="text-secondary small">{props.name}</p>
                </div>
                <div className="counter-body">
                    <button className="counter-btn" onClick={handleDecrement}>-</button>
                    <div className="count">{count}</div>
                    <button className="counter-btn" onClick={handleIncrement}>+</button>
                </div>
            </div>
        </>
    )
}
