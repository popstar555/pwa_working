import React from 'react'
import CounterFun from './CounterFun'

export default function Counter() {
    return (
        <>
            <div className="container">
                <h6 className="text-dark my-1">Room Capacity</h6>
                <CounterFun
                    name={'Number of Occupants'}
                />
            </div>
            <div className="container">
                <h6 className="text-dark my-1">Do you have any Flatmates?</h6>
                <CounterFun
                    name={'Male'}
                />
                <CounterFun
                    name={'Female'}
                />
            </div>
        </>
    )
}
