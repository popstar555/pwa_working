import slider1 from '../../img/image-4.jpg'
import slider2 from '../../img/image-2.jpg'



export const ListingsData = [
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.5 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.5 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.9 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.5 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.5 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
    {
        imgUrl: slider1,
        imgUrl2: slider2,
        rating: '4.5',
        country: 'Dubai',
        propertyType: 'Devine villa',
        propertySize: '3BHK House, 2400 sq. ft',
        propertyPrice: '$ 12.5 lacs ',
        perNight: 'night',
        perMonth: 'month'
    },
]

