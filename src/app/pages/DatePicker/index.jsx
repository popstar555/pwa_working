import "flatpickr/dist/themes/material_green.css";
 
import Flatpickr from "react-flatpickr";
import React, { useState } from "react";

export default function DatePickers() {
  const [date, setDate ] = useState(new Date())

  return (
    <>
      <Flatpickr 
        className="ml-3"
        value={date}
        onChange={date => {
          setDate({ date });
        }}
      />
    </>
  )
}
