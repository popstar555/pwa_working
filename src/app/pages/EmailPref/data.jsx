export const CheckboxData = [
    {
        id: 'customSwitch1',
        for: 'customSwitch1',
        text: 'Email Notification',
        subtext: 'Default all notification will be sent'
    },
    {
        id: 'customSwitch4',
        for: 'customSwitch4',
        text: 'SMS Notification',
        subtext: 'Receive SMS notification'
    },
    {
        id: 'customSwitch2',
        for: 'customSwitch2',
        text: 'Profile Avaialability',
        subtext: 'Everyone can see my profile in search'
    },
    {
        id: 'customSwitch3',
        for: 'customSwitch3',
        text: 'Sent Request',
        subtext: 'Everyone can sent me a request'
    },
]

