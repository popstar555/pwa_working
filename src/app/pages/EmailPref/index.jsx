import React from 'react'
import { CheckboxData } from './data'

export default function EmailPref() {
    return (
        <>
            <h6>Email Preferences</h6>
            <div className="card mt-3 mb-4">
                <div className="card-body px-0">
                    <ul className="list-group list-group-flush">
                        {CheckboxData.map((item, index)=>{
                            return(
                                <li className="list-group-item border-color">
                                    <div key={index} className="row">
                                        <div className="col-auto pr-0 align-self-center text-right">
                                            <div className="custom-control custom-switch">
                                                <input type="checkbox" className="custom-control-input" id={item.id} />
                                                <label className="custom-control-label" for={item.for}></label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <h6 className="text-dark mb-1">{item.text}</h6>
                                            <p className="text-secondary mb-0 small">{item.subtext}</p>
                                        </div>
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        </>
    )
}
