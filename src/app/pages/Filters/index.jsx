import React, { useState, useEffect } from 'react'
import { Accordion, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Carousel from 'react-bootstrap/Carousel'
import Avatar from 'react-avatar';
import SearchBar from '../Search'
import Spinner from '../../pages/Spinner/normal';

import default_img from '../../img/default.png'

export default function Filters(props) {

    const [min_search_price, setMinSearchPrice] = useState('')
    const [max_search_price, setMaxSearchPrice] = useState('')
    const [hr_balcony, setHrBalcony] = useState(false)
    const [hr_window, setHrWindow] = useState(false)
    const [hr_tv, setHrTV] = useState(false)
    const [hr_fridge, setHrFridge] = useState(false)
    const [natural_light, setNaturalLight] = useState(false)
    const [hr_wifi, setHrWifi] = useState(false)
    const [hr_parking, setHrParking] = useState(false)
    const [hr_washing, setHrWashing] = useState(false)
    const [hr_common_balcony, setHrCommonBalcony] = useState(false)
    const [dishwasher_amenity, setDishwasherAmenity] = useState(false)
    const [wheelchair_amenity, setWheelchairAmenity] = useState(false)
    const [garden_amenity, setGardenAmenity] = useState(false)
    const [terrace_amenity, setTerraceAmenity] = useState(false)
    const [living_room, setLivingRoom] = useState(false)
    const [hr_smoking, setHrSmoking] = useState(false)
    const [hr_elevator, setHrElevator] = useState(false)
    const [hr_pool, setHrPool] = useState(false)
    const [hr_couples, setHrCouples] = useState(false)
    const [hr_pets, setHrPets] = useState(false)
    const [hr_children, setHrChildren] = useState(false)
    const [room, setRoom] = useState(false)
    const [bedspace, setBedspace] = useState(false)
    const [verified, setVerified] = useState(false)
    const [gender, setGender] = useState("")
    const [sort_by, setSortBy] = useState("")

    const [listItems, setListItems] = useState([
        { 
            Area: '', 
            approved: 1,
            apt_no: "",
            avalability_date: "0000-00-00",
            bath: "",
            bedspace_expected_rent: 4200,
            bedspace_id: 1,
            bedtype: "",
            building: "",
            building_id: 1,
            currency: "AED",
            custom_room_name: "",
            fld_latitude: "25.0730814",
            fld_longitude: "55.13847110000006",
            fld_profile_pic: "",
            fld_tanent_id: 0,
            fld_type: "R",
            is_rented: 0,
            name: "",
            notice: 1,
            other_house_rules: "",
            rent: 4200,
            room_expected_rent: 4200,
            room_id: 1,
            room_images: [],
            room_mates_female: null,
            room_mates_male: null,
            room_name: "",
            room_notes: "",
        },
      ]);
    const [isFetching, setIsFetching] = useState(false);
    const [page, setPage] = useState(1);
    const [nextPage, setNextPage] = useState("");
    const [isLoaded, setIsLoad] = useState(false)
    const [isFilter, setIsFilter] = useState(false)
    const [location_id, setLocations] = useState(props.location.locations)
    const [building, setBuilding] = useState(props.location.building)
    const [non_building, setNonBuilding] = useState(props.location.non_building)

    const room_name = {
        display: "block",
        whiteSpace: "nowrap",
        width: "18em",
        overflow: "hidden",
        textOverflow: "ellipsis",
      };

    const Filter = () => {
        setIsFilter(true);
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                min_search_price: min_search_price,
                max_search_price: max_search_price,
                hr_balcony: hr_balcony,
                hr_window: hr_window,
                hr_tv: hr_tv,
                hr_fridge: hr_fridge,
                natural_light: natural_light,
                hr_wifi: hr_wifi,
                hr_parking: hr_parking,
                hr_washing: hr_washing,
                hr_common_balcony: hr_common_balcony,
                dishwasher_amenity: dishwasher_amenity,
                wheelchair_amenity: wheelchair_amenity,
                garden_amenity: garden_amenity,
                terrace_amenity: terrace_amenity,
                living_room: living_room,
                hr_smoking: hr_smoking,
                hr_elevator: hr_elevator,
                hr_pool: hr_pool,
                hr_couples: hr_couples,
                hr_pets: hr_pets,
                hr_children: hr_children,
                room: room,
                bedspace: bedspace,
                verified: verified,
                gender: gender,
                sort_by: sort_by,
                location_id: non_building,
                property_id: building,
                city_id: location_id
            })
        };
        fetch("https://roomdaddy.com/api/listing_filter", requestOptions)
          .then(response => response.json())
          .then(result => {
            setIsLoad(true)
            setNextPage(result.pagination.nextPageUrl)
            result.data.map((o) => {
                setListItems(prevState => ([...prevState, o]));
            })
            setPage(page + 1)
          })
          .catch(error => console.log('error', error));
    } 

    useEffect(() => {
        setLocations(props.location.locations)
        setBuilding(props.location.building)
        setNonBuilding(props.location.non_building)
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
      }, []);
    
    useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
    }, [isFetching]);
    
    function handleScroll() {
    if (window.innerHeight + document.documentElement.scrollTop !== document.documentElement.offsetHeight || isFetching) return;
    setIsFetching(true);
    }

    const handleMinSearchPrice = event => {
        setMinSearchPrice(event.target.value);
      };

    const handleMaxSearchPrice = event => {
        setMaxSearchPrice(event.target.value);
      };
    
    function fetchMoreListItems() {
    setTimeout(() => {
        fetch(nextPage)
            .then(response => response.json())
            .then(result => {
            setIsLoad(true)
            setNextPage(result.pagination.nextPageUrl)
            result.data.map((o) => {
                setListItems(prevState => ([...prevState, o]));
            })
            setPage(page + 1)
            })
            .catch(error => console.log('error', error));
        
            
        setIsFetching(false);
    }, 2000);
    }

    if(isFilter == false) {
        return (
            <>
                <main className="flex-shrink-0">
                    <header className="header active">
                        <div className="row">
                            <div className="col-auto px-0">
                                <Link to="/listings" className="btn menu-btn btn-link text-dark">
                                    <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                        <title>ionicons-v5-a</title>
                                        <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                        <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    </svg>
                                </Link>
                            </div>
                            <div className="text-left col align-self-center">
                                <h5>Filters</h5>
                            </div>
                            <div className="ml-auto col-auto align-self-center">

                            </div>
                        </div>
                    </header>
                </main>
                <div className='container filters-container mt-4'>
                    <Accordion defaultActiveKey="0">
                        <Card className="mb-3">
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                Budget
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <div className="form-group floating-form-group active">
                                        <label className="floating-label">Monthly Budget</label>
                                    </div>
                                    <div className="form-group floating-form-group pt-0">
                                        <div className="row">
                                            <div className="col">
                                                <input type="number" min="1" max="10000" value={min_search_price} id="min_search_price" onChange={handleMinSearchPrice} className="floating-input form-control" />
                                            </div>
                                            <div className="col-auto pt-2"> to </div>
                                            <div className="col">
                                                <input type="number" min="1" max="10000" value={max_search_price} id="max_search_price"  onChange={handleMaxSearchPrice} className="floating-input form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card className="mb-3">
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Amenities
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                <div className="form-group floating-form-group active">
                                    <label className="floating-label">Room Amenities</label>

                                    <div className="custom-control custom-switch mt-3 my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrBalcony(!hr_balcony)}} className="custom-control-input" id="private_balcony_amenity" />
                                        <label className="custom-control-label" htmlFor="private_balcony_amenity">Private Balcony</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrWindow(!hr_window)}} className="custom-control-input" id="window_amenity" />
                                        <label className="custom-control-label" htmlFor="window_amenity">Window</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrTV(!hr_tv)}} className="custom-control-input" id="tv_amenity" />
                                        <label className="custom-control-label" htmlFor="tv_amenity">Tv</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrFridge(!hr_fridge)}} className="custom-control-input" id="fridge_amenity" />
                                        <label className="custom-control-label" htmlFor="fridge_amenity">Fridge</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setNaturalLight(!natural_light)}} className="custom-control-input" id="natural_light" />
                                        <label className="custom-control-label" htmlFor="natural_light">Natural</label>
                                    </div>
                                </div>
                                <div className="form-group floating-form-group active">
                                    <label className="floating-label">Apartment Amenities</label>

                                    <div className="custom-control custom-switch mt-3 my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrWifi(!hr_wifi)}} className="custom-control-input" id="wifi_amenity" />
                                        <label className="custom-control-label" htmlFor="wifi_amenity">Wifi</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrParking(!hr_parking)}} className="custom-control-input" id="parking_amenity" />
                                        <label className="custom-control-label" htmlFor="parking_amenity">Parking</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrElevator(!hr_elevator)}} className="custom-control-input" id="elevator_amenity" />
                                        <label className="custom-control-label" htmlFor="elevator_amenity">Elevator</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrPool(!hr_pool)}} className="custom-control-input" id="pool_amenity" />
                                        <label className="custom-control-label" htmlFor="pool_amenity">Pool</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrWashing(!hr_washing)}} className="custom-control-input" id="washing_amenity" />
                                        <label className="custom-control-label" htmlFor="washing_amenity">Washing</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setHrCommonBalcony(!hr_common_balcony)}} className="custom-control-input" id="common_balcony_amenity" />
                                        <label className="custom-control-label" htmlFor="common_balcony_amenity">Common Balcony</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setDishwasherAmenity(!dishwasher_amenity)}} className="custom-control-input" id="dishwasher_amenity" />
                                        <label className="custom-control-label" htmlFor="dishwasher_amenity">Dishwasher</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setWheelchairAmenity(!wheelchair_amenity)}} className="custom-control-input" id="wheelchair_amenity" />
                                        <label className="custom-control-label" htmlFor="wheelchair_amenity">Wheelchair</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setGardenAmenity(!garden_amenity)}} className="custom-control-input" id="garden_amenity" />
                                        <label className="custom-control-label" htmlFor="garden_amenity">Garden</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setTerraceAmenity(!terrace_amenity)}} className="custom-control-input" id="terrace_amenity" />
                                        <label className="custom-control-label" htmlFor="terrace_amenity">Terrace</label>
                                    </div>
                                    <div className="custom-control custom-switch my-2">
                                        <input type="checkbox" onChange={(e)=>{setLivingRoom(!living_room)}} className="custom-control-input" id="living_room" />
                                        <label className="custom-control-label" htmlFor="living_room">Living Room</label>
                                    </div>
                                </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card className="mb-3">
                            <Accordion.Toggle as={Card.Header} eventKey="2">
                                House Rules
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body>
                                    <div className="form-group floating-form-group active">
                                        <label className="floating-label">House Rules</label>

                                        <div className="custom-control custom-switch mt-3 my-2">
                                            <input type="checkbox" onChange={(e)=>{setHrSmoking(!hr_smoking)}} className="custom-control-input" id="smoker_friendly" />
                                            <label className="custom-control-label" htmlFor="smoker_friendly">Smoker Friendly</label>
                                        </div>
                                        <div className="custom-control custom-switch my-2">
                                            <input type="checkbox" onChange={(e)=>{setHrCouples(!hr_couples)}} className="custom-control-input" id="couples_accepted" />
                                            <label className="custom-control-label" htmlFor="couples_accepted">Couples Accepted</label>
                                        </div>
                                        <div className="custom-control custom-switch my-2">
                                            <input type="checkbox" onChange={(e)=>{setHrPets(!hr_pets)}} className="custom-control-input" id="pets_allowed" />
                                            <label className="custom-control-label" htmlFor="pets_allowed">Pets Allowed</label>
                                        </div>
                                        <div className="custom-control custom-switch my-2">
                                            <input type="checkbox" onChange={(e)=>{setHrChildren(!hr_children)}} className="custom-control-input" id="children_allowed" />
                                            <label className="custom-control-label" htmlFor="children_allowed">Children Allowed</label>
                                        </div>
                                    </div>
                                    <div className="form-group floating-form-group active">
                                        <label className="floating-label">Other House Rules</label>

                                        <div className="custom-control custom-switch mt-3">
                                            <input type="radio" name="gender" onClick={() => setGender('M')} className="custom-control-input" value="M" id="strictly_male"/>
                                            <label className="custom-control-label" htmlFor="strictly_male">Strictly male</label>
                                        </div>
                                        <div className="custom-control custom-switch">
                                            <input type="radio" name="gender" onClick={() => setGender('F')} className="custom-control-input" value="F" id="strictly_female" />
                                            <label className="custom-control-label" htmlFor="strictly_female">Strictly female</label>
                                        </div>
                                        <div className="custom-control custom-switch">
                                            <input type="radio" name="gender" onClick={() => setGender('M/F')} className="custom-control-input" value="M/F" id="mix" />
                                            <label className="custom-control-label" htmlFor="mix">Mix</label>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card className="mb-3">
                            <Accordion.Toggle as={Card.Header} eventKey="3">
                                Listing Type
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="3">
                                <Card.Body>
                                    <div className="form-group floating-form-group active">
                                        <label className="floating-label"></label>

                                        <div className="custom-control custom-switch ">
                                            <input type="checkbox" onChange={(e)=>{setRoom(!room)}} className="custom-control-input" name="private_room" id="private_room" />
                                            <label className="custom-control-label" htmlFor="private_room">Private Room</label>
                                        </div>
                                        <div className="custom-control custom-switch my-2">
                                            <input type="checkbox" onChange={(e)=>{setBedspace(!bedspace)}} className="custom-control-input" id="shared_room" />
                                            <label className="custom-control-label" htmlFor="shared_room">Shared Room</label>
                                        </div>
                                        <div className="custom-control custom-switch my-2">
                                            <input type="checkbox" onChange={(e)=>{setVerified(!verified)}} className="custom-control-input" id="verified_listing" />
                                            <label className="custom-control-label" htmlFor="verified_listing">Verified Listing</label>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card className="mb-3">
                            <Accordion.Toggle as={Card.Header} eventKey="4">
                                Sort By
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="4">
                                <Card.Body>
                                    <div className="form-group floating-form-group active">
                                        <label className="floating-label">Other House Rules</label>

                                        <div className="custom-control custom-switch mt-3">
                                            <input type="radio" name="sort_by" onClick={() => setSortBy('Recent')} className="custom-control-input" value="Recent" id="radioRecent"/>
                                            <label className="custom-control-label" htmlFor="radioRecent">Recent</label>
                                        </div>
                                        <div className="custom-control custom-switch">
                                            <input type="radio" name="sort_by" onClick={() => setSortBy('priceLH')} className="custom-control-input" value="priceLH" id="radioLH" />
                                            <label className="custom-control-label" htmlFor="radioLH">Price: low to high</label>
                                        </div>
                                        <div className="custom-control custom-switch">
                                            <input type="radio" name="sort_by" onClick={() => setSortBy('priceHL')} className="custom-control-input" value="priceHL" id="radioHL" />
                                            <label className="custom-control-label" htmlFor="radioHL">Price: high to low </label>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Button onClick={()=>Filter()} className="btn btn-info btn-rounded btn-block">Search</Button>
                        <br/>
                    </Accordion>
                </div> 
            </>
        )
    }else{
        if(isLoaded == true) {
            return (
                <>
                    <div className="listing-m">
                        <div className="top">
                            <SearchBar/>
                            <div className="container mt-4 listings">
                                { listItems.length > 0 && listItems.map((item, index)=>{
                                    return(
                                        <>
                                            { index != 0 && (<div className="card product-card-large mb-3">
                                                <div className="card-header pl-2 mb-1">
                                                    <Avatar size="40" round={true} name={ item.name } src= { item.fld_profile_pic } className="mr-2" />
                                                    <span>{ item.name } </span>
                                                </div>
                                                <div className="card-body p-0">
                                                    <div className="product-image">
                                                        <Carousel controls={false} slide={false} interval={9000}>
                                                            {item.room_images.map((image, index) => (
                                                                <Carousel.Item className="" >
                                                                    <img
                                                                    className="d-block w-100 carousalImg"
                                                                    src= {image } 
                                                                    alt={ item.custom_room_name }
                                                                    /> 
                                                                </Carousel.Item>
                                                            ))}
                                                        </Carousel>
                                                        { item.room_images.length == 0 && (<img
                                                            className="d-block w-100 carousalImg"
                                                            src= {default_img } 
                                                            alt={item.custom_room_name}
                                                        /> ) }
                                                    </div>
                                                </div>
                                                <Link to="/someLink" key={index} className="card-footer">
                                                    <div className="">
                                                        <div className="">
                                                            <p className="small vm">
                                                                <span className=" text-secondary">{item.Area}</span>
                                                            </p>
                                                        </div>
                                                        <div className="">
                                                            <span className="text-dark" style={room_name} >{item.custom_room_name} </span>
                                                        </div>
                                                        
                                                    </div>
                                                    <div className="">
                                                        <div className="d-flex">
                                                            { item.bath && (<span className="small text-secondary">{ item.bath } &nbsp;&nbsp;| &nbsp; &nbsp;</span>)}
                                                            { item.bedtype && (<span className="small text-secondary">{ item.bedtype } &nbsp;&nbsp; </span>)}
                                                            { item.other_house_rules && (<span className="small text-secondary">|&nbsp; &nbsp;{ item.other_house_rules }</span>)}
                                                        </div>
                                                        <div className="">
                                                            { item.avalability_date != '0000-00-00' && (<p className="small text-secondary">
                                                                { item.avalability_date }
                                                            </p>)}
                                                        </div>
                                                        <div className="">
                                                            <p className="small text-secondary">
                                                                <b>{item.currency} &nbsp; { item.rent }</b>
                                                                <span className="small text-secondary"> / Month</span>
                                                            </p> 
                                                        </div>
                                                    </div>
                                                </Link>
                                            </div> )}
                                        </>
                                    )
                                })}
                            </div>   
                            { isFetching && 'Loading more items...' }     
                        </div>
                    </div>
                    
                </>
            )
        }else{
            return <Spinner />
        }
    }
}
