import React, { useState } from 'react'
import footer from "../Footer";
import bars from '../../img/icons/menu-outline.svg'
import userProfile from '../../img/user-1.jpg'
import promo from '../../img/new-user.png'
import SideBar from '../LeftSideBar/data'
import { Link } from 'react-router-dom'
import { SidebarData } from '../LeftSideBar/data'

export default function Header(props) {
    const [sidebar, setSidebar] = useState(false)
    const showSidebar = () => setSidebar(!sidebar)

    return (
        <>
            <header className="header">
                <div className="row">
                    <div className="col-auto px-0">
                        <Link to=''>
                            <button className="menu-btn btn btn-link-default" onClick={showSidebar} type="button">
                                <img src={bars} alt="" className="icon-size-24"/>
                            </button>
                        </Link>
                    </div>
                    <div className="text-left col">
                        <Link to='/' className="navbar-brand">
                            <img src={props.logourl}></img>
                        </Link>
                    </div>
                    <div className="ml-auto col-auto">
                        <Link to='/my_profile' className="icon icon-44 shadow-sm">
                            <figure className="m-0 background" style={{backgroundImage: `url(${userProfile})`,}}>
                                <img src={userProfile} alt="" style={{display: "none"}}/>
                            </figure>
                        </Link>
                    </div>
                </div>
            </header>
            <div className={sidebar ? 'main-menu active-m' : 'main-menu'}>
                <div className="menu-container">
                    <div className="icon icon-100 position-relative">
                        <figure className="background" style={{backgroundImage: `url(${userProfile})`,}}>
                            <img src={userProfile}  alt="" style={{display: 'none'}}/>
                        </figure>
                    </div>
                    {/* <div className="px-4 mt-4 side-new-user__wrap">
                        <div className="side-new-user display-flex" style={{backgroundImage: `url(${promo})`}}>
                            <div className="side-new-user__left flex1 height100 nc-align-center">
                                <div className="ps-4">
                                    <b>Up to 20% OFF</b>
                                    <p className="mt-1 nc-line-height-1">New User Benefits</p>
                                </div>
                            </div>
                            <div className="side-new-user__right height100 center-center">
                                <div className="side-new-user__right-button text-center text-capitalize"> Shop now </div>
                            </div>
                        </div>
                    </div> */}
                    <ul className="nav nav-pills flex-column ">
                        { SidebarData.map((item, index)=>{
                            return(
                                <li key={index} className={item.className}>
                                    <Link to={item.path} onClick={showSidebar} className="nav-link">
                                        <span>{item.title}</span>
                                        <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                            <title>ionicons-v5-a</title>
                                            <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                        </svg>
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                    <Link to='' className="text-danger my-3 d-block">Sign out</Link>
                    <button className="btn btn-danger sqaure-btn close text-white" onClick={showSidebar}>
                        <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                            <title>ionicons-v5-l</title>
                            <line x1='368' y1='368' x2='144' y2='144' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                            <line x1='368' y1='144' x2='144' y2='368' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                        </svg>
                    </button>
                </div>
            </div>

        </>
    );
};
