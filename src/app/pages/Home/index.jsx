import React from 'react'
import TopNav from '../TopNav';
import PropOption from '../PropOption';
import LayoutMode from '../HomeListings';
import Slider from '../Slider/index';
import Banner from '../Banner';
import '../../style/css/style.css';
import MobileNav from '../MobileNav';
import Signup from '../LoginInfo/Signup';
import Login from '../LoginInfo/Login';

export default function Home() {
    return (
      <>
        <MobileNav/>
        <TopNav/>
        <main className="">
        {/* <Signup></Signup> */}
        {/* <Login></Login> */}
          <Banner url={'https://stage.roomdaddy.ae/public/frontend/images/header/2.jpg'} />
          <PropOption/>
          <Slider/>
          <LayoutMode
            name={'Exoticasi Duplex'}
            images={'12'}
            rating={'4.5'}
            country={'Canada'}
            size={'3BHK, 2400 sq. ft.'}
          />
          <LayoutMode
            name={'Exoticasi Duplex'}
            images={'12'}
            rating={'4.5'}
            country={'Canada'}
            size={'3BHK, 2400 sq. ft.'}
          />
          <LayoutMode
            name={'Exoticasi Duplex'}
            images={'12'}
            rating={'4.5'}
            country={'Canada'}
            size={'3BHK, 2400 sq. ft.'}
          />
        </main>
      </>
    )
}
