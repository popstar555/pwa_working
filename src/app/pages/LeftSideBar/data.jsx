// import analytics from "../Routes/analytics";
// import language from "../Routes/language";
// import currency from "../Routes/currency";
// import nearby from "../Routes/nearby";
// import requestsent from "../Routes/requestsent";

export const SidebarData = [
    {
        title: 'Home',
        path: '/',
        className: 'nav-item',
    },
    {
        title: 'Analytics',
        path: '/analytics',
        className: 'nav-item',
    },
    {
        title: 'Language',
        path: '/language',
        className: 'nav-item',
    },
    {
        title: 'Currency',
        path: '/currency',
        className: 'nav-item',
    },
    {
        title: 'Near by',
        path: '/nearby',
        className: 'nav-item',
    },
    {
        title: 'Request Sent',
        path: '/requestsent',
        className: 'nav-item',
    },
    {
        title: 'Settings',
        path: '/settings',
        className: 'nav-item',
    },
    {
        title: 'Place an add',
        path: '/place-an-add',
        className: 'nav-item',
    },
    
]
