import React from 'react'
import userProfile from '../../img/user-1.jpg'

export default function SideBar() {
    function greetUser() {
         // do something here
        alert("clicked")
    }
    return (
        <div className="main-menu">
            <div className="menu-container">
                <div className="icon icon-100 position-relative">
                    <figure className="background" style={{backgroundImage: `url(${userProfile})`,}}>
                        <img src={userProfile}  alt="" />
                    </figure>
                </div>
                <ul className="nav nav-pills flex-column ">
                    <li className="nav-item">
                        <a className="nav-link active" href="index.html">Home
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="analytics.html">Analytics
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="">Language
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="">Currency
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="nearby.html">Near by
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="requested.html">Request Sent
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="newoffer.html">New Offers
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="bookings.html">My Bookings
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="setting.html">Settings
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="controls.html">Controls
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="colorscheme.html">Color Scheme
                            <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 arrow" viewBox='0 0 512 512'>
                                <title>ionicons-v5-a</title>
                                <polyline points='184 112 328 256 184 400' style={{fill:'none',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                            </svg>
                        </a>
                    </li>
                </ul>
                <a href="signin.html" className="text-danger my-3 d-block">Sign out</a>
                <button className="btn btn-danger sqaure-btn close text-white" onClick={greetUser}>
                    <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                        <title>ionicons-v5-l</title>
                        <line x1='368' y1='368' x2='144' y2='144' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                        <line x1='368' y1='144' x2='144' y2='368' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                    </svg>
                </button>
            </div>
        </div>
    )
}
