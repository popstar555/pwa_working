import React, { useState, useEffect } from 'react';
import SpinnerLoading from '../../pages/Spinner/normal';
import Carousel from 'react-bootstrap/Carousel'
import { Button, Spinner } from 'react-bootstrap';
import SearchBar from '../Search'
import Avatar from 'react-avatar';
import { Link } from 'react-router-dom'

import default_img from '../../img/default.png'

const List = (props) => {
  const [listItems, setListItems] = useState([
    { 
        Area: '', 
        approved: 1,
        apt_no: "",
        avalability_date: "0000-00-00",
        bath: "",
        bedspace_expected_rent: 0,
        bedspace_id: 1,
        bedtype: "",
        building: "",
        building_id: 1,
        currency: "AED",
        custom_room_name: "",
        fld_latitude: "25.0730814",
        fld_longitude: "55.13847110000006",
        fld_profile_pic: "",
        fld_tanent_id: 0,
        fld_type: "R",
        is_rented: 0,
        name: "",
        notice: 1,
        other_house_rules: "",
        rent: 4200,
        room_expected_rent: 0,
        room_id: 0,
        room_images: [],
        room_mates_female: null,
        room_mates_male: null,
        room_name: "",
        room_notes: "",
    },
  ]);

  const [isFetching, setIsFetching] = useState(false);
  const [page, setPage] = useState(1);
  const [isLoaded, setIsLoad] = useState(false)
  const [loading, setLoading] = useState(false)

  const room_name = {
    display: "block",
    whiteSpace: "nowrap",
    width: "18em",
    overflow: "hidden",
    textOverflow: "ellipsis",
  };

  const load_more_btn = {
    marginBottom: "1.3rem",
    width: "130px",
    fontSize: "10px",
    backgroundColor: "#275284"
  };

  useEffect(() => {
    if(!isLoaded) {
        fetchMoreListItems()
    }
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
  }, [isFetching]);

  function loadMore() {
    setIsFetching(true)
    setLoading(true)
  }

  function fetchMoreListItems() {
    setTimeout(() => {
        fetch("https://roomdaddy.com/api/get-property-list?page="+page)
          .then(response => response.json())
          .then(result => {
            setIsLoad(true)
            result.data.map((o) => {
                setListItems(prevState => ([...prevState, o]));
                let o_map = {};
                o_map.latitude = o.fld_latitude;
                o_map.longitude = o.fld_longitude;
                o_map.custom_room_name = o.custom_room_name;
                o_map.bath = o.bath;
                o_map.rent = o.rent;
                o_map.room_images = o.room_images;
                o_map.bedtype = o.bedtype;
                o_map.currency = o.currency;
                o_map.building_id = o.building_id;
                props.setMapItems(prevMaps => ([...prevMaps, o_map]));
                setLoading(false)
            })
            setPage(page + 1)
          })
          .catch(error => console.log('error', error));  
      setIsFetching(false);
    }, 2000);
  }

  if(isLoaded == true) {
    return (
        <>
            <div className="listing-m">
                <div className="top">
                    <SearchBar/>
                    <div className="container mt-4 listings">
                        { listItems.length > 0 && listItems.map((item, index)=>{
                            return(
                                <>
                                    { index != 0 && (
                                        <div className="card product-card-large mb-3 container" key={index}>
                                            <div className="card-header pl-2 mb-1">
                                                <Avatar size="40" round={true} name={ item.name } src= { item.fld_profile_pic } className="mr-2" />
                                                <span>{ item.name } </span>
                                            </div>
                                            <div className="card-body p-0">
                                                <div className="product-image">
                                                    <Carousel controls={false} slide={false} interval={9000}>
                                                        {item.room_images.map((image, index) => (
                                                            <Carousel.Item className="" >
                                                                <img
                                                                className="d-block w-100 carousalImg"
                                                                src= {image } 
                                                                key = {index}
                                                                alt={ item.custom_room_name }
                                                                /> 
                                                            </Carousel.Item>
                                                        ))}
                                                    </Carousel>
                                                    { item.room_images.length == 0 && (<img
                                                        className="d-block w-100 carousalImg"
                                                        src= {default_img } 
                                                        alt={item.custom_room_name}
                                                    /> ) }
                                                </div>
                                            </div>
                                            <Link to="/someLink" key={index} className="card-footer">
                                                <div className="">
                                                    <div className="">
                                                        <p className="small vm">
                                                            <span className=" text-secondary">{item.Area}</span>
                                                        </p>
                                                    </div>
                                                    <div className="">
                                                        <span className="text-dark" style={room_name} >{item.custom_room_name} </span>
                                                    </div>
                                                    
                                                </div>
                                                <div className="">
                                                    <div className="d-flex">
                                                        { item.bath && (<span className="small text-secondary">{ item.bath } &nbsp;&nbsp;| &nbsp; &nbsp;</span>)}
                                                        { item.bedtype && (<span className="small text-secondary">{ item.bedtype } &nbsp;&nbsp; </span>)}
                                                        { item.other_house_rules && (<span className="small text-secondary">|&nbsp; &nbsp;{ item.other_house_rules }</span>)}
                                                    </div>
                                                    <div className="">
                                                        { item.avalability_date != '0000-00-00' && (<p className="small text-secondary">
                                                            { item.avalability_date }
                                                        </p>)}
                                                    </div>
                                                    <div className="">
                                                        <p className="small text-secondary">
                                                            <b>{item.currency} &nbsp; { item.rent }</b>
                                                            <span className="small text-secondary"> / Month</span>
                                                        </p> 
                                                    </div>
                                                </div>
                                            </Link>
                                        </div> 
                                    )}
                                </>
                            )
                        })}
                        <div className="d-flex justify-content-center">
                            <Button onClick={loadMore} style={load_more_btn} className="btn btn-default" disabled={loading}>
                                {loading && (
                                    <Spinner
                                    as="span"
                                    animation="grow"
                                    size="sm"
                                    role="status"
                                    aria-hidden="true"
                                    />
                                )}
                                {loading && <span className="ml-2">Loading...</span>}
                                {!loading && <span>Load more...</span>}
                            </Button>
                        </div>
                    </div>   
                </div>
            </div>
            
        </>
    )
} else {
    return <SpinnerLoading />
}
};

export default List;