import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Button} from 'react-bootstrap';
import LoginIcon from '../../img/icon.svg'
import Google from '../../img/google.svg'
import Facebook from '../../img/facebook.svg'
import $ from 'jquery';
import { NotificationManager } from 'react-notifications';

export default function Login() {

    const [passwordVal, setPasswordVal] = useState('')
    const [emailVal, setEmailVal] = useState('')
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [options, setOptions] = useState({
        autoClose: false,
        keepAfterRouteChange: false
    });

    const Login = () => {
        let password = passwordVal
        let email = emailVal
        let login_api = 'https://roomdaddy.com/api/login/' + email + '/' + password
        $.ajax({
            url: login_api,
            type: 'post',
            data: {
                'email': email,
                'password': password
            },
            dataType: 'JSON',
            success: function(response){
                let status = response.status;
                if(status == true){
                    let userData = {
                        id: response.data.id,
                        name: response.data.fld_name,
                        email: response.data.email,
                        number: response.data.fld_number,
                        role: response.data.fld_user_type,
                        photo: response.data.fld_profile_pic,
                        device_id: response.data.device_id,
                        verification_code: response.data.verification_code,
                        remember_token: response.data.remember_token,
                    };
                    let appState = {
                        isLoggedIn: true,
                        user: userData
                    };
                    setIsLoggedIn(true)
                    localStorage["appState"] = JSON.stringify(appState);
                    NotificationManager.success('You have added a new book!', 'Successful!', 2000);
                }else{
                    console.log(5)
                    NotificationManager.error('Error while Creating new book!', 'Error!');
                }

            }
        });
    } 

    if(isLoggedIn == true) {
        return <Redirect to='/'/>
    } else {
        return (
            <body className="d-flex flex-column h-100">
                <header className="header">
                    <div className="row">
                        <div className="col-auto px-0">
                            <Link to="/coming_soon" className="btn menu-btn btn-link text-dark">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-a</title>
                                    <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                </svg>
                            </Link>
                        </div>
                        <div className="text-left col">
    
                        </div>
                        <div className="ml-auto col-auto px-0">
    
                        </div>
                    </div>
                </header>
                <main className="flex-shrink-0">
                    <div className="container text-center  mt-4">
                        <div className="icon icon-100 text-white mb-4 text-center">
                            <img src={LoginIcon}></img>
                            {/* <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-48" viewBox='0 0 512 512'>
                                <title>ionicons-v5-i</title>
                                <path d='M80,212V448a16,16,0,0,0,16,16h96V328a24,24,0,0,1,24-24h80a24,24,0,0,1,24,24V464h96a16,16,0,0,0,16-16V212' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                <path d='M480,256,266.89,52c-5-5.28-16.69-5.34-21.78,0L32,256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                <polyline points='400 179 400 64 352 64 352 133' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                            </svg> */}
                        </div>
                        <h4 className="mb-4">Welcome To Roomdaddy</h4>
                    </div>
                    <div className="container text-center  mt-4">
                        <h4 className="text-secondary small mb-4">Signin with social media</h4>
                    </div>
                    <div className="container mb-3">
                        <div className="row">
                            <div className="col text-center">
                                <Link to="#">
                                    <img style={{height: '25px',width: '27px'}} src={Facebook}></img>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className="container mb-3">
                        <div className="row">
                            <div className="col text-center">
                                <Link to="#">
                                    <img style={{height: '25px',width: '27px'}} src={Google}></img>
                                </Link>
                            </div>
                        </div>
                    </div>
                    
                    <div className="container">
                        <div className="login-box">
                            <div className={emailVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="email"
                                    onChange={(e)=>{setEmailVal(e.target.value)}}
                                    className="form-control floating-input" 
                                    autofocus=""
                                    // value="amayjohnson@maxartkiller.coms "
                                />
                                <label className="floating-label">Email Address</label>
                            </div>
                            <div className={passwordVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="password"
                                    onChange={(e)=>{setPasswordVal(e.target.value)}} 
                                    className="form-control floating-input"
                                  
                                />
                                <label className="floating-label">Password</label>
                            </div>
                            <div className="form-group my-4">
                                <a href="" className="link">Forget password?</a>
                            </div>
                            <Button onClick={()=>Login()} className="btn btn-block btn-info btn-lg">Sign In</Button>
                        </div>
                    </div>
                </main>
                <footer className="footer mt-auto">
                    <div className="container">
                        <div className="row">
                            <div className="col text-center">
                                <Link to="/signup" className="link">Create Account</Link>
                            </div>
                        </div>
                    </div>
                </footer>
            </body>
            
        )
    }

    
}
