import React, { useState } from 'react'
import Select from 'react-select';
import { stateOptions, groupedOptions } from '../MultiSelect/multiSelectData';
import { Link, Redirect } from 'react-router-dom'
import { Button} from 'react-bootstrap';
import LoginIcon from '../../img/icon.svg'

import $ from 'jquery';
import toastr from 'toastr';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Spinner from '../../pages/Spinner/normal';


// import multiSelect from '../MultiSelect'
// import DatePicker from '../DatePicker'

export default function Signup() {
    const [namelVal, setNameVal] = useState('')
    const [emailVal, setEmailVal] = useState('')
    const [passwordVal, setPasswordVal] = useState('')
    const [cellPhoneVal, setCellPhoneVal] = useState('')
    const [whatsappVal, setWhatsappVal] = useState('')
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [isLoading, setIsLoading] = useState(false)

    const Signup = () => {
        let name = namelVal
        let email = emailVal
        let password = passwordVal
        let cellphone = cellPhoneVal
        let whatsapp = whatsappVal
        setIsLoading(true)
        $.ajax({
            url: "https://roomdaddy.com/api/register",
            type: 'post',
            data: {
                'name': name,
                'password': password,
                'email': email,
                // 'cellphone': cellphone,
                // 'whatsapp': whatsapp
            },
            dataType: 'JSON',
            success: function(response){
                setIsLoading(false)
                let status = response.status;
                if(status == true){
                    // const notify = () => toast("Wow so easy !");
                    // toastr.success('Successfully logged in!')
                    //this.setState({isLoggedIn: true})
                    let userData = {
                        id: response.data.id,
                        name: response.data.fld_name,
                        email: response.data.email,
                        // number: response.data.fld_number,
                        role: response.data.fld_user_type,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                        // photo: response.data.fld_profile_pic,
                        // device_id: response.data.device_id,
                        // verification_code: response.data.verification_code,
                        // remember_token: response.data.remember_token,
                    };
                    let appState = {
                        isLoggedIn: true,
                        user: userData
                    };
                    setIsLoggedIn(true)
                    localStorage["appState"] = JSON.stringify(appState)
                    

                }else{
                    toastr.success(response.message)
                }

            }
        });
    } 

    if(isLoading == true) {
        return <Spinner />
    }

    if(isLoggedIn == true) {
        return <Redirect to='/'/>
    } else {
        return (
            <body className="d-flex flex-column h-100">
                <header className="header">
                    <div className="row">
                        <div className="col-auto px-0">
                            <Link to="/coming_soon" className="btn menu-btn btn-link text-dark">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-a</title>
                                    <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                </svg>
                            </Link>
                        </div>
                        <div className="text-left col">

                        </div>
                        <div className="ml-auto col-auto px-0">

                        </div>
                    </div>
                </header>
                <main ClassName="flex-shrink-0">
                    <div className="container text-center mt-4">
                        <div className="icon icon-100 text-white mb-4 text-center">
                            <img src={LoginIcon}></img>
                            {/* <svg xmlns="http://www.w3.org/2000/svg" className="icon-size-48" viewBox="0 0 512 512">
                                <title>ionicons-v5-i</title>
                                <path d="M80,212V448a16,16,0,0,0,16,16h96V328a24,24,0,0,1,24-24h80a24,24,0,0,1,24,24V464h96a16,16,0,0,0,16-16V212" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></path>
                                <path d="M480,256,266.89,52c-5-5.28-16.69-5.34-21.78,0L32,256" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></path>
                                <polyline points="400 179 400 64 352 64 352 133" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></polyline>
                            </svg> */}
                        </div>
                        <h4 className="mb-4">Roomdaddy</h4>
                    </div>
                    <div className="container">
                        <div className="login-box">
                            <div className={namelVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="text" 
                                    onChange={(e)=>{setNameVal(e.target.value)}} 
                                    className="form-control floating-input" 
                                    // value="Amay Johnson"
                                    autofocus=""
                                />
                                <label className="floating-label">Name</label>
                            </div>
                            <div className={emailVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="email" 
                                    onChange={(e)=>{setEmailVal(e.target.value)}} 
                                    className="form-control floating-input" 
                                    // value="amayjohnson@maxartkiller.co" 
                                />
                                <label className="floating-label">Email Address</label>
                            </div>
                            <div className={passwordVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="password" 
                                    onChange={(e)=>{setPasswordVal(e.target.value)}} 
                                    className="form-control floating-input" 
                                />
                                <label className="floating-label">Password</label>
                            </div>
                            <div className={cellPhoneVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="tel" id="cell" 
                                    onChange={(e)=>{setCellPhoneVal(e.target.value)}} 
                                    className="form-control floating-input" 
                                    name="phone"  
                                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" 
                                    required  
                                />
                                <label className="floating-label">Cell Number</label>
                            </div>
                            <div className={whatsappVal !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                                <input type="tel" id="whatsapp" 
                                    onChange={(e)=>{setWhatsappVal(e.target.value)}} 
                                    className="form-control floating-input" 
                                    name="whatsapp"  
                                    pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" 
                                    required
                                />
                                <label className="floating-label">Whatsapp Number</label>
                            </div>
                            {/* <div className="form-group floating-form-group">
                                <DatePicker/>
                                <label className="floating-label">Date of birth</label>
                            </div> */}
                            <div className="form-group floating-form-group">
                                <label className="">Nationality</label>
                                <Select
                                    defaultValue={stateOptions[1]}
                                    options={groupedOptions}
                                />
                            </div>
                            <div className="form-group floating-form-group">
                                <label className="">Languages you speak</label>
                                <Select
                                    
                                />
                            </div>
                            <div className="form-group floating-form-group">
                                <label className="">Gender</label>
                                <div className="mr-3"></div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="optradio"/>Male
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="optradio"/>Female
                                    </label>
                                </div>
                            </div>
                            
                            
                            <div className="form-group my-4 text-secondary">
                                By Clicking button below you are agree to
                                the <Link to='#' className="link">Terms and Condition</Link> of the app
                            </div>
                            <Button onClick={()=>Signup()} className="btn btn-block btn-danger btn-lg">Sign Up</Button>
                        </div>
                    </div>
                </main>
                <footer className="footer mt-auto">
                    <div className="container">
                        <div className="row">
                            <div className="col text-center">
                                <Link to='/login' className="link">Already have account? Login here!</Link>
                            </div>
                        </div>
                    </div>
                </footer>
            </body>
        )
    }
}
