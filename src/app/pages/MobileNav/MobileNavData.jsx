import home from '../../img/home.png'
import chat from '../../img/chat.png'
import listing from '../../img/list.svg'
import booking from '../../img/heart.svg'
export const MobileNavData = [
    {
        path: '/',
        img: home,
        // img: 'https://static.thenounproject.com/png/423483-200.png',
        title: 'Home',
    },
    {
        path: '/chat',
        img: chat,
        // img: 'https://static.thenounproject.com/png/148042-200.png',
        title: 'Chat',
    },
    {
        path: '/booking',
        img: booking,
        // img: 'https://cdn.iconscout.com/icon/premium/png-512-thumb/reserved-2-619005.png',
        title: 'Booking',
    },
    {
        path: '/listings',
        img: listing,
        // img: 'https://cdn.iconscout.com/icon/premium/png-256-thumb/circle-list-1471024-1245124.png',
        title: 'Listing',
    },
]