import React from 'react'
import { Link } from 'react-router-dom'
import { MobileNavData } from './MobileNavData'


export default function MobileNav() {
    return (
        <div className="bottom-navigation-wrap display-flex max-width750">
            {MobileNavData.map((item, index)=>{
                return(
                    <Link to={item.path} key={index} className="bottom-navigation center-center">
                        <img style={{height: '25px',width: '27px'}} src={item.img} alt="" srcSet=""/>
                        <p>{item.title}</p>
                    </Link>
                )
            })}
            
            {/* <Link to='/chat' className="bottom-navigation center-center">
                <img style={{height: '25px',width: '25px'}} src="https://static.thenounproject.com/png/148042-200.png" alt="" srcset=""/>
                <p>Chat</p>
            </Link>
            <Link to='/booking' className="bottom-navigation center-center">
                <img style={{height: '25px',width: '25px'}} src="https://cdn.iconscout.com/icon/premium/png-512-thumb/reserved-2-619005.png" alt="" srcset=""/>
                <p>Booking</p>
            </Link>
            <Link to='/listings' className="bottom-navigation center-center">
                <img style={{height: '25px',width: '25px'}} src='https://cdn.iconscout.com/icon/premium/png-256-thumb/circle-list-1471024-1245124.png' alt="" srcset=""/>
                <p>Listing</p>
            </Link> */}
        </div>
       
    )
}
