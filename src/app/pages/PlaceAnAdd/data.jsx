import home from '../../img/entire.jpg'
import staycation from '../../img/mae-unsplash.jpg'
import parking from '../../img/find-a-parking.jpg'

export const PlaceAnAddData = [
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Entire Place',
        titleClass: 'p',
        img: staycation,
        path: '/www.something2.com',
    },
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Co-Living',
        titleClass: 'p',
        img: staycation,
        path: '/room-rent',
    },
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Parking',
        titleClass: 'p',
        img: staycation,
        path: '/www.something4.com',
    },
]