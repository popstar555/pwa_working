import React from 'react'
import { Link } from 'react-router-dom'
import { PlaceAnAddData } from '../PlaceAnAdd/data'
import MobileNav from '../MobileNav'
import TopNav from '../TopNav'

export default function PlaceAnAdd() {
    return (
        <>
            <TopNav/>
            <div className="container mt">
                <div className='card'>
                    <div className="card-header">
                        <div className="row">
                            <div className="col">
                                <h6 className="text-dark my-1">
                                    <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 vm" viewBox='0 0 512 512'>
                                        <title>ionicons-v5-i</title>
                                        <path d='M80,212V448a16,16,0,0,0,16,16h96V328a24,24,0,0,1,24-24h80a24,24,0,0,1,24,24V464h96a16,16,0,0,0,16-16V212' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                        <path d='M480,256,266.89,52c-5-5.28-16.69-5.34-21.78,0L32,256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                        <polyline points='400 179 400 64 352 64 352 133' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                    </svg>
                                    <span className="vm ml-2">What are you listing?</span>
                                </h6>
                                <p class="text-secondary small">Select which category your ad fits into</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="row px-2">
                            {/* <div className="col-6 col-sm-6 col-md-4 col-lg-4 px-2"> */}
                                { PlaceAnAddData.map((item, index)=>{
                                    return(
                                        <div key={index} className="col-6 col-sm-6 col-md-4 col-lg-4 px-2">
                                            <Link to={item.path} >
                                                <label className={item.lableClass}>
                                                    <span className={item.spanClass}>
                                                        <span className={item.spanImg} style={{backgroundImage: `url(${item.img})`,}}>
                                                            <img src={item.img} alt='' style={{display: 'none',}}></img>
                                                        </span>
                                                    </span>
                                                    <span className={item.titleClass}>{item.title}</span>
                                                </label>
                                            </Link>
                                        </div>
                                    )
                                })}
                            {/* </div> */}
                        </div>
                    </div>
                </div>
            </div>
            <MobileNav/>
        </>
    )
}
