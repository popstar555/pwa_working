import React from 'react'
import { Link } from 'react-router-dom'
import userProfile from '../../img/user-1.jpg'
import ChangePassword from '../ChangePassword'
import EmailPref from '../EmailPref'

export default function ProfileSetting() {
    return (
        <>
            <main className="flex-shrink-0" style={{marginBottom: '5.5rem'}}>
                <header className="header active">
                    <div className="row">
                        <div className="col-auto px-0">
                            <Link to="/" className="btn menu-btn btn-link text-dark">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-a</title>
                                    <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                </svg>
                            </Link>
                        </div>
                        <div className="text-left col align-self-center">
                            <h5>Settings</h5>
                        </div>
                        <div className="ml-auto col-auto align-self-center">

                        </div>
                    </div>
                </header>


                <div className="container mt-4 text-center">
                    <div className="icon icon-100 position-relative">
                        <figure className="background" style={{backgroundImage: `url(${userProfile})`,}}>
                            <img src={userProfile}  alt="" style={{display: 'none'}}/>
                        </figure>
                    </div>
                </div>
                <div className="container mt-4">
                    <div className="card mt-3 mb-4">
                        <div className="card-body">
                            <div className="row mt-">
                                <div className="col-12 col-md-6">
                                    <div className="form-group floating-form-group active">
                                        <input type="text" className="form-control floating-input" value="Amay Johnson"/>
                                        <label className="floating-label">Name</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group floating-form-group active">
                                        <input type="email" className="form-control floating-input" value="amayjohnson@maxartkiller.coms "/>
                                        <label className="floating-label">Email Address</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group floating-form-group active">
                                        <div className="row">
                                            <div className="col-2 col-md-1 active">
                                                <input type="text" className="form-control floating-input" value="44"/>
                                            </div>
                                            <div className="col active">
                                                <input type="text" className="form-control floating-input" value="000 000 0000"/>
                                            </div>
                                        </div>
                                        <label className="floating-label">Phone Number</label>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group floating-form-group active mb-0">
                                        <input type="" className="form-control floating-input" value="Amay Johnson"/>
                                        <label className="floating-label">Change photo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <EmailPref/>
                    <ChangePassword/>
                    
                </div>
            </main>
        </>
    )
}
