import React, { useState } from 'react'
import { Link } from 'react-router-dom'
// import { Modal } from "react-bootstrap";
// import "../../../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import { PropertyData } from './propData'
import room from '../../img/find-a-room.jpg'

import Modal from 'react-modal';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

const useStyles = makeStyles((theme) => ({
    appBar: {
      position: 'relative',
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    button: {
      width: '100%' ,
      margin: '15px 0px' ,
    },
  }));

export default function PropOption() {
    const [show, setShow] = useState(false);

    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className="container mt-4">
            <div className="card">
                <div className="card-header">
                    <div className="row">
                        <div className="col">
                            <h6 className="text-dark my-1">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-16 vm" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-i</title>
                                    <path d='M80,212V448a16,16,0,0,0,16,16h96V328a24,24,0,0,1,24-24h80a24,24,0,0,1,24,24V464h96a16,16,0,0,0,16-16V212' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                    <path d='M480,256,266.89,52c-5-5.28-16.69-5.34-21.78,0L32,256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                    <polyline points='400 179 400 64 352 64 352 133' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}} />
                                </svg>
                                <span className="vm ml-2">Property Type</span>
                            </h6>
                        </div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="row px-2">
                        <div className="col-6 col-sm-6 col-md-4 col-lg-4 px-2">
                            <a onClick={handleClickOpen} >
                                <label className='checkbox-lable'>
                                    <span className='image-boxed'>
                                        <span className='h-180 background' style={{backgroundImage: `url(${room})`,}}>
                                            <img src={room} alt='' style={{display: 'none',}}></img>
                                        </span>
                                    </span>
                                    <span className='p'>Room</span>
                                </label>
                            </a>
                            <Dialog fullWidth={true} maxWidth="md" open={open} onClose={handleClose}>
                                <AppBar className={classes.appBar}>
                                <Toolbar>
                                    <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                                    <CloseIcon />
                                    </IconButton>
                                    <Typography variant="h6" className={classes.title}>
                                    Select Types
                                    </Typography>
                                </Toolbar>
                                </AppBar>
                                <div className="col-12 mt-2">
                                    <Link to="/listings">
                                        <Button variant="contained" color="secondary" className={classes.button}>
                                            Hotel
                                        </Button>
                                    </Link>
                                    <Divider />
                                    <Link to="/listings">
                                        <Button variant="contained" color="secondary" className={classes.button}>
                                            Co-Living
                                        </Button>
                                    </Link>
                                </div>
                            </Dialog>
                        </div>
                        { PropertyData.map((item, index)=>{
                            return(
                                <div key={index} className="col-6 col-sm-6 col-md-4 col-lg-4 px-2">
                                    <Link to={item.path} >
                                        <label className={item.lableClass}>
                                            <span className={item.spanClass}>
                                                <span className={item.spanImg} style={{backgroundImage: `url(${item.img})`,}}>
                                                    <img src={item.img} alt='' style={{display: 'none',}}></img>
                                                </span>
                                            </span>
                                            <span className={item.titleClass}>{item.title}</span>
                                        </label>
                                    </Link>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}
