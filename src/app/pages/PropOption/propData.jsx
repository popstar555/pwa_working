
import '../../style/css/style.css';
import home from '../../img/find-an-entire-apartment.jpg'
import room from '../../img/find-a-room.jpg'
import staycation from '../../img/mae-unsplash.jpg'
import parking from '../../img/find-a-parking.jpg'

export const PropertyData = [
    // {   
    //     lableClass: 'checkbox-lable',
    //     spanClass: 'image-boxed',
    //     spanImg: 'h-180 background',
    //     title: 'Room',
    //     titleClass: 'p',
    //     img: room,
    //     path: '/www.something.com',
    // },
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Home',
        titleClass: 'p',
        img: home,
        path: '/www.something2.com',
    },
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Staycation',
        titleClass: 'p',
        img: staycation,
        path: '/www.something3.com',
    },
    {   
        lableClass: 'checkbox-lable',
        spanClass: 'image-boxed',
        spanImg: 'h-180 background',
        title: 'Parking',
        titleClass: 'p',
        img: parking,
        path: '/www.something4.com',
    },
]