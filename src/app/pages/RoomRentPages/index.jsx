import React from 'react'
import StepOne from "../RoomRentPages/stepOne";
import StepTwo from "../RoomRentPages/stepTwo";
import StepThree from "../RoomRentPages/stepThree";
import StepFour from "../RoomRentPages/stepFour";
import Counter from "../Counter";

export default function PlaceAdd() {
    // let componentShow
    // if(){
    //     componentShow = 
    // }
    const handleClick = (e)=>{
        alert("clicked")
    }
    return (
        <>
            <StepOne/>
            <Counter/>
            <StepTwo/>
            <StepThree/>
            <StepFour/>
            <footer className="footer mt-auto py-3">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <button type='button' className="btn btn-block btn-info btn-lg">Back</button>
                        </div>
                        <div className="col">
                            <button type='button' onClick={handleClick}  className="btn btn-block btn-info btn-lg">Next</button>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}
