import React, { useState } from 'react'

export default function StepFour() {
    const [title, setTitle] = useState('')
    const [tellUs, setTellUs] = useState('')
    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <form >
                            <h6 className="text-dark my-1">Room Photos</h6>
                            <input type="file" />
                            {/* <button type="submit">Upload</button> */}
                        </form>
                        <form >
                            <h6 className="text-dark my-1">Building Photos</h6>
                            <input type="file" />
                            {/* <button type="submit">Upload</button> */}
                        </form>
                        <div className="form-group floating-form-group active">
                            <h6 className="text-dark my-1">Choose Display Picture</h6>
                            <div className="custom-control custom-switch mt-3">
                                <input type="radio" name="display_picture" className="custom-control-input" id="room_picture"/>
                                <label className="custom-control-label" for="room_picture">Room pictures</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="display_picture" className="custom-control-input" id="common_picture" />
                                <label className="custom-control-label" for="common_picture">Common pictures</label>
                            </div>
                        </div>

                        <div className={title !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                            <input type="password"
                                onChange={(e)=>{setTitle(e.target.value)}} 
                                className="form-control floating-input"
                              
                            />
                            <label className="floating-label">Give your listing a title (Max 10 words) </label>
                        </div>
                        <div className={tellUs !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                            <input type="password"
                                onChange={(e)=>{setTellUs(e.target.value)}} 
                                className="form-control floating-input"
                              
                            />
                            <label className="floating-label">Tell us more... (Max 50 words) </label>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
