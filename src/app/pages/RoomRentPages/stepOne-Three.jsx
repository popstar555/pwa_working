import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Select from 'react-select';
import { stateOptions, groupedOptions, colourOptions } from '../MultiSelect/multiSelectData';
import StepOneTwo from "./stepOne-Two";

export default function StepOneThree() {
    const [property, setProperty] = useState('')

    let componentShow
    if(property == 'apartment'){
        componentShow = <StepOneTwo/>
    } else {
        componentShow = ''
    }
    return (
        <>
            <div className="form-group floating-form-group">
                <label className="">Choose Property type </label>
                <div className="mr-3"></div>
                {/* <div className="form-group floating-form-group active"> */}
                    <div className="custom-control custom-switch mt-3">
                        <input type="radio" name="propType" className="custom-control-input" id="villa"
                            onChange={(e)=>{
                                const selected = e.currentTarget.id 
                                setProperty(selected)
                            }}
                        />
                        <label className="custom-control-label" for="villa">Villa</label>
                    </div>
                    <div className="custom-control custom-switch">
                        <input type="radio" name="propType" className="custom-control-input" id="apartment" 
                            onChange={(e)=>{
                                const selected = e.currentTarget.id 
                                setProperty(selected)
                            }}
                        />
                        <label className="custom-control-label" for="apartment">Apartment</label>
                    </div>
                {/* </div> */}
                {/* <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="villa" name="propType"
                            onChange={(e)=>{
                                const selected = e.currentTarget.defaultValue 
                                setProperty(selected)
                            }}
                        />Villa
                    </label>
                </div> */}
                {/* <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" value="apartment" name="propType"
                            onChange={(e)=>{
                                const selected = e.currentTarget.defaultValue 
                                setProperty(selected)
                            }}
                        />Apartment
                    </label>
                </div> */}
            </div>
            {componentShow}
            <div className="form-group floating-form-group">
                <label className="">Location</label>
                <Select
                    
                />
            </div>
            <div className="form-group floating-form-group">
                <label className="">Pin your location</label>
                <Select
                    
                />
            </div>
        </>
    )
}
