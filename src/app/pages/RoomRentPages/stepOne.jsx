import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Select from 'react-select';
import { stateOptions, groupedOptions, colourOptions } from '../MultiSelect/multiSelectData';
import MobileNav from '../MobileNav'
import TopNav from '../TopNav'
import StepTwo from "./stepOne-Three";

export default function StepOne() {
    const [country, setCountry] = useState('')
    const [city, setCity] = useState('')

    let componetShow
    if(country && city != ''){
        componetShow = <StepTwo/>
    } else {
        componetShow = ""
    }

    
    
    return (
        <>
            <main className="flex-shrink-0">
                <header className="header active">
                    <div className="row">
                        <div className="col-auto px-0">
                            <Link to="/place-an-add" className="btn menu-btn btn-link text-dark">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-a</title>
                                    <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                </svg>
                            </Link>
                        </div>
                        <div className="text-left col align-self-center">
                            <h5>Room Rent</h5>
                        </div>
                        <div className="ml-auto col-auto align-self-center">

                        </div>
                    </div>
                </header>
            </main>
            <div className='container'>
                <div className="form-group floating-form-group">
                    <label className="">Choose your Country</label>
                    <Select
                        onChange={(e)=>{
                           const selected = e.value
                           setCountry(selected)
                        }}
                        // defaultValue={stateOptions[1]}
                        options={groupedOptions}
                    />
                </div>
                <div className="form-group floating-form-group">
                    <label className="">Choose your City</label>
                    <Select
                        onChange={(e)=>{
                           const selected = e.value
                           setCity(selected)
                        }}
                        options={colourOptions}
                    />
                </div>
                {componetShow}
                
            </div>
            
            {/* <footer className="footer mt-auto py-3">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <button type='button' className="btn btn-block btn-info btn-lg">Back</button>
                        </div>
                        <div className="col">
                            <button type='button' onClick={handleClick} className="btn btn-block btn-info btn-lg">Next</button>
                        </div>
                    </div>
                </div>
            </footer> */}
        </>
    )
}
