import React, { useState } from 'react'
import DatePickers from "../DatePicker";

export default function StepThree() {
    const [transport, setTransport] = useState('')
    const [deposit, setDeposit] = useState('')
    const [rent, setRent] = useState('')

    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col">

                        <div className="form-group floating-form-group active">
                            <h6 className="text-dark my-1">Choose a bed type</h6>
                            <div className="custom-control custom-switch mt-3">
                                <input type="radio" name="bed_type" className="custom-control-input" id="sofa_bed"/>
                                <label className="custom-control-label" for="sofa_bed">Sofa bed</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="bed_type" className="custom-control-input" id="twin_or_xl_twin" />
                                <label className="custom-control-label" for="twin_or_xl_twin">Twin or XL Twin bed</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="bed_type" className="custom-control-input" id="queen_or_king_bed" />
                                <label className="custom-control-label" for="queen_or_king_bed">Full, Queen or King bed</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="bed_type" className="custom-control-input" id="no_bed" />
                                <label className="custom-control-label" for="no_bed">No bed</label>
                            </div>
                        </div>
                        
                        <div className="form-group floating-form-group active">
                            <h6 className="text-dark my-1">Set the room’s availability</h6>
                            <div className="calender">
                                <div className="">
                                    <lable className="ml-2">From</lable>
                                    <DatePickers/>
                                </div>
                                <div className="">
                                    <lable className="ml-2">To</lable>
                                    <DatePickers/>
                                </div>
                            </div>
                        </div>

                        <div className="form-group floating-form-group active">
                            <h6 className="text-dark my-1">Minimum stay (months)</h6>
                            <div className="custom-control custom-switch mt-3">
                                <input type="radio" name="minimum_stay" className="custom-control-input" id="one_month"/>
                                <label className="custom-control-label" for="one_month">1</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="minimum_stay" className="custom-control-input" id="two_month" />
                                <label className="custom-control-label" for="two_month">2</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="minimum_stay" className="custom-control-input" id="three_month" />
                                <label className="custom-control-label" for="three_month">3</label>
                            </div>
                            <div className="custom-control custom-switch">
                                <input type="radio" name="minimum_stay" className="custom-control-input" id="four_month" />
                                <label className="custom-control-label" for="four_month">4</label>
                            </div>
                        </div>

                        <div className={transport !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                            <input type="password"
                                onChange={(e)=>{setTransport(e.target.value)}} 
                                className="form-control floating-input"
                              
                            />
                            <label className="floating-label">Distance to Public Transport in minutes</label>
                        </div>
                        <div className={deposit !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                            <input type="password"
                                onChange={(e)=>{setDeposit(e.target.value)}} 
                                className="form-control floating-input"
                              
                            />
                            <label className="floating-label">Deposit for the room </label>
                        </div>
                        <div className={rent !=0 ? "form-group floating-form-group active" : "form-group floating-form-group"}>
                            <input type="password"
                                onChange={(e)=>{setRent(e.target.value)}} 
                                className="form-control floating-input"
                              
                            />
                            <label className="floating-label">What's the monthly rent?</label>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
