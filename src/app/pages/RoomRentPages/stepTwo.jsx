import React from 'react'

export default function StepTwo() {
    return (
        <>
            <div className="container">
                <div className="form-group floating-form-group">
                    <label className="">What do you want to list?</label>
                    <div className="mr-3"></div>
                    <div className="custom-control custom-switch">
                        <input type="radio" name="optradio" className="custom-control-input" id="room" />
                        <label className="custom-control-label" for="room">Room</label>
                    </div>
                    <div className="custom-control custom-switch">
                        <input type="radio" name="optradio" className="custom-control-input" id="bedspace" />
                        <label className="custom-control-label" for="bedspace">Bedspace</label>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="room_type">
                            <label>Bath</label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="radio" id="s-option" name="private_washroom_amenities" value="1"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/private_washroom.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Private </label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="radio" id="s-option" name="private_washroom_amenities" value="0"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/shared_washroom.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Shared </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="room_mates">
                            <label>Who are you Looking for? </label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="radio" className="disable_type" id="s-option" name="roommate_gender" value="M"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/male_radio.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Male</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="radio" className="disable_type" id="s-option" name="roommate_gender" value="F"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/female_radio.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Female</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="radio" className="disable_type" id="s-option" name="roommate_gender" value="M/F"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/mix.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Any </label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="amenities_check">
                            <label>Room amenities </label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="checkbox" id="f-option" className="disable_type" name="tv_amenities" value=""/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/tv_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">TV</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" className="disable_type" id="s-option" name="private_balcony_amenities" value=""/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/balcony_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Private Balcony</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" className="disable_type" id="s-option" name="window_amenities" value=""/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/window.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Window</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" className="disable_type" id="s-option" name="fridge_amenities" value=""/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/fridge.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Fridge</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" className="disable_type" id="s-option" name="natural_light_amenities" value=""/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/natural_light_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Natural light</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="amenities_check">
                            <label>House amenities</label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="checkbox" id="s-option" name="wifi_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/wifi_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Wifi</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="parking_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/parking_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Parking</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="f-option" name="elevator_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/elevatori_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Elevator</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="f-option" name="washing_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/washing_machine_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Washing machine</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="balcony_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/balcony_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Common Balcony</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="dishwasher_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/dishwasheri_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Dishwasher</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="wheelcheer_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/wheelchair_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Wheelchair friendly</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="terrace_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/terrace_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Terrace</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="living_room_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/furnished_roomi_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Living room</label>
                                        </div>
                                    </li>
                                </ul>
                                <div className="purpose-error1 555555"></div>
                            </div>
                        </div>
                        <div className="amenities_check house_rules">
                            <label>Outdoor Amenities</label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="checkbox" id="s-option" name="gym_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/gym.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Gym</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="pool_amenities" />
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/pool_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Pool</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="garden_amenities"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/garden_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Garden</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="table_tennis"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/table_tennis.png" alt="table_tennis"/>
                                            <label for="s-option" className="label_active">Table Tennis</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="squash"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/squash.png" alt="table_tennis"/>
                                            <label for="s-option" className="label_active">Squash</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" name="lone_tennis"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/lone_tennis.png" alt="table_tennis"/>
                                            <label for="s-option" className="label_active">Lawn Tennis</label>
                                        </div>
                                    </li>
                                </ul>         
                                <div className="purpose-error1 555555"></div>
                            </div>
                        </div>                   
                        <div className="amenities_check house_rules">
                            <label>Are there any house rules?</label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input type="checkbox" id="f-option" className="disable_type" name="house_rules_smoker"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/smoke_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Smoker friendly</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="s-option" className="disable_type" name="house_rules_pet"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/pet_checked.png" alt="Refinancing_hover"/>
                                            <label for="s-option" className="label_active">Pet friendly</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="f-option" className="disable_type" name="house_rules_couple"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/couples_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Couples accepted</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input type="checkbox" id="f-option" className="disable_type" name="house_rules_children"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/kids_checked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Children allowed</label>
                                        </div>
                                    </li>
                                </ul>
                                <div class="purpose-error1 555555"></div>
                            </div>
                        </div>                  
                        <div className="amenities_check house_rules">
                            <label>Other house rules?</label>
                            <div className="get_started_radio">
                                <ul className="container_ul">
                                    <li>
                                        <input value="strictly_male" className="disable_type" type="radio" id="f-option" name="other_house_rules"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/male_radio_clicked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Strictly male</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input value="strictly_female" className="disable_type" type="radio" id="f-option" name="other_house_rules"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/female_radio_clicked.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Strictly female</label>
                                        </div>
                                    </li>
                                    <li>
                                        <input value="mix" type="radio" className="disable_type" id="f-option" name="other_house_rules"/>
                                        <div className="check">
                                            <img className="radio_active" src="https://stage.roomdaddy.ae/public/assets/images/mix.png" alt="Purchasing_hover"/> 
                                            <label for="f-option" className="label_active">Mix</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
