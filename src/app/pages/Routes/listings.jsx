import React, { useState } from 'react'
import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import { Frame, Scroll } from "framer";

import MobileNav from '../MobileNav'

import { Link } from 'react-router-dom'



import Toogle from '../Toogle'
import List from '../Lists'
import Map from '../Map/FullMap'

export default function Listings() {
    const [toogled, setToogled] = useState(false)
    const [mapItems, setMapItems] = useState([{
        latitude : '',
        longitude: '',
        bath: '',
        rent: '',
        custom_room_name: '',
        room_images: [],
        bedtype: '',
        currency: '',
        building_id: 0
    }])
    const handleClick = ()=>{
        setToogled((state) => !state)
    }
    
    let component;
    if( toogled == false ){
        component =  <List mapItems={mapItems} setMapItems = {setMapItems}  ></List>
    } else {
        component =  <Map mapItems={mapItems}></Map>
    }
    return (
       
        <div>
            {/* <p className="text-dark text-center"></p> */}
            <main className="flex-shrink-0">
                <header className="header active">
                    <div className="row">
                        <div className="col-auto px-0">
                            <Link to="/" className="btn menu-btn btn-link text-dark">
                                <svg xmlns='http://www.w3.org/2000/svg' className="icon-size-24" viewBox='0 0 512 512'>
                                    <title>ionicons-v5-a</title>
                                    <polyline points='244 400 100 256 244 112' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                    <line x1='120' y1='256' x2='412' y2='256' style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'48px'}} />
                                </svg>
                            </Link>
                        </div>
                        <div className="text-left col align-self-center">
                            <h5>Listing page</h5>
                        </div>
                        <div className="ml-auto col-auto align-self-center">
                            <Toogle toogled={toogled} onClick={handleClick} />
                        </div>
                    </div>
                </header>
            </main>
            
            {component}
            <MobileNav/>
        </div>
        
    )
}
