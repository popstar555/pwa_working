import React, { useState, useEffect } from 'react'
import { Link, Redirect } from 'react-router-dom'
import Select from 'react-select';
import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import fetch from "cross-fetch";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import CircularProgress from "@material-ui/core/CircularProgress";

function sleep(delay = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

const Locations = [
    { label: "Dubai", value: "48975" },
    { label: "Sharjah", value: "Sharjah" },
    { label: "Abu Dhabi", value: "Abu Dhabi" },
    { label: "Ajman", value: "Ajman" },
    { label: "Ras Al Khaimah", value: "Ras Al Khaimah" },
    { label: "Umm Al Quwain", value: "Umm Al Quwain" },
    { label: "Fujairah", value: "Fujairah" }
  ];

  const CssTextField = withStyles({
    root: {
      "& label.Mui-focused": {
        color: "#80bdff"
      },
      "& .MuiInput-underline:after": {
        borderBottomColor: "#80bdff"
      },
      "& .MuiOutlinedInput-root": {
        "& fieldset": {
          borderColor: "#ced4da",
          borderRadius: "50px",
        },
        "&:hover fieldset": {
          borderColor: "#0a5e77"
        },
        "&.Mui-focused fieldset": {
          borderColor: "#80bdff"
        }
      }
    }
  })(TextField);

  const customStyles = {
    control: (base, state) => ({
      ...base,
      // match with the menu
      borderRadius: "50px",
      // Overwrittes the different states of border
      borderColor: state.isFocused ? "#80bdff" : "#ced4da",
      height: "48px",
      // Removes weird border around container
      boxShadow: state.isFocused ? null : null,
      "&:hover": {
        // Overwrittes the different states of border
        borderColor: state.isFocused ? "#80bdff" : "#0a5e77"
      }
    }),
    menu: base => ({
      ...base,
     
    }),
    menuList: base => ({
      
    })
  };

export default function Search() {
    const [sidebar, setSidebar] = useState(false)
    const [isFilter, setFilter] = useState(false)
    const [location, setLocation] = useState(null)
    const [city, setCity] = useState([])
    const [building, setBuilding] = useState([])
    const [non_building, setNonBuilding] = useState([])
    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [letters, setLetters] = React.useState(0);
    const [suffix, setSuffix] = React.useState("");
    const loading = open && letters > 3 && options.length === 0;

    const goToFilter = () => {
        setFilter(true)
        setSidebar(!sidebar)
    }

    const onPropertychange = (e, v) => {
        //setProperty(Array.isArray(e) ? e.map(x => x.value) : []);
        setNonBuilding([])
        setBuilding([])
        v.map(x => {
          if(x.type == "location") {
            setNonBuilding(oldArray => [...oldArray, x.id])
          }else {
            setBuilding(oldArray => [...oldArray, x.id])
          }
        })
      }
    const onLocationchange = (location) => {
        setLocation( location )
        setCity( location.value )
    }

    const onCheckLetters = (e) => {
      setLetters(e.target.value.length + 1)
      setSuffix(e.target.value)
    }

    React.useEffect(() => {
        let active = true;
    
        if (!loading) {
          return undefined;
        }
    
        (async () => {
          if(letters > 3){
            const response = await fetch(
              "https://roomdaddy.com/api/location_filter/" + suffix
            );
            await sleep(1e3); // For demo purposes.
            const countries = await response.json();
      
            if (active) {
              setOptions(countries.data);
            }
          }
        })();
    
        return () => {
          active = false;
        };
      }, [loading]);

      React.useEffect(() => {
        if (!open) {
          setOptions([]);
        }
      }, [open]);

    if(isFilter) {
        return <Redirect to={{pathname: '/filters', locations: city, building: building, non_building: non_building}}/>
    }else {
        return (
        <>
            <div className="container mt-4">
                <div className="form-group mb-0">
                    <div className="row">
                        <div className="col">
                            <Select options={Locations} styles={customStyles} onChange={onLocationchange} value={location} />
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col">
                          <Autocomplete
                            multiple
                            id="asynchronous-demo"
                            style={customStyles}
                            open={open}
                            onOpen={() => {
                                setOpen(true);
                            }}
                            onClose={() => {
                                setOpen(false);
                                setLetters(0);
                                setSuffix("");
                            }}
                            options={options}
                            getOptionSelected={(option, value) => option.value === value.name}
                            getOptionLabel={(option) => option.value + ":     " + option.type}
                            onChange={onPropertychange}
                            loading={loading}
                            filterSelectedOptions
                            renderInput={(params) => (
                                <CssTextField
                                {...params}
                                label="Asynchronous"
                                variant="outlined"
                                margin="0"
                                onChange={onCheckLetters}
                                InputProps={{
                                    ...params.InputProps,
                                    endAdornment: (
                                    <React.Fragment>
                                        {loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                        ) : null}
                                        {params.InputProps.endAdornment}
                                    </React.Fragment>
                                    )
                                }}
                                />
                            )}
                          />
                        </div>
                        <div className="col-auto pl-0">
                            <Button onClick={()=>goToFilter()} className="sqaure-btn btn btn-info text-white filter-btn" >
                                <svg xmlns="http://www.w3.org/2000/svg" className="icon-size-24" viewBox="0 0 512 512">
                                    <title>ionicons-v5-i</title>
                                    <line x1="368" y1="128" x2="448" y2="128" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <line x1="64" y1="128" x2="304" y2="128" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <line x1="368" y1="384" x2="448" y2="384" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <line x1="64" y1="384" x2="304" y2="384" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <line x1="208" y1="256" x2="448" y2="256" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <line x1="64" y1="256" x2="144" y2="256" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></line>
                                    <circle cx="336" cy="128" r="32" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></circle>
                                    <circle cx="176" cy="256" r="32" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></circle>
                                    <circle cx="336" cy="384" r="32" style={{fill:'none',stroke:'#000',strokeLinecap:'round',strokeLinejoin:'round',strokeWidth:'32px'}}></circle>
                                </svg>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <div className={sidebar ? "filter active" : "filter"}>

            </div>
            <div className="filter-backdrop"></div>
        </>
    )
    }
}
