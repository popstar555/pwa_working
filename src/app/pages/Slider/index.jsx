import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore from 'swiper';
import 'swiper/swiper-bundle.css';
import 'swiper/swiper.scss';
import SlideData from './SlideData';

import slider1 from '../../img/image-4.jpg'
import slider2 from '../../img/image-2.jpg'

export default function Slider() {
    return (
        <Swiper
        spaceBetween={50}
        slidesPerView={1.0}
        // onSlideChange={() => console.log('slide change')}
        // onSwiper={(swiper) => console.log(swiper)}
        >
            <SwiperSlide>{SlideData}</SwiperSlide>
            <SwiperSlide>{SlideData}</SwiperSlide>
            <SwiperSlide>{SlideData}</SwiperSlide>
            <SwiperSlide>{SlideData}</SwiperSlide>
            <SwiperSlide>{SlideData}</SwiperSlide>
        </Swiper>
    )
}
