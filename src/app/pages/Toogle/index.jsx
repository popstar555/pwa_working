import React from 'react'
import Location from '../../img/pin.svg'
import Listing from '../../img/list.svg'

export default function Toogle({toogled, onClick}) {
    return (
        <>
            <div onClick={onClick} className={`toogle${toogled ? " map" : " "}`}>
                <img src={Listing}  style={{ height: '25px',width: '15px',zIndex: '3'}}/>
                <div className="notch"></div>
                <img src={Location} style={{ height: '25px',width: '15px',zIndex: '3'}}/>
            </div>
        </>
    )
}
