import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from '../Header'
import chat from '../Routes/chat'
export default function index() {
    return (
        <body className="body-scroll d-flex flex-column h-100 menu-overlay">
            <Header logourl={'https://stage.roomdaddy.ae/public/frontend/images/footer_logo_dark_new.png'} />
        </body>
    )
}
