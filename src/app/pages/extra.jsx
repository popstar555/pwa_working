<div className="top">
<SearchBar/>
<div className="container mt-4 listings">
    { ListingsData.map((item, index)=>{
        return(
            <>
                <div className="card product-card-large mb-3">
                    <div className="card-body p-0">
                        <div className="product-image">
                            <Carousel controls={false} slide={false} interval={90000}>
                                <Carousel.Item className="" >
                                    <img
                                    className="d-block w-100 carousalImg"
                                    src={item.imgUrl}
                                    alt="First slide"
                                    />
                                </Carousel.Item>
                                <Carousel.Item className="" >
                                    <img
                                    className="d-block w-100 carousalImg"
                                    src={item.imgUrl2}
                                    alt="Third slide"
                                    />
                                </Carousel.Item>
                                <Carousel.Item className="">
                                    <img
                                    className="d-block w-100 carousalImg"
                                    src={item.imgUrl}
                                    alt="Third slide"
                                    />
                                </Carousel.Item>
                            </Carousel>
                            
                        </div>
                    </div>
                    <Link to="/someLink" key={index} className="card-footer">
                    <div className="">
                        <div className="">
                            <p className="small vm">
                                <span className=" text-secondary">{item.rating}</span>
                                <svg xmlns="http://www.w3.org/2000/svg" className="icon-size-12 vm" viewBox="0 0 24 24">
                                    <path d="M0 0h24v24H0z" fill="none" />
                                    <path d="M0 0h24v24H0z" fill="none" />
                                    <path fill="#FFD500" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
                                </svg>
                                <span className=" text-secondary">| {item.country}</span>
                            </p>
                        </div>
                        <div className="">
                            <p className="text-dark">{item.propertyType} </p>
                        </div>
                        
                    </div>
                    <div className="">
                        <div className="">
                            <p className="small text-secondary">{item.propertySize}</p>
                        </div>
                        <div className="">
                            <p className="small text-secondary">
                                {item.propertyPrice}
                                <span className="small text-secondary"> / {item.perNight}</span>
                            </p>
                        </div>
                    </div>
                </Link>
                </div> 
            </>
        )
    })}
</div>        
</div>



















{/* <SwipeableBottomSheet style={{ zIndex: '2'}} overflowHeight={64} fullScreen={true} > */}
<div style={{ height: '40px' }}>
<div style={{backgroundImage: `url(${Arrow})`,
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    width: '5%',
    margin: 'auto',
    position: 'relative',
    overflow: 'hidden',
}}>
{/* <img src={Arrow}/> */}
</div>
<div className="top">
    <SearchBar/>
    <div className="container mt-4 listings">
        {/* <Scroll width={317} height={860} radius={0}> */}
            { ListingsData.map((item, index)=>{
                return(
                    <>
                    {/* <Frame 
                        transition={{ duration: 1 }}
                        height={350} radius={20} width={317} 
                        background={"none"}
                        radius={30}
                    > */}
                        <div className="card product-card-large mb-3">
                            <div className="card-body p-0">
                                <div className="product-image">
                                    <Carousel controls={false} slide={false} interval={90000}>
                                        <Carousel.Item className="" >
                                            <img
                                            className="d-block w-100 carousalImg"
                                            src={item.imgUrl}
                                            alt="First slide"
                                            />
                                        </Carousel.Item>
                                        <Carousel.Item className="" >
                                            <img
                                            className="d-block w-100 carousalImg"
                                            src={item.imgUrl2}
                                            alt="Third slide"
                                            />
                                        </Carousel.Item>
                                        <Carousel.Item className="">
                                            <img
                                            className="d-block w-100 carousalImg"
                                            src={item.imgUrl}
                                            alt="Third slide"
                                            />
                                        </Carousel.Item>
                                    </Carousel>
                                    
                                </div>
                            </div>
                            <Link to="/someLink" key={index} className="card-footer">
                            <div className="">
                                <div className="">
                                    <p className="small vm">
                                        <span className=" text-secondary">{item.rating}</span>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="icon-size-12 vm" viewBox="0 0 24 24">
                                            <path d="M0 0h24v24H0z" fill="none" />
                                            <path d="M0 0h24v24H0z" fill="none" />
                                            <path fill="#FFD500" d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z" />
                                        </svg>
                                        <span className=" text-secondary">| {item.country}</span>
                                    </p>
                                </div>
                                <div className="">
                                    <p className="text-dark">{item.propertyType} </p>
                                </div>
                                
                            </div>
                            <div className="">
                                <div className="">
                                    <p className="small text-secondary">{item.propertySize}</p>
                                </div>
                                <div className="">
                                    <p className="small text-secondary">
                                        {item.propertyPrice}
                                        <span className="small text-secondary"> / {item.perNight}</span>
                                    </p>
                                </div>
                            </div>
                        </Link>
                        </div> 
                    {/* </Frame> */}
                    </>
                )
            })}
        {/* </Scroll> */}
    </div>        
</div>
</div>
{/* <MobileNav/> */}
{/* </SwipeableBottomSheet> */}